package com.ICMSoft.BH_Health;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ICMSoft.BH_Health.ui.WelcomeActivity;

import java.util.Objects;

public class SplashAcitivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Objects.requireNonNull(getSupportActionBar()).hide();
        new Handler().postDelayed(() -> {
            Intent i = new Intent(SplashAcitivity.this, WelcomeActivity.class);
            startActivity(i);
            finish();

        }, 3000);
    }
}
