package com.ICMSoft.BH_Health.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.News.Datum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends  RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    List<Datum> activitics;
    private NewsDetailsShowListener newsDetailsShowListener;
    private ArrayList<Datum> FamilyAdapterArrayList;
    public interface NewsDetailsShowListener{void newsDetailsShowOnClicked(View view,int position);}

    public void setNewsDetailsShowListener(NewsDetailsShowListener newsDetailsShowListener){
        this.newsDetailsShowListener = newsDetailsShowListener;

    }


    public NewsAdapter(ArrayList<Datum> FamilyAdapterArrayList) {
        activitics = FamilyAdapterArrayList;
    }


    @NonNull
    @Override
    public NewsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_latest_news, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.MyViewHolder holder, int position) {
        Datum data = activitics.get(position);
        holder.titleTV.setText(data.getTitle());
        holder.detailsTV.setText(data.getContent());


    }



    @Override
    public int getItemCount() {
        return activitics.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTV)
        TextView titleTV;
        @BindView(R.id.timeTV)
        TextView timeTV;
        @BindView(R.id.headerTV)
        TextView headerTV;
        @BindView(R.id.detailsTV)
        TextView detailsTV;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(v -> {
                Log.d("news", "onClick: "+getAdapterPosition());
                newsDetailsShowListener.newsDetailsShowOnClicked(itemView,getAdapterPosition());
            });
        }
    }
}
