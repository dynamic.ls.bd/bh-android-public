package com.ICMSoft.BH_Health.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Circle.information.CircleUserData;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;

import java.util.List;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_PHONE;


public class CircleAdapter extends RecyclerView.Adapter<CircleAdapter.MyViewHolder> {
    private static final String TAG = "CircleAdapter";
    private List<CircleUserData> circleUserList;
    private CircleMemberUpdateClickListener CircleMemberUpdateClickListener;
    private CircleMemberDeleteClickListener CircleMemberDeleteClickListener;
    private Context context;


    public interface CircleMemberUpdateClickListener {
        void CircleMemberUpdateClicked(View view, CircleUserData circleUser);
    }
    public interface  CircleMemberDeleteClickListener{
        void CircleMemberDeleteClicked(View view, CircleUserData circleUser);
    }

    public void setCircleMemberUpdateClickListener(CircleMemberUpdateClickListener CircleMemberUpdateClickListener)
    {
        this.CircleMemberUpdateClickListener= CircleMemberUpdateClickListener;
    }
    public void setCircleMemberDeleteClickListener(CircleMemberDeleteClickListener CircleMemberDeleteClickListener)
    {
        this.CircleMemberDeleteClickListener= CircleMemberDeleteClickListener;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView nameTV;
        private TextView ageTV;
        private TextView contactTV;
        private ImageView editIV, deleteTV;

        public MyViewHolder(View v) {
            super(v);
            nameTV = itemView.findViewById(R.id.usernameTV);
            //ageTV = (TextView) itemView.findViewById(R.id.ageTV);
            contactTV = itemView.findViewById(R.id.contactTV);
            editIV = itemView.findViewById(R.id.edit_iv);
            deleteTV = itemView.findViewById(R.id.delete);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CircleAdapter(List<CircleUserData> FamilyAdapterArrayList,Context context) {
        this.circleUserList = FamilyAdapterArrayList;
        GeneralHelper generalHelper = new GeneralHelper(context);

    }

    // Create new views (invoked by the layout manager)
    @Override
    public CircleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        //     TextView v = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_trip_card, parent, false);
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_safe_circle_member, parent, false);

        CircleAdapter.MyViewHolder vh = new CircleAdapter.MyViewHolder(v);
        return vh;

    }

    @NonNull
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        CircleUserData user = circleUserList.get(position);
        Log.d(TAG, "onBindViewHolder: "+user.getName());

        if (user != null ) {
            Log.d(TAG, "onBindViewHolder: " + user.getName());
            holder.nameTV.setText(user.getName());
            // holder.ageTV.setText(user.getAge());
            holder.contactTV.setText(user.getPhone());

            if (user.getStatic()==0)
                holder.editIV.setVisibility(View.VISIBLE);
            holder.editIV.setOnClickListener(view -> {
                Log.d(TAG, "onBindViewHolder11: "+user.getName());
                CircleMemberUpdateClickListener.CircleMemberUpdateClicked(view, user);
            });
            holder.deleteTV.setOnClickListener(view -> CircleMemberDeleteClickListener.CircleMemberDeleteClicked(view, user));



        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return circleUserList.size();
    }
}
