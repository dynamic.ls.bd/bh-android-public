package com.ICMSoft.BH_Health.adapter;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.faq.Datum;

import java.util.ArrayList;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.MyViewHolder> {
    private static final String TAG = "FaqAdapter";
    private ArrayList<Datum> dataList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        protected TextView categoryTV;
        protected TextView dateTV;
        protected TextView tvTo;
        protected TextView subjectTV;
        protected TextView messageTV;


        ///   protected LinearLayout linearLayout;



        public MyViewHolder(View v) {
            super(v);
            categoryTV = (TextView) itemView.findViewById(R.id.tv_category);
            dateTV = (TextView) itemView.findViewById(R.id.date_tv);
            tvTo = (TextView) itemView.findViewById(R.id.tv_to);
            subjectTV = (TextView) itemView.findViewById(R.id.tv_subject);
            messageTV = (TextView) itemView.findViewById(R.id.tv_message);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FaqAdapter(ArrayList<Datum> dataList) {
        this.dataList = dataList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FaqAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        //     TextView v = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_trip_card, parent, false);
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_faq, parent, false);

        FaqAdapter.MyViewHolder vh = new FaqAdapter.MyViewHolder(v);
        return vh;

    }

    @NonNull
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Datum Datum = dataList.get(position);

        holder.categoryTV.setText(Datum.getCategory());
        holder.dateTV.setText(Datum.getCreatedAt());
        holder.tvTo.setText(Datum.getTo());
        holder.subjectTV.setText(Datum.getSubject());
        holder.messageTV.setText(Datum.getMessage());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
