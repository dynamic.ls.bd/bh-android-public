package com.ICMSoft.BH_Health.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.SelfQuarantine.Datum;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityQuarantineAdapter extends RecyclerView.Adapter<ActivityQuarantineAdapter.MyViewHolder>{

    private Context mContext;
    private ActivityListener activityListener;
    List<Datum> activitics;
    Date date;
    public interface ActivityListener{void activityOnClicked(View view,int position);}

    public void setActivityListener(ActivityListener activityListener){
        this.activityListener = activityListener;

    }

    public ActivityQuarantineAdapter(ArrayList<Datum> list, Date date) {
        activitics = list;
        this.date = date;

    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewlistItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.self_quarantine_activity_list,parent,false);

        MyViewHolder vh = new MyViewHolder(viewlistItem);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Date today = new Date();
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);

        Calendar tommorow=Calendar.getInstance();
        tommorow.add(Calendar.DAY_OF_MONTH,1);
        Datum data = activitics.get(position);
        String d = "Day " + (position + 1);
        calendar.add(Calendar.DAY_OF_MONTH,(position));
        int dayDiff = (int) ((today.getTime()-calendar.getTimeInMillis())/86400000);
        if(dayDiff < 14 && dayDiff >=0)
        {

            SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
            String day = sf.format(calendar.getTime());
            String tm = sf.format(tommorow.getTime());
            if(!day.equalsIgnoreCase(tm))
            d+="\n"+day;

            //SelfQuarantineFragment.DateWiseActivityDialog();
        }
        if(data!=null) {
            holder.quarantineDay.setText(d);
            holder.quarantineActivity.setText(data.getActivity().get(0));
            holder.quarantineSuffering.setText(data.getSufferings().get(0));
        }
        else {
            holder.quarantineDay.setText(d);
            holder.quarantineActivity.setText("Activity");
            holder.quarantineSuffering.setText("Suffering");
        }
    }

    @Override
    public int getItemCount() {
        return activitics.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.quarantine_day)
        TextView quarantineDay;
        @BindView(R.id.quarantine_activity)
        TextView quarantineActivity;
        @BindView(R.id.quarantine_suffering)
        TextView quarantineSuffering;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activityListener.activityOnClicked(itemView,getAdapterPosition());
                }
            });
        }
    }
}
