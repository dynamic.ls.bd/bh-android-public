package com.ICMSoft.BH_Health.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.notification.Datum;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends  RecyclerView.Adapter<NotificationAdapter.MyViewHolder>  {

    private List<Datum> notifications;

    public NotificationAdapter(List<Datum> notifications) {
        this.notifications = notifications;
    }

    @NonNull
    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder holder, int position) {
        Datum data = notifications.get(position);

        holder.title.setText(data.getTitle());
        if(data.getReatAt()==null)
            holder.title.setTypeface(null, Typeface.BOLD);
        holder.description.setText(data.getMessage());
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notification_title)
        TextView title;
        @BindView(R.id.notification_description)
        TextView description;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
