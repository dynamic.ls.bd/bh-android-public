package com.ICMSoft.BH_Health.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting.TodayIMeet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodayIMeetAdapter extends RecyclerView.Adapter<TodayIMeetAdapter.MyHolder> {

    private MemberUpdateClickListener memberUpdateClickListener;
    private MemberDeleteClickListener memberDeleteClickListener;
    private List<TodayIMeet>meetedList;

    public interface MemberUpdateClickListener {
        void MemberUpdateClicked(TodayIMeet circleUser);
    }
    public interface  MemberDeleteClickListener{
        void MemberDeleteClicked(TodayIMeet circleUser);
    }

    public void setMemberUpdateClickListener(MemberUpdateClickListener memberUpdateClickListener) {
        this.memberUpdateClickListener = memberUpdateClickListener;
    }

    public void setMemberDeleteClickListener(MemberDeleteClickListener memberDeleteClickListener) {
        this.memberDeleteClickListener = memberDeleteClickListener;
    }

    public TodayIMeetAdapter(List<TodayIMeet> meetedList) {
        this.meetedList = meetedList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_today_meet_member, parent, false);

        MyHolder vh = new MyHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        TodayIMeet user = meetedList.get(position);
        holder.name.setText(user.getPersonName());


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy, hh:mm a");
        try {
            Date date = dateFormat.parse(user.getMeetingTime());
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a");
            holder.time.setText(dateFormat1.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return meetedList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.timeTM)
        TextView time;
        @BindView(R.id.nameTM)
        TextView name;

        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.edit)
        ImageView edit;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.delete)
        public void deleteMember(){
            memberDeleteClickListener.MemberDeleteClicked(meetedList.get(getAdapterPosition()));
        }
        @OnClick(R.id.edit)
        public void editMember(){
            memberUpdateClickListener.MemberUpdateClicked(meetedList.get(getAdapterPosition()));
        }
    }
}
