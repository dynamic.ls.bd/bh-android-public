package com.ICMSoft.BH_Health.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.protocol.ProtocolDatum;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProtocolAdapter extends  RecyclerView.Adapter<ProtocolAdapter.MyViewHolder> {

    List<ProtocolDatum> protocols;
    private ProtocolShowListener protocolShowListener;
    public interface ProtocolShowListener{
        void onClickedForShow(String document);
    }

    public void setProtocolShowListener(ProtocolShowListener protocolShowListener) {
        this.protocolShowListener = protocolShowListener;
    }

    public ProtocolAdapter(List<ProtocolDatum> protocols) {
        this.protocols = protocols;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_latest_news, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProtocolDatum data = protocols.get(position);
        holder.titleTV.setText(data.getTitle());
        holder.detailsTV.setVisibility(View.GONE);
        holder.timeTV.setVisibility(View.GONE);
        holder.headerTV.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(v -> {
            protocolShowListener.onClickedForShow(data.getDocument());
        });

    }

    @Override
    public int getItemCount() {
        return protocols.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTV)
        TextView titleTV;
        @BindView(R.id.timeTV)
        TextView timeTV;
        @BindView(R.id.headerTV)
        TextView headerTV;
        @BindView(R.id.detailsTV)
        TextView detailsTV;

        View itemView;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            this.itemView =itemView;
        }
    }
}
