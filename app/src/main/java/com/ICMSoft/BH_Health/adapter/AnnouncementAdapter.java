package com.ICMSoft.BH_Health.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Announcement.Datum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.MyHolder> {

    List<Datum> activitics;
    private AnnouncementDetailsListener announcementDetailsListener;
    private ArrayList<Datum> FamilyAdapterArrayList;
    public interface AnnouncementDetailsListener{void AnnouncementDetailsOnClicked(View view,int position);}



    @NonNull
    @Override


    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_latest_news, parent, false);
        AnnouncementAdapter.MyHolder vh = new AnnouncementAdapter.MyHolder(v);
        return vh;
    }

    public AnnouncementAdapter(List<Datum> activitics) {
        this.activitics = activitics;
    }

    public void setAnnouncementDetailsListener(AnnouncementDetailsListener announcementDetailsListener) {
        this.announcementDetailsListener = announcementDetailsListener;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        Datum data = activitics.get(position);
        holder.titleTV.setText(data.getTitle());
        holder.detailsTV.setText(data.getContent());
    }






    @Override
    public int getItemCount() {
        return activitics.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTV)
        TextView titleTV;
        @BindView(R.id.timeTV)
        TextView timeTV;
        @BindView(R.id.headerTV)
        TextView headerTV;
        @BindView(R.id.detailsTV)
        TextView detailsTV;


        public MyHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                Log.d("news", "onClick: " + getAdapterPosition());
                announcementDetailsListener.AnnouncementDetailsOnClicked(itemView, getAdapterPosition());
            });
        }
    }
}
