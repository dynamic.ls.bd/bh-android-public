package com.ICMSoft.BH_Health.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Circle.todayIVisit.TodayIVisit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodayIVisitAdapter extends RecyclerView.Adapter<TodayIVisitAdapter.MyHolder> {

    private VisitUpdateClickListener visitUpdateClickListener;
    private VisitDeleteClickListener visitDeleteClickListener;
    private List<TodayIVisit> visitList;

    public interface VisitUpdateClickListener {
        void VisitUpdateClicked(TodayIVisit user);
    }
    public interface  VisitDeleteClickListener{
        void VisitDeleteClicked(TodayIVisit user);
    }

    public void setVisitUpdateClickListener(VisitUpdateClickListener visitUpdateClickListener) {
        this.visitUpdateClickListener = visitUpdateClickListener;
    }

    public void setVisitDeleteClickListener(VisitDeleteClickListener visitDeleteClickListener) {
        this.visitDeleteClickListener = visitDeleteClickListener;
    }

    public TodayIVisitAdapter(List<TodayIVisit> visitList) {
        this.visitList = visitList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_today_visit, parent, false);

        MyHolder vh = new MyHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        TodayIVisit user = visitList.get(position);
        holder.place.setText(user.getVisitAddress());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy, hh:mm a");
        try {
            Date date = dateFormat.parse(user.getTime());
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a");
            holder.time.setText(dateFormat1.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return visitList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.timeTM)
        TextView time;


        @BindView(R.id.placeTV)
        TextView place;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.edit)
        public void editVisit(){
            visitUpdateClickListener.VisitUpdateClicked(visitList.get(getAdapterPosition()));
        }
        @OnClick(R.id.delete)
        public void deleteVisit(){
            visitDeleteClickListener.VisitDeleteClicked(visitList.get(getAdapterPosition()));
        }
    }
}
