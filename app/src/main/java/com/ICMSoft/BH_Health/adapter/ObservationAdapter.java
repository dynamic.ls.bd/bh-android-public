package com.ICMSoft.BH_Health.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.observetion.observationList.Datum;

import java.util.ArrayList;
import java.util.List;

public class ObservationAdapter extends RecyclerView.Adapter<ObservationAdapter.MyViewHolder> {
    private static final String TAG = "FaqAdapter";
    private List<Datum> dataList;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        protected TextView categoryTV;
        protected TextView dateTV;
        protected TextView tvTo;
        protected TextView subjectTV;
        protected TextView messageTV;


        public MyViewHolder(View v) {
            super(v);
            categoryTV = itemView.findViewById(R.id.tv_category);
            dateTV = itemView.findViewById(R.id.date_tv);
            tvTo = itemView.findViewById(R.id.tv_to);
            subjectTV = itemView.findViewById(R.id.tv_subject);
            messageTV = itemView.findViewById(R.id.tv_message);


        }
    }

    public ObservationAdapter(List<Datum> dataList) {
        this.dataList = dataList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_faq, parent, false);

        return new MyViewHolder(v);

    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Datum Datum = dataList.get(position);

        holder.categoryTV.setText(Datum.getType());
        holder.dateTV.setText(Datum.getCreatedAt());
        holder.tvTo.setText(Datum.getTo());
        String tager = "";
        boolean flag = false;
        for (String tag : Datum.getTags()) {
            if (flag)
                tager += ", ";
            flag = true;
            tager += tag;
        }
        holder.subjectTV.setText(tager);
        holder.messageTV.setText(Datum.getMessage());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
