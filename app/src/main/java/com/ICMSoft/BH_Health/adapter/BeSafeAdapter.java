package com.ICMSoft.BH_Health.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Html;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.beSafe.Datum;
import com.ICMSoft.BH_Health.data.model.beSafe.PicassoImageGetter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BeSafeAdapter extends RecyclerView.Adapter<BeSafeAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Datum> data;
    public BeSafeAdapter(@NonNull Context context, ArrayList<Datum> data) {

        this.context =context;
        this.data = data;
    }
    private ImageShow imageShow;
    public interface ImageShow{
        void imagePressed(View view, String url);
    }

    public void setImageShow(ImageShow imageShow) {
        this.imageShow = imageShow;
    }

    @NonNull
    @Override
    public BeSafeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_be_safe, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull BeSafeAdapter.MyViewHolder holder, int position) {
        Datum user = data.get(position);
        PicassoImageGetter imageGetter = new PicassoImageGetter(context,holder.imageView,user.getImage());

        Spannable html;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            imageGetter.setFlag(true);
            html = (Spannable) Html.fromHtml(user.getContent(), Html.FROM_HTML_MODE_LEGACY, imageGetter, null);
        } else {
            imageGetter.setFlag(false);
            Log.d("be", "onBindViewHolder: "+holder.body.getMeasuredWidth());

            html = (Spannable) Html.fromHtml(user.getContent(), imageGetter, null);

        }
       //String data = "https://covid.icmsoft.xyz/images/covid-info01.jpg";
        if(user.getImage()!=null)
        Picasso.get()
                .load(user.getImage())
                //.fit()
                .into(holder.imageView);
        if(user.getContent()!= null)
        holder.body.setText(html);


        holder.title.setText(user.getTitle());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageShow.imagePressed(v,user.getImage());
            }
        });
        /*
        holder.body.loadData(user.getContent(),"text/html; charset=UTF-8","UTF-8");
        holder.body.getSettings().setBuiltInZoomControls(true);

         */


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTV)
        TextView title;
        @BindView(R.id.bodyTV)
        TextView body;

        @BindView(R.id.bodyImage)
        ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }


    }
}
