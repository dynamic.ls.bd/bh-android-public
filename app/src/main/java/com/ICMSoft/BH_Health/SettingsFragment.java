package com.ICMSoft.BH_Health;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.UserPref;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    private static final String Language = "My_Language";
    private SharedPreferences sharedpreferences;
    public SettingsFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.language_changer)
    ToggleSwitch changer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        sharedpreferences = requireActivity().getSharedPreferences(UserPref, Context.MODE_PRIVATE);


        if (sharedpreferences.getString(Language, "bn").compareToIgnoreCase("en") == 0) {
            changer.setCheckedTogglePosition(1);
        }

        changer.setOnToggleSwitchChangeListener((position, isChecked) -> {
            if(changer.getCheckedTogglePosition() == 0)
                setLocal("bn");
            else
                setLocal("en");
        });

        return view;
    }




    private void setLocal(String locale) {
        if(sharedpreferences.getString(Language, "").compareToIgnoreCase(locale) != 0) {
            Resources resources = getResources();
            DisplayMetrics dm = resources.getDisplayMetrics();
            Configuration config = resources.getConfiguration();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                config.setLocale(new Locale(locale.toLowerCase()));
            } else {
                config.locale = new Locale(locale.toLowerCase());
            }
            resources.updateConfiguration(config, dm);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Language, locale);
            editor.apply();
            refreshActivity();
        }

    }
    private void refreshActivity(){
        Intent i = requireActivity().getIntent();
        i.putExtra("jump",true);
        requireActivity().finish();
        requireActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        requireActivity().startActivity(i);
    }

}
