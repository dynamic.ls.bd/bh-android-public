package com.ICMSoft.BH_Health;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.ICMSoft.BH_Health.ui.login.LoginActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ICMSoft.BH_Health.data.model.Auth.LogoutResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;

import java.util.Objects;

import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.AccessToken;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.Language;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.ROLE;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.UserPref;


public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";
    private MainActivityViewHolder mainActivityViewHolder;
    private GeneralHelper mGeneralHelper;
    private SharedPreferences sharedPreferences;
    private static String Role;
    private NavController navController;
    boolean doubleBackToExitPressedOnce = false;
    AppBarConfiguration appBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isLoggedIn();
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        mGeneralHelper = new GeneralHelper(getApplicationContext());

        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();


        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        mainActivityViewHolder = new ViewModelProvider(this).get(MainActivityViewHolder.class);
        if(getIntent().getExtras()!=null)
            gotoSetting();

    }

    private void isLoggedIn() {

        sharedPreferences = getSharedPreferences(UserPref, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(AccessToken, null);
        if (token == null) {

            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }
        Role = sharedPreferences.getString(ROLE,"");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                logout();
                return true;

            case R.id.menu_profile:
                gotoProfile();
                return true;

            case R.id.menu_settings:
                gotoSetting();
                return true;


            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void gotoProfile() {
        navController.navigate(R.id.profileFragment);
    }

    private void gotoSetting() {
        navController.navigate(R.id.settingsFragment);
    }

    private void logout() {

        String token = mGeneralHelper.getToken();
        Log.d(TAG, "logout: " + token);
        final Call<LogoutResponse> call = mGeneralHelper.getApiService().logout(token);
        mainActivityViewHolder.logoutUser(call);


        mainActivityViewHolder.logoutResponse().observe(this, aBoolean -> {

            if (aBoolean) {
                Toast.makeText(getApplicationContext(), "Logout Successfully", Toast.LENGTH_SHORT).show();
                sharedPreferences = getSharedPreferences(UserPref, Context.MODE_PRIVATE);
                String lan = sharedPreferences.getString(Language,"");
                sharedPreferences.edit().clear().apply();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Language,lan);
                editor.apply();

                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();

            } else {

                Toast.makeText(getApplicationContext(), "Logout Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {

        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }


    public static Boolean checkRole(String role){
        return Role.compareToIgnoreCase(role)==0;
    }



    @Override
    public void onBackPressed() {
        if(Objects.requireNonNull(navController.getCurrentDestination()).getId()==R.id.navigation_home) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
        else
            super.onBackPressed();
    }

}


