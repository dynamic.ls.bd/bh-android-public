package com.ICMSoft.BH_Health.Services;

import android.content.Context;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static final String DOMAIN = "https://www.covid.icmsoft.xyz/api/";
    private static Retrofit retrofit = null;

    public static Retrofit getRetrofitClient(Context context) {
        if (retrofit==null) {
            ////
            int cacheSize = 10 * 1024 * 1024; // 10 MB
            Cache cache = new Cache(context.getCacheDir(), cacheSize);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .cache(cache)
                    .build();

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(DOMAIN)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create());

            retrofit = builder.build();

        }
        return retrofit;
    }

}
