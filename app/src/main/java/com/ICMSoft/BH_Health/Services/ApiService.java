package com.ICMSoft.BH_Health.Services;


import com.ICMSoft.BH_Health.data.model.Announcement.AnnouncementResponse;
import com.ICMSoft.BH_Health.data.model.Auth.ForgetPassword.ForgetPassword;
import com.ICMSoft.BH_Health.data.model.Auth.LoginResponse;
import com.ICMSoft.BH_Health.data.model.Auth.LogoutResponse;
import com.ICMSoft.BH_Health.data.model.Auth.ResetPassword.ResetPassword;
//import com.ICMSoft.BH_Health.data.model.Circle.catagoryWiseData.CatagoryWiseData;
//import com.ICMSoft.BH_Health.data.model.Circle.configuration.SafeCircleConfig;
import com.ICMSoft.BH_Health.data.model.Circle.editCircle.EditUserCircle;
import com.ICMSoft.BH_Health.data.model.Circle.information.CircleInformation;
import com.ICMSoft.BH_Health.data.model.Circle.memberInformation.MemberInformation;
import com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting.TodayIMeetResponse;
import com.ICMSoft.BH_Health.data.model.Circle.todayIVisit.TodayIVisitResponse;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.HealthRecord.HealthRecordResponse;
import com.ICMSoft.BH_Health.data.model.HealthRecord.PainList;
import com.ICMSoft.BH_Health.data.model.News.Details.NewsDetailsResponse;
import com.ICMSoft.BH_Health.data.model.News.NewsResponse;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryListResponse;
import com.ICMSoft.BH_Health.data.model.Register.RegisterResponse;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.UpazilaListResponse;
import com.ICMSoft.BH_Health.data.model.SelfQuarantine.SelfQuarantineResponse;
import com.ICMSoft.BH_Health.data.model.Circle.user.UserResponse;
import com.ICMSoft.BH_Health.data.model.Statistics.GetStatistics;
import com.ICMSoft.BH_Health.data.model.beSafe.BeSafeResponse;
import com.ICMSoft.BH_Health.data.model.beSafe.Catagory.BeSafeCatagory;
import com.ICMSoft.BH_Health.data.model.faq.FaqResponse;
import com.ICMSoft.BH_Health.data.model.notification.GetNotification;
import com.ICMSoft.BH_Health.data.model.observetion.ObservationTypeResponse;
import com.ICMSoft.BH_Health.data.model.observetion.observationList.ObservationListResponse;
import com.ICMSoft.BH_Health.data.model.profile.ProfileResponse;
import com.ICMSoft.BH_Health.data.model.protocol.ProtocolResponse;
import com.ICMSoft.BH_Health.ui.Announcement.AnnouncementDetailsResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {


    @POST("auth/login")
    @Headers("Content-Type: application/json")
    Call<LoginResponse> login(@Body Map<String, String> body);

    @POST("auth/register")
    @Headers("Content-Type: application/json")
    Call<RegisterResponse> register(@Body Map<String, Object> body);

    @POST("auth/recovery")
    @Headers("Content-Type: application/json")
    Call<ForgetPassword> forgetPassword(@Body Map<String, String> body);

    @POST("auth/reset")
    @Headers("Content-Type: application/json")
    Call<ResetPassword> resetPassword(@Body Map<String, String> body);


    @POST("auth/logout")
    @Headers({"Accept: application/json"})
    Call<LogoutResponse> logout(
            @Header("Authorization") String Authorization);


    @POST("store-user-feedback")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> storeQuestion(
            @Header("Authorization") String Authorization,
            @Body Map<String, String> body);





    @GET("my-circle-wise-user")
    @Headers("Content-Type: application/json")
    Call<CircleInformation> myCircles(
            @Header("Authorization") String Authorization, @Query("category") String body);
    @POST("circle-user-edit")
    @Headers("Content-Type: application/json")
    Call<MemberInformation> memberInformation(
            @Header("Authorization") String Authorization, @Body Map<String, String> body);

    @GET("news")
    @Headers("Content-Type: application/json")
    Call<NewsResponse> getNews(
            @Header("Authorization") String Authorization);

    @GET
    @Headers("Content-Type: application/json")
    Call<NewsDetailsResponse> getNewsDetails(@Url String url);

    @GET("announcements/index")
    @Headers("Content-Type: application/json")
    Call<AnnouncementResponse> getAnnouncement(
            @Header("Authorization") String Authorization);

    @GET
    @Headers("Content-Type: application/json")
    Call<AnnouncementDetailsResponse> getAnnouncementDetails(@Header("Authorization") String Authorization, @Url String url);



    @POST("circle-user-store")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> storeCircleMember(
            @Header("Authorization") String Authorization, @Body Map<String, String> body);

    @POST("circle-user-update")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> updateCircleMember(
            @Header("Authorization") String Authorization, @Body Map<String, String> body);

    @POST("circle-store")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> storeCircle(
            @Header("Authorization") String Authorization, @Body Map<String, String> body);
    @POST
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> deleteMember(@Header("Authorization") String Authorization, @Url String url);

    @GET("profile")
    @Headers("Content-Type: application/json")
    Call<ProfileResponse> getProfile(
            @Header("Authorization") String Authorization);

    @POST("profile")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> updateProfile(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);


    @POST("health_records/own_records")
    @Headers("Content-Type: application/json")
    Call<HealthRecordResponse> getHealthRecord(
            @Header("Authorization") String Authorization, @Query("date") String date);

    @POST("health_records/store")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> createHealthRecord(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> updateHealthRecord (@Header("Authorization") String Authorization,@Url String uri, @Body Map<String, Object> body);

    @GET("quarentations")
    @Headers("Content-Type: application/json")
    Call<SelfQuarantineResponse> getSelfQuarantine(
            @Header("Authorization") String Authorization);
    @GET
    @Headers("Content-Type: application/json")
    Call<SelfQuarantineResponse> getPaginationWiseSelfQuarantine(
            @Header("Authorization") String Authorization,@Url String quarantinePath);

    @POST("quarentations")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> createSelfQuarantine(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST("quarentation-update")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> updateSelfQuarantine(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);

    //@Streaming
    @GET
    Call<BeSafeResponse> getBeSafe(@Url String url);


    @POST("list")
    @Headers("Content-Type: application/json")
    Call<FaqResponse> getFaq(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
   // @Multipart
    @POST("message-store")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> createEmergancy(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @Multipart
    @POST("message-store")
   // @Headers("Content-Type: application/json")
    Call<GeneralResponse> createEmergancyVoice(
            @Header("Authorization") String Authorization,@Part("type") RequestBody type,@Part MultipartBody.Part record);
    @POST("search-upazila")
    @Headers("Content-Type: application/json")
    Call<UpazilaListResponse> getUpazilaList(@Body Map<String, String> body);

    @POST("search-user")
    @Headers("Content-Type: application/json")
    Call<UserResponse> getUserList(@Header("Authorization") String Authorization,@Query("term") String phoneNumber);

    @POST("search-country")
    @Headers("Content-Type: application/json")
    Call<CountryListResponse> getCountryList(@Body Map<String, String> body);

    @GET("category")
    @Headers("Content-Type: application/json")
    Call<BeSafeCatagory> getBeSafeCatagory();

    @GET("get-config/pain_type")
    @Headers("Content-Type: application/json")
    Call<PainList> getPainList();


    @POST("circle-user-store")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> createMember(
            @Header("Authorization") String Authorization, @Body Map<String, String> body);
    @POST("circle-user-edite")
    @Headers("Content-Type: application/json")
    Call<EditUserCircle> editUserCircle(
            @Header("Authorization") String Authorization, @Body Map<String, String> body);

    @POST("circle-user-delete")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> deleteMember(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST("meetings")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> todayMeetingCreateMember(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST("meeting/update")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> todayMeetingUpdateMember(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST("meeting/delete")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> todayMeetingDeleteMember(
            @Header("Authorization") String Authorization, @Query("meeting_id") Integer body);

    @GET("my-todays-meeting")
    @Headers("Content-Type: application/json")
    Call<TodayIMeetResponse> todayMeetingGetMember(
            @Header("Authorization") String Authorization);

    @POST("visits")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> todayVisitingCreateMember(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST("visit/update")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> todayVisitingUpdateMember(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);
    @POST("visit/delete")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> todayVisitingDeleteMember(
            @Header("Authorization") String Authorization, @Query("visit_id") Integer body);

    @GET("my-todays-visit")
    @Headers("Content-Type: application/json")
    Call<TodayIVisitResponse> todayVisitingGetMember(
            @Header("Authorization") String Authorization);

    @GET("statistics")
    @Headers("Content-Type: application/json")
    Call<GetStatistics> getStatistics(
            @Header("Authorization") String Authorization);

    @GET("notifications")
    @Headers("Content-Type: application/json")
    Call<GetNotification> getNotifications(
            @Header("Authorization") String Authorization);
    @GET("protocols/index")
    @Headers("Content-Type: application/json")
    Call<ProtocolResponse> getProtocols(
            @Header("Authorization") String Authorization);


    @POST("observations/store")
    @Headers("Content-Type: application/json")
    Call<GeneralResponse> createObservation(
            @Header("Authorization") String Authorization, @Body Map<String, Object> body);

    @GET("observations/index")
    @Headers("Content-Type: application/json")
    Call<ObservationListResponse> getObservationList(
            @Header("Authorization") String Authorization);


    @GET("get-config/observation_type")
    @Headers("Content-Type: application/json")
    Call<ObservationTypeResponse> getObservationType();





    @GET("get-config/observation_to")
    @Headers("Content-Type: application/json")
    Call<ObservationTypeResponse> getObservationTo();




    @GET("get-config/observation_tag")
    @Headers("Content-Type: application/json")
    Call<ObservationTypeResponse> getObservationTag();





}
