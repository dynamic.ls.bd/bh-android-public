
package com.ICMSoft.BH_Health.data.model.SelfQuarantine;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("activity")
    @Expose
    private List<String> activity = null;
    @SerializedName("sufferings")
    @Expose
    private List<String> sufferings = null;
    @SerializedName("meetings")
    @Expose
    private Object meetings;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<String> getActivity() {
        return activity;
    }

    public void setActivity(List<String> activity) {
        this.activity = activity;
    }

    public List<String> getSufferings() {
        return sufferings;
    }

    public void setSufferings(List<String> sufferings) {
        this.sufferings = sufferings;
    }

    public Object getMeetings() {
        return meetings;
    }

    public void setMeetings(Object meetings) {
        this.meetings = meetings;
    }

}
