
package com.ICMSoft.BH_Health.data.model.Circle.addCircle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Error {

    @SerializedName("name")
    @Expose
    private List<String> name = null;
    @SerializedName("circle")
    @Expose
    private List<String> circle = null;
    @SerializedName("phone")
    @Expose
    private List<String> phone = null;

    public List<String> getName() {
        return name;
    }

    public void setName(List<String> name) {
        this.name = name;
    }

    public List<String> getCircle() {
        return circle;
    }

    public void setCircle(List<String> circle) {
        this.circle = circle;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

}
