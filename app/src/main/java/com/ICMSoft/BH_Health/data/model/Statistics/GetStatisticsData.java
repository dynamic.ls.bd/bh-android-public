
package com.ICMSoft.BH_Health.data.model.Statistics;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetStatisticsData {

    @SerializedName("total_case")
    @Expose
    private Integer totalCase;
    @SerializedName("death")
    @Expose
    private Integer death;
    @SerializedName("recovered")
    @Expose
    private Integer recovered;
    @SerializedName("currently_sick")
    @Expose
    private Integer currentlySick;
    @SerializedName("today_case")
    @Expose
    private Integer todayCase;
    @SerializedName("total_quarantine")
    @Expose
    private Integer totalQuarantine;
    @SerializedName("stats_data")
    @Expose
    private List<StatsDatum> statsData = null;

    public Integer getTotalCase() {
        return totalCase;
    }

    public void setTotalCase(Integer totalCase) {
        this.totalCase = totalCase;
    }

    public Integer getDeath() {
        return death;
    }

    public void setDeath(Integer death) {
        this.death = death;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getCurrentlySick() {
        return currentlySick;
    }

    public void setCurrentlySick(Integer currentlySick) {
        this.currentlySick = currentlySick;
    }

    public Integer getTodayCase() {
        return todayCase;
    }

    public void setTodayCase(Integer todayCase) {
        this.todayCase = todayCase;
    }

    public Integer getTotalQuarantine() {
        return totalQuarantine;
    }

    public void setTotalQuarantine(Integer totalQuarantine) {
        this.totalQuarantine = totalQuarantine;
    }

    public List<StatsDatum> getStatsData() {
        return statsData;
    }

    public void setStatsData(List<StatsDatum> statsData) {
        this.statsData = statsData;
    }

}
