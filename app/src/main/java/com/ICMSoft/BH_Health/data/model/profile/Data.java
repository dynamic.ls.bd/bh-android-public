
package com.ICMSoft.BH_Health.data.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("firebase_token")
    @Expose
    private String firebaseToken;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("blood_group")
    @Expose
    private Object bloodGroup;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("returned_from_abroad")
    @Expose
    private String returnedFromAbroad;
    @SerializedName("upazila_id")
    @Expose
    private Object upazilaId;
    @SerializedName("upazila_name")
    @Expose
    private Object upazilaName;
    @SerializedName("country_id")
    @Expose
    private Object countryId;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("returned_date")
    @Expose
    private Object returnedDate;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("job_address")
    @Expose
    private Object jobAddress;
    @SerializedName("diabetes")
    @Expose
    private Object diabetes;
    @SerializedName("heart_disease")
    @Expose
    private Object heartDisease;
    @SerializedName("kidney_disease")
    @Expose
    private Object kidneyDisease;
    @SerializedName("respiratory_disease")
    @Expose
    private Object respiratoryDisease;
    @SerializedName("others")
    @Expose
    private Object others;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(Object bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getReturnedFromAbroad() {
        return returnedFromAbroad;
    }

    public void setReturnedFromAbroad(String returnedFromAbroad) {
        this.returnedFromAbroad = returnedFromAbroad;
    }

    public Object getUpazilaId() {
        return upazilaId;
    }

    public void setUpazilaId(Object upazilaId) {
        this.upazilaId = upazilaId;
    }

    public Object getUpazilaName() {
        return upazilaName;
    }

    public void setUpazilaName(Object upazilaName) {
        this.upazilaName = upazilaName;
    }

    public Object getCountryId() {
        return countryId;
    }

    public void setCountryId(Object countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Object getReturnedDate() {
        return returnedDate;
    }

    public void setReturnedDate(Object returnedDate) {
        this.returnedDate = returnedDate;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Object getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(Object jobAddress) {
        this.jobAddress = jobAddress;
    }

    public Object getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(Object diabetes) {
        this.diabetes = diabetes;
    }

    public Object getHeartDisease() {
        return heartDisease;
    }

    public void setHeartDisease(Object heartDisease) {
        this.heartDisease = heartDisease;
    }

    public Object getKidneyDisease() {
        return kidneyDisease;
    }

    public void setKidneyDisease(Object kidneyDisease) {
        this.kidneyDisease = kidneyDisease;
    }

    public Object getRespiratoryDisease() {
        return respiratoryDisease;
    }

    public void setRespiratoryDisease(Object respiratoryDisease) {
        this.respiratoryDisease = respiratoryDisease;
    }

    public Object getOthers() {
        return others;
    }

    public void setOthers(Object others) {
        this.others = others;
    }

}
