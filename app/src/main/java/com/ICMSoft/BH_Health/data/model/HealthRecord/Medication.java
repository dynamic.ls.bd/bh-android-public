
package com.ICMSoft.BH_Health.data.model.HealthRecord;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Medication {

    @SerializedName("noon")
    @Expose
    private Map<String,Integer> noon;
    @SerializedName("evening")
    @Expose
    private Map<String,Integer> evening;
    @SerializedName("morning")
    @Expose
    private Map<String,Integer> morning;

    public Map<String, Integer> getNoon() {
        return noon;
    }

    public void setNoon(Map<String, Integer> noon) {
        this.noon = noon;
    }

    public Map<String, Integer> getEvening() {
        return evening;
    }

    public void setEvening(Map<String, Integer> evening) {
        this.evening = evening;
    }

    public Map<String, Integer> getMorning() {
        return morning;
    }

    public void setMorning(Map<String, Integer> morning) {
        this.morning = morning;
    }
}
