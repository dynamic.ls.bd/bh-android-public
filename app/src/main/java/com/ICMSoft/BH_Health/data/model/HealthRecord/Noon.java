
package com.ICMSoft.BH_Health.data.model.HealthRecord;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Noon {

    @SerializedName("Napa")
    @Expose
    private Integer napa;

    public Integer getNapa() {
        return napa;
    }

    public void setNapa(Integer napa) {
        this.napa = napa;
    }

}
