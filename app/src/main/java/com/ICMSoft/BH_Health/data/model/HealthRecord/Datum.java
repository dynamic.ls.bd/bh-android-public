
package com.ICMSoft.BH_Health.data.model.HealthRecord;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("diastolic")
    @Expose
    private Integer diastolic;
    @SerializedName("systolic")
    @Expose
    private Integer systolic;
    @SerializedName("pulse")
    @Expose
    private Integer pulse;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("temperature")
    @Expose
    private String temperature;
    @SerializedName("cough")
    @Expose
    private String cough;
    @SerializedName("sneezing")
    @Expose
    private String sneezing;
    @SerializedName("breathing")
    @Expose
    private String breathing;
    @SerializedName("pain")
    @Expose
    private List<String> pain = null;
    @SerializedName("hand_wash")
    @Expose
    private String handWash;
    @SerializedName("medication")
    @Expose
    private Medication medication;
    @SerializedName("food_diets")
    @Expose
    private String foodDiets;
    @SerializedName("sleeping_hour")
    @Expose
    private Integer sleepingHour;
    @SerializedName("activities")
    @Expose
    private String activities;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(Integer diastolic) {
        this.diastolic = diastolic;
    }

    public Integer getSystolic() {
        return systolic;
    }

    public void setSystolic(Integer systolic) {
        this.systolic = systolic;
    }

    public Integer getPulse() {
        return pulse;
    }

    public void setPulse(Integer pulse) {
        this.pulse = pulse;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCough() {
        return cough;
    }

    public void setCough(String cough) {
        this.cough = cough;
    }

    public String getSneezing() {
        return sneezing;
    }

    public void setSneezing(String sneezing) {
        this.sneezing = sneezing;
    }

    public String getBreathing() {
        return breathing;
    }

    public void setBreathing(String breathing) {
        this.breathing = breathing;
    }

    public List<String> getPain() {
        return pain;
    }

    public void setPain(List<String> pain) {
        this.pain = pain;
    }

    public String getHandWash() {
        return handWash;
    }

    public void setHandWash(String handWash) {
        this.handWash = handWash;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public String getFoodDiets() {
        return foodDiets;
    }

    public void setFoodDiets(String foodDiets) {
        this.foodDiets = foodDiets;
    }

    public Integer getSleepingHour() {
        return sleepingHour;
    }

    public void setSleepingHour(Integer sleepingHour) {
        this.sleepingHour = sleepingHour;
    }

    public String getActivities() {
        return activities;
    }

    public void setActivities(String activities) {
        this.activities = activities;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
