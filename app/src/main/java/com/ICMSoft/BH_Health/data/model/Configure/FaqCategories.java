
package com.ICMSoft.BH_Health.data.model.Configure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaqCategories {

    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("suggestion")
    @Expose
    private String suggestion;
    @SerializedName("contribution")
    @Expose
    private String contribution;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getContribution() {
        return contribution;
    }

    public void setContribution(String contribution) {
        this.contribution = contribution;
    }

}
