
package com.ICMSoft.BH_Health.data.model.HealthRecord;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Evening {

    @SerializedName("Napa")
    @Expose
    private Integer napa;
    @SerializedName("Seclo")
    @Expose
    private Integer seclo;

    public Integer getNapa() {
        return napa;
    }

    public void setNapa(Integer napa) {
        this.napa = napa;
    }

    public Integer getSeclo() {
        return seclo;
    }

    public void setSeclo(Integer seclo) {
        this.seclo = seclo;
    }

}
