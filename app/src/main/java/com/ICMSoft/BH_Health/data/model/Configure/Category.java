
package com.ICMSoft.BH_Health.data.model.Configure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("family")
    @Expose
    private String family;
    @SerializedName("friend")
    @Expose
    private String friend;
    @SerializedName("related")
    @Expose
    private String related;
    @SerializedName("colleague")
    @Expose
    private String colleague;

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getFriend() {
        return friend;
    }

    public void setFriend(String friend) {
        this.friend = friend;
    }

    public String getRelated() {
        return related;
    }

    public void setRelated(String related) {
        this.related = related;
    }

    public String getColleague() {
        return colleague;
    }

    public void setColleague(String colleague) {
        this.colleague = colleague;
    }

}
