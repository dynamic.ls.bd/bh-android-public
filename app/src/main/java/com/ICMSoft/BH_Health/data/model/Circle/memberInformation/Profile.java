
package com.ICMSoft.BH_Health.data.model.Circle.memberInformation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Profile {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("upazila_id")
    @Expose
    private Object upazilaId;
    @SerializedName("returned_from_abroad")
    @Expose
    private Integer returnedFromAbroad;
    @SerializedName("rba_date")
    @Expose
    private Object rbaDate;
    @SerializedName("rb_country_id")
    @Expose
    private Object rbCountryId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("job_address")
    @Expose
    private String jobAddress;
    @SerializedName("diabetes")
    @Expose
    private Object diabetes;
    @SerializedName("heart_disease")
    @Expose
    private Object heartDisease;
    @SerializedName("kidney_disease")
    @Expose
    private Object kidneyDisease;
    @SerializedName("respiratory_disease")
    @Expose
    private Object respiratoryDisease;
    @SerializedName("others")
    @Expose
    private Object others;
    @SerializedName("dob")
    @Expose
    private Object dob;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private Object lat;
    @SerializedName("lng")
    @Expose
    private Object lng;
    @SerializedName("blood_group")
    @Expose
    private Object bloodGroup;
    @SerializedName("gender")
    @Expose
    private Boolean gender;
    @SerializedName("age")
    @Expose
    private String age;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getUpazilaId() {
        return upazilaId;
    }

    public void setUpazilaId(Object upazilaId) {
        this.upazilaId = upazilaId;
    }

    public Integer getReturnedFromAbroad() {
        return returnedFromAbroad;
    }

    public void setReturnedFromAbroad(Integer returnedFromAbroad) {
        this.returnedFromAbroad = returnedFromAbroad;
    }

    public Object getRbaDate() {
        return rbaDate;
    }

    public void setRbaDate(Object rbaDate) {
        this.rbaDate = rbaDate;
    }

    public Object getRbCountryId() {
        return rbCountryId;
    }

    public void setRbCountryId(Object rbCountryId) {
        this.rbCountryId = rbCountryId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public Object getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(Object diabetes) {
        this.diabetes = diabetes;
    }

    public Object getHeartDisease() {
        return heartDisease;
    }

    public void setHeartDisease(Object heartDisease) {
        this.heartDisease = heartDisease;
    }

    public Object getKidneyDisease() {
        return kidneyDisease;
    }

    public void setKidneyDisease(Object kidneyDisease) {
        this.kidneyDisease = kidneyDisease;
    }

    public Object getRespiratoryDisease() {
        return respiratoryDisease;
    }

    public void setRespiratoryDisease(Object respiratoryDisease) {
        this.respiratoryDisease = respiratoryDisease;
    }

    public Object getOthers() {
        return others;
    }

    public void setOthers(Object others) {
        this.others = others;
    }

    public Object getDob() {
        return dob;
    }

    public void setDob(Object dob) {
        this.dob = dob;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getLat() {
        return lat;
    }

    public void setLat(Object lat) {
        this.lat = lat;
    }

    public Object getLng() {
        return lng;
    }

    public void setLng(Object lng) {
        this.lng = lng;
    }

    public Object getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(Object bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
