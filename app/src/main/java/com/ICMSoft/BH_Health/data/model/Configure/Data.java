
package com.ICMSoft.BH_Health.data.model.Configure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("faq_categories")
    @Expose
    private FaqCategories faqCategories;
    @SerializedName("message_type")
    @Expose
    private MessageType messageType;
    @SerializedName("affect_type")
    @Expose
    private AffectType affectType;
    @SerializedName("pain_type")
    @Expose
    private PainType painType;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public FaqCategories getFaqCategories() {
        return faqCategories;
    }

    public void setFaqCategories(FaqCategories faqCategories) {
        this.faqCategories = faqCategories;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public AffectType getAffectType() {
        return affectType;
    }

    public void setAffectType(AffectType affectType) {
        this.affectType = affectType;
    }

    public PainType getPainType() {
        return painType;
    }

    public void setPainType(PainType painType) {
        this.painType = painType;
    }

}
