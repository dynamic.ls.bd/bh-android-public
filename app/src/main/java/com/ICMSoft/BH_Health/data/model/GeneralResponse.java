package com.ICMSoft.BH_Health.data.model;

public class GeneralResponse {
    private boolean Success;
    private  String message;

    public GeneralResponse(boolean Success, String message) {
        this.Success = Success;
        this.message = message;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
