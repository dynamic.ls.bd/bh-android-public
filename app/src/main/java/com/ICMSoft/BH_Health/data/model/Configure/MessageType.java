
package com.ICMSoft.BH_Health.data.model.Configure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageType {

    @SerializedName("voice")
    @Expose
    private String voice;
    @SerializedName("text")
    @Expose
    private String text;

    public String getVoice() {
        return voice;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
