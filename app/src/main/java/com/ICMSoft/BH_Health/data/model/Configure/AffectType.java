
package com.ICMSoft.BH_Health.data.model.Configure;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AffectType {

    @SerializedName("infected")
    @Expose
    private String infected;
    @SerializedName("deceased")
    @Expose
    private String deceased;
    @SerializedName("sick")
    @Expose
    private String sick;
    @SerializedName("recovered")
    @Expose
    private String recovered;

    public String getInfected() {
        return infected;
    }

    public void setInfected(String infected) {
        this.infected = infected;
    }

    public String getDeceased() {
        return deceased;
    }

    public void setDeceased(String deceased) {
        this.deceased = deceased;
    }

    public String getSick() {
        return sick;
    }

    public void setSick(String sick) {
        this.sick = sick;
    }

    public String getRecovered() {
        return recovered;
    }

    public void setRecovered(String recovered) {
        this.recovered = recovered;
    }

}
