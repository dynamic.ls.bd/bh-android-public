
package com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodayIMeet {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("your_name")
    @Expose
    private String yourName;
    @SerializedName("person_name")
    @Expose
    private String personName;
    @SerializedName("person_mobile")
    @Expose
    private String personMobile;
    @SerializedName("meeting_time")
    @Expose
    private String meetingTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYourName() {
        return yourName;
    }

    public void setYourName(String yourName) {
        this.yourName = yourName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonMobile() {
        return personMobile;
    }

    public void setPersonMobile(String personMobile) {
        this.personMobile = personMobile;
    }

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

}
