package com.ICMSoft.BH_Health.data.model.beSafe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.ICMSoft.BH_Health.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class PicassoImageGetter implements Html.ImageGetter {

    private ImageView imageView = null;
    private Context context;
    private String image;
    private Boolean flag;
    private int width;

    public void setWidth(int width) {
        this.width = width;
    }

    public PicassoImageGetter() {

    }

    public PicassoImageGetter(Context context, ImageView target, String image) {
        imageView = target;
        this.context =context;
        this.image = image;

    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    @Override
    public Drawable getDrawable(String source) {
        BitmapDrawablePlaceHolder drawable = new BitmapDrawablePlaceHolder();
        if(flag)
        Picasso.get()
                .load(source)

                .into(drawable);
        else
            Picasso.get()
                    .load(source)
                    .fit()

                    .into(imageView);

        return drawable;
    }

    private class BitmapDrawablePlaceHolder extends BitmapDrawable implements Target {

        protected Drawable drawable;

        @Override
        public void draw(final Canvas canvas) {
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            Log.d("imagecheck", "setDrawable: "+width+"  "+height);
            drawable.setBounds(0, 0, width, height);

            setBounds(0, 0, width, height);
            if (imageView != null) {
              //  imageView.setImageBitmap(drawable.);
            }


        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
           // setDrawable(new BitmapDrawable(context.getResources(), bitmap));
            imageView.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }


        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

    }
}
