
package com.ICMSoft.BH_Health.data.model.Statistics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatsDatum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bn_name")
    @Expose
    private String bnName;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;
    @SerializedName("quarantine_web")
    @Expose
    private Object quarantineWeb;
    @SerializedName("quarantine_app")
    @Expose
    private Object quarantineApp;
    @SerializedName("total_case")
    @Expose
    private Integer totalCase;
    @SerializedName("death")
    @Expose
    private Integer death;
    @SerializedName("recovered")
    @Expose
    private Integer recovered;
    @SerializedName("currently_sick")
    @Expose
    private Integer currentlySick;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBnName() {
        return bnName;
    }

    public void setBnName(String bnName) {
        this.bnName = bnName;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getQuarantineWeb() {
        return quarantineWeb;
    }

    public void setQuarantineWeb(Object quarantineWeb) {
        this.quarantineWeb = quarantineWeb;
    }

    public Object getQuarantineApp() {
        return quarantineApp;
    }

    public void setQuarantineApp(Object quarantineApp) {
        this.quarantineApp = quarantineApp;
    }

    public Integer getTotalCase() {
        return totalCase;
    }

    public void setTotalCase(Integer totalCase) {
        this.totalCase = totalCase;
    }

    public Integer getDeath() {
        return death;
    }

    public void setDeath(Integer death) {
        this.death = death;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getCurrentlySick() {
        return currentlySick;
    }

    public void setCurrentlySick(Integer currentlySick) {
        this.currentlySick = currentlySick;
    }

}
