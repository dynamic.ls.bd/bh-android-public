
package com.ICMSoft.BH_Health.data.model.Circle.editCircle;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot {

    @SerializedName("circle_id")
    @Expose
    private Integer circleId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("relation")
    @Expose
    private Object relation;
    @SerializedName("isAdmin")
    @Expose
    private Integer isAdmin;

    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getRelation() {
        return relation;
    }

    public void setRelation(Object relation) {
        this.relation = relation;
    }

    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

}
