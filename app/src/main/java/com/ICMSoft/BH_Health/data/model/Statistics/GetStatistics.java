
package com.ICMSoft.BH_Health.data.model.Statistics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetStatistics {

    @SerializedName("Success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private GetStatisticsData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetStatisticsData getData() {
        return data;
    }

    public void setData(GetStatisticsData data) {
        this.data = data;
    }

}
