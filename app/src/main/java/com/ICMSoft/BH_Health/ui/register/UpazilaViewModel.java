package com.ICMSoft.BH_Health.ui.register;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Register.Upazila.Datum;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.UpazilaListResponse;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpazilaViewModel extends ViewModel {
    private static final String TAG = "UpazilaViewModel";
    private MutableLiveData<List<Datum>> mUpazilaLiveData;
    public UpazilaViewModel() {
        mUpazilaLiveData = new MutableLiveData<>();
    }
    public LiveData<List<Datum>> getUpazilaResponse() {
        return mUpazilaLiveData;
    }
    public void getUpazila(Call<UpazilaListResponse> responseCall) {
        Log.d(TAG, "news: "+responseCall);
        responseCall.enqueue(new Callback<UpazilaListResponse>() {
            @Override
            public void onResponse(Call<UpazilaListResponse> call, Response<UpazilaListResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                List<Datum> data = response.body().getData();
                Log.d(TAG, "onResponse: "+data);
                mUpazilaLiveData.setValue(data);

            }

            @Override
            public void onFailure(Call<UpazilaListResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
}
