package com.ICMSoft.BH_Health.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.ICMSoft.BH_Health.MainActivity;
import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.Services.ApiService;
import com.ICMSoft.BH_Health.Services.RetrofitClient;
import com.ICMSoft.BH_Health.data.model.Auth.Data;
import com.ICMSoft.BH_Health.data.model.Auth.LoginResponse;
import com.ICMSoft.BH_Health.ui.register.RegisterActivity;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    public static final String UserPref = "UserData";
    public static final String USER_PHONE = "user_phone";
    public static final String USER_EMAIL = "user_email";
    public static final String ROLE = "role";

    public static final String USER_NAME = "UserName";
    public static final String AccessToken = "accessToken";
    public static final String USER_FIREBASE_TOKEN = "fi4cQlK9yjV6uvylDLqaM4:APA91bE0T-TZ0Y90X0AklZJ6i95BxMozQ6kS4JI48PHHi21_aPQxZIMVGCN0EaF-d9UtFacQobYQ0j-zruKcdlNYAXiZpxvkYNkZV36751f8V01k6BF3fx9snyC3SfTjSDlev42pSDrV";
    public static final String Language = "My_Language";
    private SharedPreferences sharedpreferences;

    private ApiService apiService;
    private LoginViewModel loginViewModel;

    @BindView(R.id.login_loading_view)
    LinearLayout loginLoading;
    @BindView(R.id.passlayout)
    TextInputLayout passlayout;

    @BindView(R.id.loginlayout)
    LinearLayout loginActivity;


    @BindView(R.id.login_view)
    ScrollView loginView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;



    @BindView(R.id.username)
    TextInputEditText usernameEditText;
    @BindView(R.id.password)
    TextInputEditText passwordEditText;
    @BindView(R.id.no_network_news_view)
    RelativeLayout noNetworkHealthView;
    @BindView(R.id.no_network_view)
    LottieAnimationView noNetwork;

    @BindView(R.id.ln_button1)
    ToggleSwitch changer;


    Timer timer;
    TimerTask task;
    Handler handler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getSharedPreferences(UserPref, Context.MODE_PRIVATE);
        if (sharedpreferences.getString(Language, "bn").compareToIgnoreCase("bn") == 0) {
            setLocal("bn");
        }
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        if (sharedpreferences.getString(Language, "bn").compareToIgnoreCase("en") == 0) {
            changer.setCheckedTogglePosition(1);
        }
        Objects.requireNonNull(getSupportActionBar()).hide();

        loginActivity.setOnClickListener(null);

        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordEditText.getText().toString().length() <8)
                    passlayout.setError(getResources().getString(R.string.pass_length));
                else
                    passlayout.setErrorEnabled(false);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        changer.setOnToggleSwitchChangeListener((position, isChecked) -> {
            String lang = "";
            if (changer.getCheckedTogglePosition() == 0)
                lang += "bn";
            else
                lang += "en";
            if (lang.compareToIgnoreCase(sharedpreferences.getString(Language, "")) != 0) {
                setLocal(lang);
                refreshActivity();
            }
        });


        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);
        apiService = RetrofitClient.getRetrofitClient(getApplicationContext()).create(ApiService.class);
        sharedpreferences = getSharedPreferences(UserPref, Context.MODE_PRIVATE);

        loginViewModel.getLoginResult().observe(this, loginResult -> {
            if (loginResult == null) {
                return;
            }
            if (loginResult.getError() != null)
                showLoginFailed();
            if (loginResult.getSuccess() != null)
                updateUiWithUser();

            setResult(Activity.RESULT_OK);
        });
        isAlreadyLogged();
    }


    private void startLoading() {
        loginView.setVisibility(View.GONE);
        loginLoading.setVisibility(View.VISIBLE);
        animationView.playAnimation();
        startTimer();
    }

    private void stopLoading() {
        loginView.setVisibility(View.VISIBLE);
        loginLoading.setVisibility(View.GONE);
        animationView.cancelAnimation();
        stopTimer();
    }

    private void setTask() {
        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    loginView.setVisibility(View.GONE);
                    loginLoading.setVisibility(View.GONE);
                    animationView.cancelAnimation();
                    noNetworkHealthView.setVisibility(View.VISIBLE);
                    noNetwork.playAnimation();
                    Objects.requireNonNull(getSupportActionBar()).show();
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                });
            }
        };
    }

    private void startTimer() {
        timer = new Timer();
        setTask();
        timer.schedule(task, 20000);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            noNetwork.cancelAnimation();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Objects.requireNonNull(getSupportActionBar()).hide();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        noNetwork.cancelAnimation();
        noNetworkHealthView.setVisibility(View.GONE);
        stopLoading();
        return true;
    }

    private boolean userCheck(String user) {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(user).matches()) {
            return true;
        } else if (user.isEmpty()) {
            usernameEditText.setError("Cannot Empty");
            return false;
        } else if (user.length() == 11) {

            return true;
        } else {
            usernameEditText.setError("Must be given email or mobile number");
            return false;
        }
    }


    private boolean passwordCheck(String pass) {
        if (pass.length() < 8) {
            passwordEditText.setError("Must be at least 8 character");
            return false;
        }
        return true;
    }





    @OnClick(R.id.login)
    public void logIn() {

        if (!userCheck(usernameEditText.getText().toString()))
            return;

        if (!passwordCheck(passwordEditText.getText().toString()))
            return;
        Map<String, String> params = new HashMap<>();
        params.put("email", usernameEditText.getText().toString());
        params.put("password", passwordEditText.getText().toString());
        params.put("firebase_token", USER_FIREBASE_TOKEN);
        startLoading();
        final Call<LoginResponse> loginCall = apiService.login(params);
        loginViewModel.login(loginCall);
    }


    private void isAlreadyLogged() {
        String token = sharedpreferences.getString(AccessToken, null);
        if (token != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }
    }


    private void updateUiWithUser() {
        loginViewModel.getLoginResponse().observe(this, loginResponse -> {
            if (loginResponse == null) {
                stopLoading();
                return;
            }
            if (loginResponse.getToken() == null) {
                stopLoading();
                Toast.makeText(LoginActivity.this, "Incorrect username/password", Toast.LENGTH_SHORT).show();
                return;
            }
            Log.d(TAG, "getLoginResponse: ");
            Data data = loginResponse.getData();
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(AccessToken, loginResponse.getToken());
            editor.putString(USER_NAME, data.getName());
            editor.putString(USER_EMAIL, data.getEmail());
            editor.putString(USER_PHONE, data.getPhone());
            editor.putString(ROLE, data.getRole());
            editor.apply();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        });


    }

    private void showLoginFailed() {
        stopLoading();
        Toast.makeText(getApplicationContext(), "Incorrect username/password", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_register)
    public void register() {
        Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(i);
        finish();
    }


    @OnClick(R.id.forgot_password)
    public void forgetPasswordPressed() {
        Intent i = new Intent(LoginActivity.this, Reset_Password.class);
        startActivity(i);
    }


    private void setLocal(String locale) {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            config.setLocale(new Locale(locale.toLowerCase()));
        else
            config.locale = new Locale(locale.toLowerCase());
        resources.updateConfiguration(config, dm);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Language, locale);
        editor.apply();
    }

    private void refreshActivity() {
        Activity mCurrentActivity = this;
        Intent intent = this.getIntent();
        mCurrentActivity.finish();
        mCurrentActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        mCurrentActivity.startActivity(intent);
    }


}
