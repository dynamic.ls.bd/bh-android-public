package com.ICMSoft.BH_Health.ui.beSafe;

import android.app.Activity;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.beSafe.Catagory.BeSafeCatagory;
import com.ICMSoft.BH_Health.data.model.protocol.ProtocolDatum;
import com.ICMSoft.BH_Health.data.model.protocol.ProtocolResponse;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.beSafe.BeSafeResponse;
import com.ICMSoft.BH_Health.data.model.beSafe.Datum;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeSafeViewModel extends ViewModel {
    private static final String TAG = "BeSage";
    private MutableLiveData<List<Datum>> data;
    private MutableLiveData<BeSafeCatagory> catagory;
    private MutableLiveData<List<ProtocolDatum>> protocolsData;
    private MutableLiveData<ProtocolResponse> protocolDescriptionData;
    private Activity activity;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public BeSafeViewModel() {
        data = new MutableLiveData<>();
        catagory = new MutableLiveData<>();
        protocolsData = new MutableLiveData<>();
        protocolDescriptionData = new MutableLiveData<>();
    }


    MutableLiveData<List<Datum>> getBeSafeData() {
        return data;
    }
    MutableLiveData<BeSafeCatagory> getBeSafeCatagoryData() {
        return catagory;
    }


    public MutableLiveData<List<ProtocolDatum>> getProtocolsData() {
        return protocolsData;
    }

    public MutableLiveData<ProtocolResponse> getProtocolDescriptionData() {
        return protocolDescriptionData;
    }

    public void getProtocols(Call<ProtocolResponse> responseCall) {

        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<ProtocolResponse>() {
            @Override
            public void onResponse(Call<ProtocolResponse> call, Response<ProtocolResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                if(response.body().getData()!=null)
                    protocolsData.setValue(response.body().getData());
                else
                    protocolsData.setValue(null);





            }

            @Override
            public void onFailure(Call<ProtocolResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }

    public void getBesafe(Call<BeSafeResponse> responseCall) {

        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<BeSafeResponse>() {
            @Override
            public void onResponse(Call<BeSafeResponse> call, Response<BeSafeResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                if(response.body().getData()!=null)
                data.setValue(response.body().getData());
                else
                    data.setValue(null);





            }

            @Override
            public void onFailure(Call<BeSafeResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void getBesafeCatagory(Call<BeSafeCatagory> responseCall) {

        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<BeSafeCatagory>() {
            @Override
            public void onResponse(Call<BeSafeCatagory> call, Response<BeSafeCatagory> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                catagory.setValue(response.body());

            }

            @Override
            public void onFailure(Call<BeSafeCatagory> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }






}
