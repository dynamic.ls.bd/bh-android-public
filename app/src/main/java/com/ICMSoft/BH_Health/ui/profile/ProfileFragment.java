package com.ICMSoft.BH_Health.ui.profile;


import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryData;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryListResponse;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.Datum;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.UpazilaListResponse;
import com.ICMSoft.BH_Health.data.model.profile.ProfileResponse;
import com.ICMSoft.BH_Health.ui.Utils.DatePickerCalander;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.register.CountryViewModel;
import com.ICMSoft.BH_Health.ui.register.UpazilaViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private static final String TAG = "ProfileFragment";
    private ProfileViewModel profileViewModel;
    private GeneralHelper mGeneralHelper;
    private UpazilaViewModel upazilaViewModel;
    private CountryViewModel countryViewModel;

    @BindView(R.id.return_from_aboard)
    Spinner return_from_aboard;

    @BindView(R.id.return_date)
    TextView return_date;

    @BindView(R.id.country_name)
    AutoCompleteTextView country_name;
    @BindView(R.id.up_name)
    AutoCompleteTextView upazila_name;

    @BindView(R.id.etName)
    EditText usernameET;

    @BindView(R.id.etUserAge)
    EditText ageET;

    @BindView(R.id.etUserLocation)
    EditText locationET;

    @BindView(R.id.etContactNo)
    EditText contactET;

    @BindView(R.id.etOccupation)
    EditText occupationET;

    @BindView(R.id.etDiabetes)
    CheckBox diabeticET;

    @BindView(R.id.etHeartDisease)
    CheckBox heartDiseaseET;

    @BindView(R.id.etKidneyComplication)
    CheckBox kidneyComplicationET;

    @BindView(R.id.etRespiratoryProblem)
    CheckBox respiratoryProblemET;

    @BindView(R.id.etOthers)
    CheckBox othersET;
    @BindView(R.id.et_email)
    EditText emailET;
    @BindView(R.id.profile_View)
    ScrollView profileView;

    @BindView(R.id.male)
    RadioButton male;
    @BindView(R.id.female)
    RadioButton female;

    private ArrayAdapter<String> upazila_adapter;
    private ArrayAdapter<String> country_adapter;

    private Map<String, String> upazilaValue;
    private Map<String, String> countryValue;
    private Map<String, Integer> upazilaMap;
    private Map<String, Integer> countryMap;
    private String uid;
    private String cid;
    private String retdate;

    private Loading loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ButterKnife.bind(this, view);
        loading = new Loading(view, profileView);
        DatePickerCalander datePickerCalander = new DatePickerCalander(return_date,requireContext());
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        mGeneralHelper = new GeneralHelper(requireContext());
        upazilaViewModel = new ViewModelProvider(this).get(UpazilaViewModel.class);
        countryViewModel = new ViewModelProvider(this).get(CountryViewModel.class);
        profileViewModel.setContext(getContext());


        Log.d(TAG, "onCreateView: " + profileViewModel);


        ArrayList<String> UPAZILA = new ArrayList<>();
        ArrayList<String> COUNTRIES = new ArrayList<>();
        upazila_adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, UPAZILA);
        upazila_name.setAdapter(upazila_adapter);
        country_adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, COUNTRIES);
        country_name.setAdapter(country_adapter);

        return_date.setInputType(InputType.TYPE_NULL);
        return_date.setVisibility(View.GONE);
        country_name.setVisibility(View.GONE);
        upazilaValue = new HashMap<>();
        countryValue = new HashMap<>();
        upazilaMap = new HashMap<>();
        countryMap = new HashMap<>();
        uid = null;
        cid = null;
        retdate = null;

        getProfile();


        return view;
    }



    @OnTextChanged(R.id.up_name)
    void getUpazila() {
        upazilaMap.clear();
        upazilaValue.clear();
        upazilaValue.put("term", upazila_name.getText().toString());
        getUpazilaList();
    }

    @OnTextChanged(R.id.country_name)
    void getCountry() {
        countryMap.clear();
        countryValue.clear();
        countryValue.put("term", country_name.getText().toString());
        getCountryList();
    }

    private void getUpazilaList() {
        final Call<UpazilaListResponse> responseCall = mGeneralHelper.getApiService().getUpazilaList(upazilaValue);
        upazilaViewModel.getUpazila(responseCall);
        upazilaViewModel.getUpazilaResponse().observe(this, data -> {
            upazila_adapter.clear();
            if (data != null) {
                for (Datum d : data) {
                    upazilaMap.put(d.getName(), d.getId());
                    upazila_adapter.add(d.getName());
                }
            }
        });
    }

    private void getCountryList() {
        final Call<CountryListResponse> responseCall = mGeneralHelper.getApiService().getCountryList(countryValue);
        countryViewModel.getCountry(responseCall);
        countryViewModel.getCountryResponse().observe(this, data -> {
            country_adapter.clear();
            if (data != null) {
                for (CountryData d : data) {
                    countryMap.put(d.getName(), d.getId());
                    country_adapter.add(d.getName());
                }
            }
        });
    }

    @OnItemSelected(R.id.return_from_aboard)
    void returnFromAbroard(int position) {
        switch (position) {
            case 0:
                return_date.setVisibility(View.GONE);
                country_name.setVisibility(View.GONE);
                break;
            case 1:
                return_date.setVisibility(View.VISIBLE);
                country_name.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void getProfile() {
        loading.startLoading();
        Call<ProfileResponse> profileResponseCall = mGeneralHelper.getApiService().getProfile(mGeneralHelper.getToken());
        profileViewModel.getProfile(profileResponseCall);
        Log.d(TAG, "getProfile: ");

        profileViewModel.getProfileResponse().observe(getViewLifecycleOwner(), data -> {

            if (data == null)
                return;
            loading.stopLoading();
            usernameET.setText(data.getName());
            locationET.setText(data.getAddress());

            ageET.setText(data.getAge());
            contactET.setText(data.getPhone());
            occupationET.setText(data.getOccupation());
            emailET.setText(data.getEmail());
            if (data.getGender() != null) {
                if (data.getGender().compareToIgnoreCase("Male") == 0)
                    male.setChecked(true);
                if (data.getGender().compareToIgnoreCase("Female") == 0)
                    female.setChecked(true);
            }
            if (data.getDiabetes() != null) {
                if (data.getDiabetes().toString().compareToIgnoreCase("yes") == 0)
                    diabeticET.setChecked(true);
                else
                    diabeticET.setChecked(false);
            }
            if (data.getHeartDisease() != null) {
                if (data.getHeartDisease().toString().compareToIgnoreCase("yes") == 0)
                    heartDiseaseET.setChecked(true);
                else
                    heartDiseaseET.setChecked(false);
            }
            if (data.getRespiratoryDisease() != null) {
                if (data.getRespiratoryDisease().toString().compareToIgnoreCase("yes") == 0)
                    respiratoryProblemET.setChecked(true);
                else
                    respiratoryProblemET.setChecked(false);
            }
            if (data.getKidneyDisease() != null) {
                if (data.getKidneyDisease().toString().compareToIgnoreCase("yes") == 0)
                    kidneyComplicationET.setChecked(true);
                else
                    kidneyComplicationET.setChecked(false);
            }
            if (data.getOthers() != null) {
                if (data.getOthers().toString().compareToIgnoreCase("yes") == 0)
                    othersET.setChecked(true);
                else
                    othersET.setChecked(false);
            }

            if (data.getUpazilaId() != null) {
                upazila_name.setText(data.getUpazilaName().toString());
                if (data.getUpazilaId() != null)
                    uid = data.getUpazilaId().toString();
            }
            if (data.getReturnedFromAbroad() != null) {
                if (data.getReturnedFromAbroad().compareToIgnoreCase("yes") == 0) {

                    return_from_aboard.setSelection(1);
                    return_date.setVisibility(View.VISIBLE);
                    country_name.setVisibility(View.VISIBLE);
                    Log.d(TAG, "onChanged: ");
                    if (data.getReturnedDate() != null) {
                        return_date.setText(data.getReturnedDate().toString());
                        retdate = data.getReturnedDate().toString();
                    }
                    if (data.getCountryId() != null) {
                        country_name.setText(data.getCountryName());
                        if (data.getCountryId() != null)
                            cid = data.getCountryId().toString();
                    }
                }
            }

        });

    }


    @OnClick(R.id.btnSignUp)
    public void updateProfile() {
        Log.d(TAG, "updateProfile11: ");
        String name = usernameET.getText().toString();
        String email = emailET.getText().toString();
        String phone = contactET.getText().toString();
        String age = ageET.getText().toString();
        String location = locationET.getText().toString();
        String occupation = occupationET.getText().toString();
        String country_name = this.country_name.getText().toString();
        String return_date = this.return_date.getText().toString();
        String upazila_name = this.upazila_name.getText().toString();
        String return_from_aboard = this.return_from_aboard.getSelectedItem().toString();
        if (TextUtils.isEmpty(name)) {
            usernameET.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(phone)) {
            contactET.setError("Cannot be empty");
            return;
        }
        if (TextUtils.isEmpty(email)) {
            emailET.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(country_name) && return_from_aboard.compareToIgnoreCase("yes") == 0) {
            this.country_name.setError("Cannot be empty");
            return;
        }
        if (TextUtils.isEmpty(return_date) && return_from_aboard.compareToIgnoreCase("yes") == 0) {
            this.return_date.setError("Cannot be empty");
            return;
        }

        final Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("age", age);
        params.put("address", location);
        params.put("phone", phone);
        params.put("occupation", occupation);
        params.put("email", email);
        if (male.isChecked())
            params.put("gender", "1");
        if (female.isChecked())
            params.put("gender", "0");
        if (diabeticET.isChecked())
            params.put("diabetes", "yes");
        if (respiratoryProblemET.isChecked())
            params.put("respiratory_disease", "yes");
        if (heartDiseaseET.isChecked())
            params.put("heart_disease", "yes");
        if (othersET.isChecked())
            params.put("others", "yes");
        if (kidneyComplicationET.isChecked())
            params.put("kidney_disease", "yes");

        if (upazilaMap.get(upazila_name) == null) {
            this.upazila_name.setError("Upazila cannot found. Please select upazila from dropdown");
            return;
        }
        boolean aboard = return_from_aboard.compareToIgnoreCase("yes") == 0 || return_from_aboard.compareToIgnoreCase("হ্যাঁ") == 0;
        if (TextUtils.isEmpty(country_name) && aboard) {
            this.country_name.setError("Cannot be empty");
            return;
        }

        if (countryMap.get(country_name) == null && aboard) {
            this.country_name.setError("Country cannot found. Please select country from dropdown");
            return;
        }
        if (TextUtils.isEmpty(return_date) && aboard) {
            this.return_date.setError("Cannot be empty");
            return;
        }
        else if (uid != null)
            params.put("upazila_id", uid);
        if (return_from_aboard.compareToIgnoreCase("yes") == 0) {
            params.put("returned_from_abroad", "1");
            if (TextUtils.isEmpty(return_date)) {
                this.return_date.setError("Cannot be empty");
                return;
            }
            if (TextUtils.isEmpty(country_name)) {
                this.country_name.setError("Cannot be empty");
                return;
            }
            if (countryMap.get(country_name) != null)
                params.put("rb_country_id", countryMap.get(country_name));
            else if (cid != null)
                params.put("rb_country_id", cid);
            try {
                if (return_date.isEmpty())
                    if (retdate != null)
                        return_date = retdate;
                Date d = new SimpleDateFormat("dd/MM/yyyy").parse(return_date);
                params.put("rba_date", new SimpleDateFormat("yyyy-MM-dd").format(d));


            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else
            params.put("returned_from_abroad", "0");
        loading.startLoading();
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().updateProfile(mGeneralHelper.getToken(), params);
        profileViewModel.updateProfile(responseCall);

        profileViewModel.getGeneralResponse().observe(this, aBoolean -> {
            Log.d(TAG, "onChanged: " + aBoolean);
            if (aBoolean)
                Toast.makeText(getContext(), "Profile update Successfully", Toast.LENGTH_SHORT).show();
            loading.stopLoading();

        });
    }
}