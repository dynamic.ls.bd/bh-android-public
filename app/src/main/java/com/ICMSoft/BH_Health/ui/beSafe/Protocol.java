package com.ICMSoft.BH_Health.ui.beSafe;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.ProtocolAdapter;
import com.ICMSoft.BH_Health.data.model.protocol.ProtocolResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;



import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;


public class Protocol extends Fragment {
   @BindView(R.id.protocol_list)
    RecyclerView protocolList;
   @BindView(R.id.protocol_view)
    NestedScrollView protocolView;
    private BeSafeViewModel mViewModel;
    private GeneralHelper mGeneralHelper;
    private ProtocolAdapter protocolAdapter;
    private LinearLayoutManager linearLayoutManager;
    public static String protocolDocument;
    private Loading loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_protocol, container, false);
        ButterKnife.bind(this,view);
        mViewModel = new ViewModelProvider(this).get(BeSafeViewModel.class);
        mGeneralHelper = new GeneralHelper(requireContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        loading = new Loading(view,protocolView);
        getProtocols();

        return view;
    }
    private void getProtocols(){
        loading.startLoading();
        Call<ProtocolResponse> call = mGeneralHelper.getApiService().getProtocols(mGeneralHelper.getToken());
        mViewModel.getProtocols(call);

        mViewModel.getProtocolsData().observe(getViewLifecycleOwner(), data -> {
            if(data==null)
                return;
            loading.stopLoading();
            protocolAdapter = new ProtocolAdapter(data);
            protocolList.setAdapter(protocolAdapter);
            protocolList.setLayoutManager(linearLayoutManager);
            getActions();
        });
    }
    private void getActions(){
        protocolAdapter.setProtocolShowListener(documentNumber -> {
            protocolDocument = documentNumber;
            NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.protocolDetails);
        });
    }
}
