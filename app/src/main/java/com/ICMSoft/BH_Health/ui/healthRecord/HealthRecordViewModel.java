package com.ICMSoft.BH_Health.ui.healthRecord;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.HealthRecord.Datum;
import com.ICMSoft.BH_Health.data.model.HealthRecord.HealthRecordResponse;
import com.ICMSoft.BH_Health.data.model.HealthRecord.PainList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HealthRecordViewModel extends ViewModel {
    private static final String TAG = "HealthRecordViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<List<Datum>> mHealthRecordLiveData;
    private MutableLiveData<List<String>>getPainListData;
    private Context context;

    public HealthRecordViewModel() {
        mSuccessLiveData = new MutableLiveData<>();
        mHealthRecordLiveData = new MutableLiveData<>();
        Log.d(TAG, "HealthRecordViewModel: ");
        context = null;
        getPainListData = new MutableLiveData<>();
    }



    public LiveData<Boolean> getGeneralResponse() {
        return mSuccessLiveData;
    }
    public LiveData<List<String>> getPainListDataList() {
        return getPainListData;
    }

    public LiveData<List<Datum>> getHealthRecordResponse() {
        return mHealthRecordLiveData;
    }

    public  void getHealthRecord(Call<HealthRecordResponse> healthRecordResponseCall) {
        Log.d(TAG, "getHealthRecord: "+healthRecordResponseCall);

        healthRecordResponseCall.enqueue(new Callback<HealthRecordResponse>() {
            @Override
            public void onResponse(Call<HealthRecordResponse> call, Response<HealthRecordResponse> response) {
                if (response.code() != 200) {
                    Log.d(TAG, "onResponse11: "+response.code());
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                mHealthRecordLiveData.setValue(response.body().getData());
                Log.d(TAG, "onResponse11: "+response.code());
            }

            @Override
            public void onFailure(Call<HealthRecordResponse> call, Throwable t) {
                Log.d(TAG, "onFailure1: "+t.getMessage());
            }
        });

    }

    public void updateHealthRecord(Call<GeneralResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    Log.d(TAG, "onResponse: "+response.code());
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });


    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void crateeHealthRecord(Call<GeneralResponse> responseCall) {
        responseCall.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                Gson gson = new Gson();
              //  Log.d(TAG, "onResponse: "+response.code());
               // SuccessHealthRecord message=gson.fromJson(response.body()., SuccessHealthRecord.class);
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });
    }
    public void getPainList(Call<PainList> responseCall) {
        responseCall.enqueue(new Callback<PainList>() {
            @Override
            public void onResponse(Call<PainList> call, Response<PainList> response) {
                if (response.code() != 200) {
                    getPainListData.setValue(null);
                   // Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
               // Gson gson = new Gson();
              //  Log.d(TAG, "onResponse: "+response.code());
               // SuccessHealthRecord message=gson.fromJson(response.body()., SuccessHealthRecord.class);
                getPainListData.setValue(response.body().getData());
                //Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<PainList> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                getPainListData.setValue(null);
            }
        });
    }

}
