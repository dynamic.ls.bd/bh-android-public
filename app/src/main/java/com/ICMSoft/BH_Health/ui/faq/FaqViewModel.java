package com.ICMSoft.BH_Health.ui.faq;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.faq.Datum;
import com.ICMSoft.BH_Health.data.model.faq.FaqResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FaqViewModel extends ViewModel {

    private static final String TAG = "FaqViewModel";
    private MutableLiveData<GeneralResponse> questionCreate;
    private MutableLiveData<ArrayList<GeneralResponse>> questionList;
    private MutableLiveData<ArrayList<Datum>> faqList;


    public FaqViewModel() {
        questionCreate = new MutableLiveData<>();
        questionList = new MutableLiveData<>();
        faqList = new MutableLiveData<>();
    }


    MutableLiveData<ArrayList<Datum>> getFaqResponse() {
        return faqList;
    }
    MutableLiveData<GeneralResponse> getStoreQuestionResponse() {
        return questionCreate;
    }

    MutableLiveData<ArrayList<GeneralResponse>> getQuestionListResponse() {
        return questionList;
    }

    public void saveQuestion(Call<GeneralResponse> responseCall) {

        responseCall.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                   // loginResult.setValue(new LoginResult(R.string.login_failed));
                    questionCreate.setValue(new GeneralResponse(false, "Failed"));
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                questionCreate.setValue(new GeneralResponse(true, "Successful"));

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                questionCreate.setValue(new GeneralResponse(false, "Failed "+t.getMessage()));
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });

    }
    public void getFaq(Call<FaqResponse> responseCall) {

        responseCall.enqueue(new Callback<FaqResponse>() {
            @Override
            public void onResponse(Call<FaqResponse> call, Response<FaqResponse> response) {
                if (response.code() != 200) {
                    // loginResult.setValue(new LoginResult(R.string.login_failed));
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.body().getData().size());
                faqList.setValue(response.body().getData());

            }

            @Override
            public void onFailure(Call<FaqResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

}
