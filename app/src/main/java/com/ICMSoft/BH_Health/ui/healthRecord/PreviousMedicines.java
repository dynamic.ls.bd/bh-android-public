package com.ICMSoft.BH_Health.ui.healthRecord;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ICMSoft.BH_Health.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviousMedicines {


    private View view;
    private Context context;

    @BindView(R.id.show_medicine_name)
    TextView name;
    @BindView(R.id.show_medicine_quantity)
    TextView quantity;
    public PreviousMedicines(Context context) {
        this.context =context;
        view = LayoutInflater.from(context).inflate(R.layout.medicine_previous_items,null);
        ButterKnife.bind(this,view);
    }
    public View getView(){
        return view;
    }


}
