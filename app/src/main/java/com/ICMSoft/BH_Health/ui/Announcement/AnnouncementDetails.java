package com.ICMSoft.BH_Health.ui.Announcement;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.text.Html;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.News.NewsFragment.newsPosition;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnnouncementDetails extends Fragment {


    private static final String TAG = "AnnouncementDetailsFragment";
    private AnnouncementViewModel newsViewModel;
    private GeneralHelper mGeneralHelper;

    @BindView(R.id.details_news)
    TextView detailsNews;
    @BindView(R.id.title_details_news)
    TextView title;


    @BindView(R.id.news_details_view)
    ScrollView newsDetailsView;

    Loading loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_announcement_details, container, false);
        ButterKnife.bind(this,view);
        loading = new Loading(view,newsDetailsView);
        newsViewModel = new ViewModelProvider(this).get(AnnouncementViewModel.class);
        mGeneralHelper = new GeneralHelper(getContext());


        getData();
        return view;
    }

    private void getData() {
        loading.startLoading();
        Call<AnnouncementDetailsResponse> call = mGeneralHelper.getApiService().getAnnouncementDetails( mGeneralHelper.getToken(),"announcements/" + (newsPosition+1));
        newsViewModel.announcementDetails(call);

        newsViewModel.getAnnouncementDetailsResponse().observe(getViewLifecycleOwner(), new Observer<Data>() {
            @Override
            public void onChanged(Data datum) {
                if (datum == null) {
                    // titleTV.setText("");
                    // bodyTV.setText("No Data Found");
                    loading.stopLoading();
                    return;
                }
                loading.stopLoading();
                //   PicassoImageGetter imageGetter = new PicassoImageGetter(getContext(),detailsNews,datum.getImage());
                Spannable html;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    html = (Spannable) Html.fromHtml(datum.getContent(), Html.FROM_HTML_MODE_LEGACY,null ,null);
                } else {
                    html = (Spannable) Html.fromHtml(datum.getContent(), null, null);
                }

                detailsNews.setText(html);
                title.setText(datum.getTitle());
                //Log.d(TAG, "onChanged: "+datum.getTitle());


            }
        });
    }
}
