package com.ICMSoft.BH_Health.ui.selfQuarantine;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.ActivityQuarantineAdapter;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.SelfQuarantine.Datum;
import com.ICMSoft.BH_Health.data.model.SelfQuarantine.SelfQuarantineResponse;
import com.ICMSoft.BH_Health.ui.Utils.DatePickerCalander;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class SelfQuarantineFragmentModify extends Fragment  {



    @BindView(R.id.start_quarentine_layout)
    LinearLayout startQuarantineLayout;
    @BindView(R.id.activity_quarentine)
    LinearLayout activityQuarantineLayout;
    @BindView(R.id.date_field)
    TextView dateQuarantineStart;

    @BindView(R.id.self_quarantine_list)
    RecyclerView activitysList;


    @BindView(R.id.tvDate)
    TextView datetext;


    @BindView(R.id.self_quarantine_view)
    NestedScrollView selfQuarantineView;

    private static final String TAG = "SelfQuarantine1";
    private SelfQuarantineViewModel selfQuarantineViewModel;
    private GeneralHelper mGeneralHelper;

    private ActivityQuarantineAdapter activityQuarantineAdapter;

    private ArrayList<Datum> ds;
    private AlertDialog dialog;
    private SimpleDateFormat sd = new SimpleDateFormat("MMMM dd, yyyy");
    private SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM,yyyy");
    private SimpleDateFormat submitformat = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar calendar=Calendar.getInstance();
    private Date quarantineDayStartDate;
    private Date today = new Date();
    private Loading loading;
    private boolean dataCome = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.self_quarentine_modify, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view,selfQuarantineView);
        DatePickerCalander datePickerCalander = new DatePickerCalander(dateQuarantineStart, requireContext());
        selfQuarantineViewModel = new ViewModelProvider(this).get(SelfQuarantineViewModel.class);
        mGeneralHelper = new GeneralHelper(requireContext());
        selfQuarantineViewModel.setContext(requireContext());
        startQuarantineLayout.setVisibility(View.GONE);
        activityQuarantineLayout.setVisibility(View.GONE);
        Date date = new Date();
        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
        dateQuarantineStart.setText(sd.format(date));
        sd = new SimpleDateFormat("MMMM dd,yyyy");
        datetext.setText("Date: " + sd.format(date));

        quarantineDayStartDate = null;
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        activitysList.setLayoutManager(mLinearLayoutManager);

        getSelfQuarantine();
        return view;
    }




    public void create(final Map<String, Object> params) {
        Log.d(TAG, "create: " + params);
        selfQuarantineViewModel.setContext(getContext());
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().createSelfQuarantine(mGeneralHelper.getToken(), params);
        Log.d(TAG, "create: " + responseCall);
        selfQuarantineViewModel.createSelfQuarantine(responseCall);

        selfQuarantineViewModel.getGeneralResponse().observe(getViewLifecycleOwner(), aBoolean -> {
            Log.d(TAG, "onChanged11: " + aBoolean);
            if(!aBoolean)
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            else
            {
                dataCome = true;
                getSelfQuarantine();
            }
        });
    }
    private void updateQuarantine(final Map<String, Object> params) {
        Log.d(TAG, "create: " + params);
        selfQuarantineViewModel.setContext(getContext());
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().updateSelfQuarantine(mGeneralHelper.getToken(), params);
        Log.d(TAG, "create: " + responseCall);
        selfQuarantineViewModel.updateSelfQuarantine(responseCall);

        selfQuarantineViewModel.getGeneralResponse().observe(getViewLifecycleOwner(), aBoolean -> {
            Log.d(TAG, "onChanged11: " + aBoolean);
            if(!aBoolean)
                Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            else
               {dataCome = true;
                   getSelfQuarantine();
               }
        });
    }



    private int dayDifference(Date date) {
        Log.d(TAG, "dayDifference: "+date.getTime());
        int dayDiff = (int) ((today.getTime() - date.getTime())
                / (1000 * 60 * 60 * 24));
        Log.d(TAG, "dayDifference: yr");
        return dayDiff;
    }

    private Date addDay(Date date,int day){
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH,day);
        return calendar.getTime();
    }

    private boolean startToday = true;
    private void getSelfQuarantine() {
        loading.startLoading();
        ds = new ArrayList<>();
        for (int i = 0; i < 14; i++)
            ds.add(null);


            Call<SelfQuarantineResponse> selfQuarantineResponseCall = mGeneralHelper.getApiService().getSelfQuarantine(mGeneralHelper.getToken());
            selfQuarantineViewModel.getSelfQuarantine(selfQuarantineResponseCall);


            selfQuarantineViewModel.getSelfQuarantineResponse().observe(getViewLifecycleOwner(), data -> {
                int time = 14;
                if(dataCome) {
                    if (data.getData() != null) {
                        for (Datum da : data.getData()) {
                            try {
                                Date date = sdf.parse(da.getDate());
                                Log.d(TAG, "onChanged: "+da.getId());
                                int diff = dayDifference(date);
                                if (diff < 14) {
                                    ds.set(Integer.parseInt(da.getDay()) - 1, da);
                                    startToday = false;
                                }
                                if (diff < time) {
                                    time = diff;
                                    if (Integer.parseInt(da.getDay()) == 1)
                                        quarantineDayStartDate = date;
                                    else
                                        quarantineDayStartDate = addDay(date, ((-1) * Integer.parseInt(da.getDay())) + 1);


                                }

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        if(dayDifference(quarantineDayStartDate)>14){
                            startToday = true;
                        }
                    }

                    if (startToday) {
                        startQuarantineLayout.setVisibility(View.VISIBLE);
                        loading.stopLoading();
                    } else if (data.getPagination()!=null && data.getPagination().getLastPage() > 1)
                        getPaginationWiseSelfQuarantine(data.getPagination().getLastPage());
                    else {
                        activityQuarantineLayout.setVisibility(View.VISIBLE);
                        datetext.setText("Date: " + sd.format(quarantineDayStartDate));
                        for (int i = 0; i < ds.size(); i++)
                            Log.d(TAG, "onChanged: " + (i + 1) + " " + ds.get(i));
                        activityQuarantineAdapter = new ActivityQuarantineAdapter(ds, quarantineDayStartDate);
                        activitysList.setAdapter(activityQuarantineAdapter);
                        setActivitysClicked();
                        loading.stopLoading();
                    }
                }


            });



    }

    private void getPaginationWiseSelfQuarantine(int pageList) {
        for (int page = 2; page <= pageList; page++) {
            Log.d(TAG, "getPaginationWiseSelfQuarantine: "+pageList);
            Call<SelfQuarantineResponse> selfQuarantineResponseCall = mGeneralHelper.getApiService().getPaginationWiseSelfQuarantine(mGeneralHelper.getToken(), "quarentations?page=" + String.valueOf(page));
            selfQuarantineViewModel.getSelfQuarantine(selfQuarantineResponseCall);
            selfQuarantineViewModel.getSelfQuarantineResponse().observe(getViewLifecycleOwner(), data -> {
                Log.d(TAG, "onChanged: " + data);

                int time = 14;
                if (data.getData() != null) {
                    for (Datum da : data.getData()) {

                        try {
                            Date date = sdf.parse(da.getDate());
                            int diff = dayDifference(date);
                            if ( diff < 14) {
                                ds.set(Integer.parseInt(da.getDay()) - 1, da);

                            }
                            if(diff < time)
                            {
                                Log.d(TAG, "onChangeddate: "+date);
                                time = diff;
                                if(Integer.parseInt(da.getDay()) == 1)
                                    quarantineDayStartDate = date;
                                else
                                    quarantineDayStartDate = addDay(date,(((-1)*Integer.parseInt(da.getDay()))+1));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (pageList == data.getPagination().getCurrentPage()) {
                    if (ds.get(0) == null) {
                        startQuarantineLayout.setVisibility(View.VISIBLE);
                        loading.stopLoading();
                    } else {
                        activityQuarantineLayout.setVisibility(View.VISIBLE);
                        datetext.setText("Date: " + sd.format(quarantineDayStartDate));
                        for (int i = 0; i < ds.size(); i++)
                            Log.d(TAG, "onChanged: " + (i + 1) + " " + ds.get(i));
                        activityQuarantineAdapter = new ActivityQuarantineAdapter(ds, quarantineDayStartDate);
                        activitysList.setAdapter(activityQuarantineAdapter);
                        setActivitysClicked();
                        loading.stopLoading();
                    }
                }
            });
        }
    }





    @OnClick(R.id.get_start)
    public void startQuarantineNow() {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                requireContext());
        try {
            quarantineDayStartDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateQuarantineStart.getText().toString());
            String d = sd.format(quarantineDayStartDate);
            builder.setMessage("Are you sure to start isoletion from "+d+"?");
                    builder.setNegativeButton("NO",
                            (dialog, which) -> {

                            });
            builder.setPositiveButton("YES",
                    (dialog, which) -> {


                            datetext.setText("Date: " + d);
                            ArrayList<Datum> ds = new ArrayList<>();
                            for (int i = 0; i < 14; i++)
                                ds.add(null);
                            activityQuarantineAdapter = new ActivityQuarantineAdapter(ds, quarantineDayStartDate);
                            activitysList.setAdapter(activityQuarantineAdapter);
                            setActivitysClicked();
                            startQuarantineLayout.setVisibility(View.GONE);
                            activityQuarantineLayout.setVisibility(View.VISIBLE);
                            startToday =false;


                    });
        } catch (ParseException e) {
            e.printStackTrace();
        }

        builder.show();




    }

    private void setActivitysClicked(){
        activityQuarantineAdapter.setActivityListener((view1, position) -> {

                Date date = addDay(quarantineDayStartDate,position);
                Calendar tommorow = calendar.getInstance();
                tommorow.add(Calendar.DAY_OF_MONTH,1);
                String d = sdf.format(date);
                String tm = sdf.format(tommorow.getTime());
                if(dayDifference(date) >= 0 && !d.equalsIgnoreCase(tm)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
                    View view = getLayoutInflater().inflate(R.layout.date_wise_activity_dialog, null);
                    builder.setView(view);
                    dialog = builder.create();

                    dialog.show();

                    TextView sendBtn = view.findViewById(R.id.send_date_wise_activity);
                    TextView cancelBtn = view.findViewById(R.id.cancel_date_wise_activity);
                    EditText activity = view.findViewById(R.id.activity_write_field);
                    EditText suffering = view.findViewById(R.id.suffering_write_field);
                    TextView dayTitle = view.findViewById(R.id.dayTitle);

                    if(ds.get(position)!=null){
                        sendBtn.setText(getResources().getString(R.string.edit));
                        activity.setText(ds.get(position).getActivity().get(0));
                        suffering.setText(ds.get(position).getSufferings().get(0));
                    }

                    dayTitle.setText("Day " + (position + 1)+"\n"+sd.format(date));
                    sendBtn.setOnClickListener(v -> {
                        Map<String,Object> acti = new HashMap<>();
                        String getAct = activity.getText().toString();
                        String getSuffer = suffering.getText().toString();
                        if(getAct.isEmpty())
                            activity.setError("Cannot Empty");
                        List<String> list = new ArrayList<>();
                        list.add(getAct);
                        acti.put("activity",list);
                        list = new ArrayList<>();
                        list.add(getSuffer);
                        acti.put("sufferings",list);
                        acti.put("day",String.valueOf((position+1)));
                        acti.put("date",submitformat.format(date));
                        Log.d(TAG, "onClick: "+acti);

                        dataCome = false;
                        if(ds.get(position)==null)
                            create(acti);
                        else {
                            acti.put("quarentation_id",ds.get(position).getId());
                            updateQuarantine(acti);
                        }

                        dialog.dismiss();

                    });
                    cancelBtn.setOnClickListener(v -> dialog.dismiss());
                }
        });
    }


}
