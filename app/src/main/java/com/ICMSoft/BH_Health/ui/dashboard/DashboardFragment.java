package com.ICMSoft.BH_Health.ui.dashboard;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Statistics.GetStatistics;
import com.ICMSoft.BH_Health.data.model.Statistics.GetStatisticsData;
import com.ICMSoft.BH_Health.data.model.Statistics.StatsDatum;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class DashboardFragment extends Fragment implements OnMapReadyCallback {

    private DashboardViewModel dashboardViewModel;
    private static final String TAG = "HealthRecordFragment";
    private GeneralHelper mGeneralHelper;
    private GoogleMap mMap;

    @BindView(R.id.mapDetails)
    TextView details;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);


        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
      //  final TextView textView = root.findViewById(R.id.text_dashboard);

        ButterKnife.bind(this,root);
        mGeneralHelper = new GeneralHelper(getContext());


        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               // textView.setText(s);
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);


        mapFragment.getMapAsync(this);
        return root;
    }
    private  String resouce(int id){
        return getResources().getString(id);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Call<GetStatistics> statisticsCall = mGeneralHelper.getApiService().getStatistics(mGeneralHelper.getToken());
        dashboardViewModel.getStatistics(statisticsCall);
        Log.d(TAG, "getHealthRecord: " + statisticsCall);
        dashboardViewModel.getStatisticsData().observe(getViewLifecycleOwner(), new Observer<GetStatisticsData>() {
            @Override
            public void onChanged(GetStatisticsData data) {

                if (data == null)
                    return;
                details.setText( resouce(R.string.total_test)+" "+String.valueOf(data.getTotalCase())+"\n" +
                        resouce(R.string.death)+" "+String.valueOf(data.getDeath())+"\n" +
                        resouce(R.string.recovered)+" "+String.valueOf(data.getRecovered())+"\n" +
                        resouce(R.string.currently_sick)+" "+String.valueOf(data.getCurrentlySick())+"\n" +
                        resouce(R.string.today_test)+" "+String.valueOf(data.getTodayCase())+"\n" +
                        resouce(R.string.total_quarantine)+" "+String.valueOf(data.getTotalQuarantine()));
                for(StatsDatum datum: data.getStatsData()){
                    LatLng point = new LatLng(datum.getLat(),datum.getLng());
                    mMap.addMarker(new MarkerOptions()
                            .position(point)
                            .title(datum.getBnName())
                            .snippet("Death: "+String.valueOf(datum.getDeath())+"\n"+"Recoverd: "+String.valueOf(datum.getRecovered())+"\n"+"Currently Sick: "+String.valueOf(datum.getCurrentlySick())+"\nTotal Test"+String.valueOf(datum.getCurrentlySick()))

                    );
                    mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                        @Override
                        public View getInfoWindow(Marker marker) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {
                            LinearLayout info = new LinearLayout(getContext());
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(getContext());
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());

                            TextView snippet = new TextView(getContext());
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());

                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });

                    mMap.addCircle(new CircleOptions()
                            .center(point)
                            .radius(50)
                            .strokeColor(Color.RED)
                            .fillColor(Color.argb(150,50,0,0)));
                }
            }
        });


       LatLng dhaka = new LatLng(23.8103, 90.4125);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(dhaka,6.8f));
       /* LatLng chittagong = new LatLng(22.3569, 91.7832);
        mMap.addMarker(new MarkerOptions().position(dhaka).title("Marker in Dhaka"));
        mMap.addMarker(new MarkerOptions().position(chittagong).title("Marker in Chittagong"));

         */
        /*
        mMap.addMarker(new MarkerOptions()
                .position(chittagong)
                .title("Chittagong")
                .snippet("Population: 4,627,300")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.be_safe_144x144)));

         */
        /*

        Circle circle = mMap.addCircle(new CircleOptions()
                .center(dhaka)
                .radius(50)
                .strokeColor(Color.RED)
                .fillColor(Color.argb(150,50,0,0)));
                *
         */

    }
}