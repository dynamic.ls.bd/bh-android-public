package com.ICMSoft.BH_Health.ui.News;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.News.Datum;
import com.ICMSoft.BH_Health.data.model.News.Details.Data;
import com.ICMSoft.BH_Health.data.model.News.Details.NewsDetailsResponse;
import com.ICMSoft.BH_Health.data.model.News.NewsResponse;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NewsViewModel extends ViewModel {
    private static final String TAG = "NewsViewModel";


    private MutableLiveData<List<Datum>> newsList = new MutableLiveData<>();
    private MutableLiveData<Data> newsdetail = new MutableLiveData<>();
    MutableLiveData<List<Datum>> getNewsListResponse() {
        return newsList;
    }
    MutableLiveData<Data> getNewsDetailsResponse() {
        return newsdetail;
    }

    public void news(Call<NewsResponse> responseCall) {
        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                List<Datum> data = response.body().getData();
                newsList.setValue(data);

            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void newsDetails(Call<NewsDetailsResponse> responseCall) {
        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<NewsDetailsResponse>() {
            @Override
            public void onResponse(Call<NewsDetailsResponse> call, Response<NewsDetailsResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                Data data = response.body().getData();
                newsdetail.setValue(data);

            }

            @Override
            public void onFailure(Call<NewsDetailsResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void getQuestionList(Call<GeneralResponse> responseCall) {


    }

}
