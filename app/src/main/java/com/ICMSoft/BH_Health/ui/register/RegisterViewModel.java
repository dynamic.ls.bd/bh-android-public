package com.ICMSoft.BH_Health.ui.register;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Register.ErrorHandle.ErrorRegister;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryData;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryListResponse;
import com.ICMSoft.BH_Health.data.model.Register.RegisterResponse;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.Datum;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.UpazilaListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.AccessToken;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_EMAIL;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_NAME;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.UserPref;

public class RegisterViewModel extends ViewModel {
    private static final String TAG = "RegisterViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<RegisterResponse> register;
    private MutableLiveData<String> asregister;
    private Context context = null;
    public RegisterViewModel() {
        mSuccessLiveData = new MutableLiveData<>();
        register = new MutableLiveData<>();
       asregister = new MutableLiveData<>();
    }

    public LiveData<Boolean> getRegisterResponse() {
        return mSuccessLiveData;
    }
    public LiveData<RegisterResponse> getRegister() {
        return register;
    }
    public LiveData<String> getString() {
        return asregister;
    }
    public void setContext(Context context){
        this.context = context;
    }
    private boolean checkContext(){
        if(context == null)
            return false;
        return true;
    }

    public void registerUser(Call<RegisterResponse> call) {
        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    register.setValue(null);
                    Gson gson = new Gson();
                    Log.d(TAG, "onResponse: "+response.code());
                    ErrorRegister message=gson.fromJson(response.errorBody().charStream(),ErrorRegister.class);
                    if(message.getError().getEmail()!= null && message.getError().getPhone() != null && checkContext())
                    {
                        Toast.makeText(context,message.getError().getEmail().get(0)+"\n"+message.getError().getPhone().get(0),Toast.LENGTH_SHORT).show();
                    }
                    else if(message.getError().getEmail()!= null && checkContext())
                    {
                        Toast.makeText(context,message.getError().getEmail().get(0),Toast.LENGTH_SHORT).show();
                    }
                    else if(message.getError().getPhone() != null && checkContext())
                    {
                        Toast.makeText(context,message.getError().getPhone().get(0),Toast.LENGTH_SHORT).show();
                    }
                    Log.d(TAG, "onResponse: "+message.getError().getEmail());
                    // asregister.setValue(String.valueOf(response.code())+"\n"+new GsonBuilder().setPrettyPrinting().create().toJson(response));
                   // Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                asregister.setValue(String.valueOf(response.code()));
                mSuccessLiveData.setValue(true);
               register.setValue(response.body());
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
                register.setValue(null);
            }
        });


    }




}
