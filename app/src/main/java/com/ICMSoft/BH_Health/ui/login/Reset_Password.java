package com.ICMSoft.BH_Health.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Auth.ForgetPassword.ForgetPassword;
import com.ICMSoft.BH_Health.data.model.Auth.ResetPassword.ResetPassword;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class Reset_Password extends AppCompatActivity {
    private static final String TAG = "Reset_Password";
    private LoginViewModel loginViewModel;
    private GeneralHelper mGeneralHelper;

    @BindView(R.id.part_one)
    LinearLayout firstPart;


    @BindView(R.id.part_two)
    LinearLayout secondPart;

    @BindView(R.id.et_email)
    EditText email;
    @BindView(R.id.btn_back)
    Button back;
    @BindView(R.id.btn_token)
    Button submit;

    @BindView(R.id.email)
    TextView emailShow;

    @BindView(R.id.et_token)
    EditText token;

    @BindView(R.id.et_pass)
    EditText password;

    @BindView(R.id.et_confirm_pass)
    EditText confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset__password);
        ButterKnife.bind(this);
        secondPart.setVisibility(View.GONE);
        loginViewModel  = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);
        mGeneralHelper = new GeneralHelper(getApplicationContext());
        loginViewModel.setContext(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean onOptionsItemSelected(MenuItem item){

        finish();
        return true;
    }

    @OnClick(R.id.btn_change_email)
    public void checkEmail(){
        if(email.getText().toString().isEmpty())
        {
            email.setError("Cannot Empty");
            return;
        }
        final Map<String, String> params = new HashMap<>();
        params.put("email",email.getText().toString());
        Call<ForgetPassword> call = mGeneralHelper.getApiService().forgetPassword(params);

        loginViewModel.forgetPassword(call);

        loginViewModel.getforgetPassword().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean result) {
                if(result)
                {
                    firstPart.setVisibility(View.GONE);
                    secondPart.setVisibility(View.VISIBLE);
                    emailShow.setText(email.getText().toString());
                }
            }
        });


    }
    @OnClick(R.id.btn_back)
    public void backButton(){
        firstPart.setVisibility(View.VISIBLE);
        secondPart.setVisibility(View.GONE);
    }
    @OnClick(R.id.btn_token)
    public void submit(){
        if(submit.getText().toString().compareToIgnoreCase("Submit")==0 || submit.getText().toString().compareToIgnoreCase("প্রদান করুন")==0) {
            if (token.getText().toString().isEmpty()) {
                token.setError("Cannot Empty");
                return;
            }
            if (password.getText().toString().isEmpty()) {
                password.setError("Cannot Empty");
                return;
            }
            if (confirmPassword.getText().toString().isEmpty()) {
                confirmPassword.setError("Cannot Empty");
                return;
            }
            if (password.getText().toString().compareToIgnoreCase(confirmPassword.getText().toString()) != 0) {
                Toast.makeText(getApplicationContext(), "Password and confirm password don't match.", Toast.LENGTH_SHORT).show();
                return;
            }

            final Map<String, String> params = new HashMap<>();
            params.put("email", email.getText().toString());
            params.put("password", password.getText().toString());
            params.put("password_confirmation", confirmPassword.getText().toString());
            params.put("token", token.getText().toString());
            Call<ResetPassword> call = mGeneralHelper.getApiService().resetPassword(params);

            loginViewModel.resetPassword(call);

            loginViewModel.getResetPassword().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean result) {
                    if (result) {
                        submit.setText(getResources().getString(R.string.label_finish));
                        back.setVisibility(View.GONE);

                    }
                }
            });

        }

        if(submit.getText().toString().compareToIgnoreCase("Finish")==0||submit.getText().toString().compareToIgnoreCase("সমাপ্ত করা")==0) {
            Intent i  = new Intent(Reset_Password.this,LoginActivity.class);
            startActivity(i);
            finish();
        }
    }
    @Override
    public void onBackPressed() {
        if(firstPart.getVisibility() == View.VISIBLE) {
            Intent go = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(go);
            finish();
        }
        else {
            firstPart.setVisibility(View.VISIBLE);
            secondPart.setVisibility(View.GONE);
        }
    }

}
