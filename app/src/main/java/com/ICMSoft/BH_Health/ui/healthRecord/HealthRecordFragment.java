package com.ICMSoft.BH_Health.ui.healthRecord;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.HealthRecord.Datum;
import com.ICMSoft.BH_Health.data.model.HealthRecord.HealthRecordResponse;
import com.ICMSoft.BH_Health.data.model.HealthRecord.Medication;
import com.ICMSoft.BH_Health.data.model.HealthRecord.PainList;
import com.ICMSoft.BH_Health.ui.Utils.DatePickerCalander;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;


public class HealthRecordFragment extends Fragment {
    private static final String TAG = "HealthRecordFragment";
    private HealthRecordViewModel healthRecordViewModel;
    private GeneralHelper mGeneralHelper;
    private int id;
    private boolean activate;
    private ArrayList<MedicineList> medicineMorningLists,medicineNoonLists, medicineNightLists;
    private ArrayList<PreviousMedicines> previoueMorningLists,previoueNoonLists, previoueNightLists;
    @BindView(R.id.tvDate)
    TextView tvdate;

    @BindView(R.id.health_date)
    TextView historyHealthDate;


    @BindView(R.id.temperature)
    EditText temperature;

    @BindView(R.id.sleep)
    EditText sleep;

    @BindView(R.id.food)
    EditText food;

    @BindView(R.id.activities)
    EditText activities;

    @BindView(R.id.coughing)
    EditText coughing;

    @BindView(R.id.sneezing)
    EditText sneezing;

    @BindView(R.id.breath)
    SeekBar breath;

    @BindView(R.id.hand_wash)
    EditText handWash;

    @BindView(R.id.spin_temp)
    Spinner spinnerTemp;
    @BindView(R.id.spin_cough)
    Spinner spinnerCough;
    @BindView(R.id.spin_sneezing)
    Spinner spinnerSneezing;
    @BindView(R.id.spin_handwash)
    Spinner spinnerHandwash;

    @BindView(R.id.save_health)
    Button saveButton;


    @BindView(R.id.low)
    EditText lowbp;
    @BindView(R.id.high)
    EditText highbp;
    @BindView(R.id.pulse)
    EditText pulse;

    @BindView(R.id.firstSuffer)
    LinearLayout firstSuffer;

    @BindView(R.id.docter_comment_health)
    LinearLayout doctorCommentLayout;

    @BindView(R.id.comment_doctor_for_health)
    TextView commentDoctorForHealth;

    @BindView(R.id.health_view)
    ScrollView healthView;

    @BindView(R.id.btn_health)
    RadioButton healthButton;


    @BindView(R.id.btn_health_history)
    RadioButton healthHistory;


    @BindView(R.id.btn_health_summary)
    RadioButton healthSummary;


    @BindView(R.id.today_health)
    LinearLayout todayHealth;

    @BindView(R.id.previous_health)
    LinearLayout previousHealth;


    //health history

    @BindView(R.id.show_temperature)
    TextView showTemperature;
    @BindView(R.id.show_temperature_type)
    TextView showTemperatureType;
    @BindView(R.id.show_low)
    TextView showLow;
    @BindView(R.id.show_high)
    TextView showHigh;
    @BindView(R.id.show_pulse)
    TextView showPulse;
    @BindView(R.id.show_coughing)
    TextView showCoughing;
    @BindView(R.id.show_coughing_type)
    TextView showCoughingType;
    @BindView(R.id.show_sneezing)
    TextView showSneezing;
    @BindView(R.id.show_sneezing_type)
    TextView showSneezingType;
    @BindView(R.id.show_breath)
    SeekBar showBreath;
    @BindView(R.id.show_hand_wash)
    TextView showHandWash;
    @BindView(R.id.show_hand_wash_type)
    TextView showHandWashType;
    @BindView(R.id.show_sleep)
    TextView showSleep;
    @BindView(R.id.show_food)
    TextView showFood;
    @BindView(R.id.show_activities)
    TextView showActivities;


    //Medicine
    @BindView(R.id.morning_row)
    LinearLayout medicineMorningRow;

    @BindView(R.id.noon_row)
    LinearLayout medicineNoonRow;

    @BindView(R.id.night_row)
    LinearLayout medicineNightRow;

    //Previous Medicine

    @BindView(R.id.show_morning_row)
    LinearLayout previousMoringRow;

    @BindView(R.id.show_noon_row)
    LinearLayout previousNoonRow;

    @BindView(R.id.show_night_row)
    LinearLayout previousNightRow;

    boolean edit = false;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat dateFormatshow = new SimpleDateFormat("MMMM dd, yyyy");
    //  SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm a");


    Date todayDate;

    //Calendar calendar;
    ArrayList<CheckBox> painList;

    Loading loading;

    DatePickerCalander datePickerCalander;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_record_modify, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view, healthView);
        healthRecordViewModel = new ViewModelProvider(this).get(HealthRecordViewModel.class);
        healthRecordViewModel.setContext(getContext());
        mGeneralHelper = new GeneralHelper(getContext());
        Log.d(TAG, "onCreateView: " + mGeneralHelper.getToken());
        //calendar = Calendar.getInstance();
        todayDate = new Date();
        activate = true;
        // String[] pa = {"Headache","Sore throat"};
        loading.startLoading();
        painListConfig();
        healthViewVisible(todayHealth, healthButton);
        healthButton.setChecked(true);
        datePickerCalander = new DatePickerCalander(historyHealthDate, getContext());
        medicineMorningLists = new ArrayList<>();
        medicineNoonLists = new ArrayList<>();
        medicineNightLists = new ArrayList<>();

        previoueMorningLists = new ArrayList<>();
        previoueNoonLists = new ArrayList<>();
        previoueNightLists = new ArrayList<>();

        MedicineList medicineList = new MedicineList(getContext());
        medicineMorningLists.add(medicineList);
        medicineMorningRow.addView(medicineList.getView());
        medicineButton(medicineList,medicineMorningLists,medicineMorningRow);

        medicineList = new MedicineList(getContext());
        medicineNoonLists.add(medicineList);
        medicineNoonRow.addView(medicineList.getView());
        medicineButton(medicineList,medicineNoonLists,medicineNoonRow);

        medicineList = new MedicineList(getContext());
        medicineNightLists.add(medicineList);
        medicineNightRow.addView(medicineList.getView());
        medicineButton(medicineList,medicineNightLists,medicineNightRow);
        painList = new ArrayList<>();

        return view;
    }

    public LinearLayout createLinear() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 30, 0, 0);
        params.weight = 2;
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        return linearLayout;
    }

    private void setTodayMedicine(Map<String, Integer> medicine,ArrayList<MedicineList> medicineLists, LinearLayout rows){
        boolean flag = true;
        for(String name: medicine.keySet()){
            if(flag)
            {
                medicineLists.get(0).name.setText(name);
                medicineLists.get(0).quantity.setText(String.valueOf(medicine.get(name)));
                flag = false;
            }
            else {
                MedicineList medicineList = new MedicineList(getContext());
                medicineList.name.setText(name);
                medicineList.quantity.setText(String.valueOf(medicine.get(name)));
                medicineLists.add(medicineList);
                rows.addView(medicineList.getView());
                medicineButton(medicineList,medicineLists,rows);
            }
        }
    }

    public void getTodayHealthRecord() {

        loading.startLoading();
        String today = dateFormatshow.format(todayDate);
        tvdate.setText("Date: " + dateFormatshow.format(todayDate) /*+ "\nTime: " + timeformat.format(todayDate)*/);

        Call<HealthRecordResponse> healthRecordResponseCall = mGeneralHelper.getApiService().getHealthRecord(mGeneralHelper.getToken(), dateFormat.format(todayDate));
        healthRecordViewModel.getHealthRecord(healthRecordResponseCall);
        Log.d(TAG, "getHealthRecord: " + healthRecordResponseCall);
        healthRecordViewModel.getHealthRecordResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {
                Log.d(TAG, "onChanged: " + data);

                if (data == null) {
                    loading.stopLoading();
                    return;
                }
                //tvtime.setText("Time: "+dateFormat.format(date).toString());
                loading.stopLoading();
                for (Datum d : data) {
                    try {
                        Date ds = dateFormat.parse(d.getDate());
                        //dateFormat12 = new SimpleDateFormat("MMMM dd, yyyy");
                        String ps = dateFormatshow.format(ds);
                        Log.d(TAG, "onChanged25: " + ps);
                        Log.d(TAG, "onChanged25: " + today);
                        if (ps.equalsIgnoreCase(today)) {
                            // dateFormat = new SimpleDateFormat("hh:mm a");
                            // Log.d(TAG, "onChanged: "+timeformat.format(ds));
                            tvdate.setText("Date: " + dateFormatshow.format(todayDate) /*+ "\nTime: " + timeformat.format(ds)*/);
                            Log.d(TAG, "onChanged: " + d.getId());
                            saveButton.setText(getResources().getString(R.string.edit));
                            edit = true;
                            id = d.getId();
                            String temp = d.getTemperature();
                            if (!temp.isEmpty()) {
                                String[] sp = temp.split("/");
                                temperature.setText(sp[0]);
                                if (sp[1].compareToIgnoreCase("Fahrenheit") == 0 || sp[1].compareToIgnoreCase("ফাঃ") == 0) {
                                    spinnerTemp.setSelection(1);
                                }
                            }
                            String cough = d.getCough();
                            if (!cough.isEmpty()) {
                                String[] sp = cough.split("/");
                                coughing.setText(sp[0]);
                                if (sp[1].compareToIgnoreCase("Hour") == 0 || sp[1].compareToIgnoreCase("ঘন্টা") == 0) {
                                    spinnerCough.setSelection(1);
                                }
                            }
                            if (d.getDiastolic() != null)
                                lowbp.setText(d.getDiastolic().toString());
                            if (d.getSystolic() != null)
                                highbp.setText(d.getSystolic().toString());
                            if (d.getPulse() != null)
                                pulse.setText(d.getPulse().toString());
                            if (d.getBreathing() != null) {
                                if (d.getBreathing().compareToIgnoreCase("normal") == 0)
                                    breath.setProgress(0);
                                else
                                    breath.setProgress(Integer.parseInt(d.getBreathing()));
                            }

                            String sneez = d.getSneezing();
                            if (!sneez.isEmpty()) {
                                String[] sp = sneez.split("/");
                                sneezing.setText(sp[0]);
                                Log.d(TAG, "onChanged: " + sp[0]);
                                if (sp[1].compareToIgnoreCase("Hour") == 0 || sp[1].compareToIgnoreCase("ঘন্টা") == 0) {
                                    spinnerSneezing.setSelection(1);
                                }
                            }
                            String hand = d.getHandWash();
                            if (!hand.isEmpty()) {
                                String[] sp = hand.split("/");
                                handWash.setText(sp[0]);
                                if (sp[1].compareToIgnoreCase("Hour") == 0 || sp[1].compareToIgnoreCase("ঘন্টা") == 0) {
                                    spinnerHandwash.setSelection(1);
                                }
                            }

                            List<String> painListData = d.getPain();
                            Log.d(TAG, "onChanged: " + painListData);
                            if (painListData != null) {
                                for (String i : painListData)
                                    if (!i.isEmpty() && !painList.isEmpty()) {
                                        painList.get(Integer.parseInt(i)).setChecked(true);
                                    }
                            }
                            if (d.getSleepingHour() != null)
                                sleep.setText(String.valueOf(d.getSleepingHour()));

                            if (d.getActivities() != null)
                                activities.setText(d.getActivities());

                            if (d.getFoodDiets() != null)
                                food.setText(d.getFoodDiets());
                            Medication medication = d.getMedication();
                            setTodayMedicine(medication.getMorning(),medicineMorningLists,medicineMorningRow);
                            setTodayMedicine(medication.getNoon(),medicineNoonLists,medicineNoonRow);
                            setTodayMedicine(medication.getEvening(),medicineNightLists,medicineNightRow);

                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }


            }
        });


    }

    public void painListConfig() {
        Call<PainList> healthRecordResponseCall = mGeneralHelper.getApiService().getPainList();
        healthRecordViewModel.getPainList(healthRecordResponseCall);
        Log.d(TAG, "getHealthRecord: " + healthRecordResponseCall);
        healthRecordViewModel.getPainListDataList().observe(getViewLifecycleOwner(), new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> data) {

                if (data == null)
                    return;
                if (activate) {
                    CheckBox checkBox;
                    LinearLayout linearLayout = null;


                    for (int i = 1; i <= data.size(); i++) {
                        checkBox = new CheckBox(getContext());
                        LinearLayout.LayoutParams checkparam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        checkparam.weight = 1;
                        checkBox.setText(data.get(i - 1));
                        checkBox.setTag(data.get(i - 1));
                        checkBox.setLayoutParams(checkparam);
                        painList.add(checkBox);

                        if (i % 2 != 0) {
                            linearLayout = createLinear();
                            linearLayout.setTag("Linear" + i);
                            linearLayout.addView(checkBox);
                            firstSuffer.addView(linearLayout);
                            //firstSuffer.addView(checkBox);
                        } else {
                            linearLayout.addView(checkBox);
                        }
                        activate = false;

                    }
                    getTodayHealthRecord();
                }
            }
        });


    }

    private Map<String,Integer> getMedications(Map<String,Integer> map, ArrayList<MedicineList> medicineLists){
        for(MedicineList medicineList: medicineLists){
            String name,quantity;
            name = medicineList.name.getText().toString();
            quantity = medicineList.quantity.getText().toString();
            if(!name.isEmpty()&& !quantity.isEmpty())
                map.put(name,Integer.valueOf(quantity));
        }
        return map;
    }

    @OnClick(R.id.save_health)
    public void save() {

        String temp = temperature.getText().toString();
        String cough = coughing.getText().toString();
        String sneezing = this.sneezing.getText().toString();

        String pulse = this.pulse.getText().toString();
        String lowbp = this.lowbp.getText().toString();
        String highbp = this.highbp.getText().toString();
        String handwash = handWash.getText().toString();
        String sleep = this.sleep.getText().toString();
        String food = this.food.getText().toString();
        String activites = this.activities.getText().toString();
        boolean check = false;
        if (temp.isEmpty()) {
            temperature.setError("Cannot Empty");
            check = true;
        }
        if (cough.isEmpty()) {
            coughing.setError("Cannot Empty");
            check = true;
        }

        if (sneezing.isEmpty()) {
            this.sneezing.setError("Cannot Empty");
            check = true;
        }
        if (pulse.isEmpty()) {
            this.pulse.setError("Cannot Empty");
            check = true;
        }
        if (handwash.isEmpty()) {
            handWash.setError("Cannot Empty");
            check = true;
        }

        if (!lowbp.isEmpty() && !highbp.isEmpty()) {
            int l = Integer.parseInt(lowbp);
            int h = Integer.parseInt(highbp);
            Log.d(TAG, "save: " + l + " " + h);
            if (l > h) {
                Toast.makeText(getContext(), "Low BP cannot greater than high BP", Toast.LENGTH_SHORT).show();
                return;
            }

        }

        if (check)
            return;
        temp += "/" + spinnerTemp.getSelectedItem().toString();
        cough += "/" + spinnerCough.getSelectedItem().toString();
        sneezing += "/" + spinnerSneezing.getSelectedItem().toString();
        List<String> pain = new ArrayList<>();
        // pain.add(String.valueOf(spinnerPain.getSelectedItemPosition()));
        String notes = "some";
        handwash += "/" + spinnerHandwash.getSelectedItem().toString();


        Map<String, Object> params = new HashMap<>();
        params.put("temperature", temp);
        params.put("cough", cough);
        params.put("sneezing", sneezing);
        params.put("breathing", String.valueOf(this.breath.getProgress()));
        params.put("hand_wash", handwash);
        params.put("notes", notes);
        params.put("sleeping_hour", sleep);
        params.put("food_diets", food);
        params.put("activities", activites);

        Map<String,Object> medication = new HashMap<>();
        medication.put("morning",getMedications(new HashMap<>(),medicineMorningLists));
        medication.put("noon",getMedications(new HashMap<>(),medicineNoonLists));
        medication.put("evening",getMedications(new HashMap<>(),medicineNightLists));
        params.put("medication", medication);

        ArrayList<String> painArray = new ArrayList<>();
        for (int i = 0; i < painList.size(); i++) {
            if (painList.get(i).isChecked())
                painArray.add(String.valueOf(i));
        }
        params.put("pain", painArray);

        params.put("diastolic", lowbp);
        params.put("systolic", highbp);
        params.put("pulse", pulse);
        // Log.d(TAG, "save: "+dateFormat.format(calendar.getTime()));
        loading.startLoading();

        if (edit) {

            String today1 = dateFormat.format(new Date());
            params.put("date", today1);
            update(params);
        } else {
            String today = dateFormat.format(todayDate);
            Log.d(TAG, "save: " + today);
            params.put("date", today);
            create(params);
            saveButton.setText(getResources().getString(R.string.edit));
            edit = true;
        }


    }

    public void create(Map<String, Object> params) {

        loading.startLoading();
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().createHealthRecord(mGeneralHelper.getToken(), params);
        healthRecordViewModel.crateeHealthRecord(responseCall);

        healthRecordViewModel.getGeneralResponse().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Log.d(TAG, "onChanged: " + aBoolean);

                // Toast.makeText(getContext(), "Successful", Toast.LENGTH_SHORT).show();
                if (!aBoolean) {
                    Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }
                loading.stopLoading();
                //else
                //    getHealthRecord();

            }
        });
    }

    public void update(Map<String, Object> params) {
        loading.startLoading();
        Log.d(TAG, "update: " + id);
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().updateHealthRecord(mGeneralHelper.getToken(), "health_records/update/" + String.valueOf(id), params);
        healthRecordViewModel.updateHealthRecord(responseCall);


        healthRecordViewModel.getGeneralResponse().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Log.d(TAG, "onChanged: " + aBoolean);
                if (!aBoolean) {
                    Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }
                loading.stopLoading();
                // else
                //  getHealthRecord();

            }
        });
    }
/*
    @OnClick(R.id.summary_of_health_record)
    public void summaryShow(){

        View view = getLayoutInflater().inflate(R.layout.summery_dialog,null);

        Summary summary = new Summary(view);



    }

 */

    public class Summary {
        AlertDialog dialog;

        public Summary(View view) {
            ButterKnife.bind(this, view);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(view);
            AlertDialog dialog = builder.create();
            dialog.show();
            this.dialog = dialog;
        }
/*
        @OnClick(R.id.summary_cancel)
        public void cancel() {
            dialog.dismiss();
        }

        @OnClick(R.id.save_summary_health)
        public void save() {
            Toast.makeText(getContext(), "Successfully", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }

 */
    }

    private void resetHealthViews() {
        todayHealth.setVisibility(View.GONE);
        previousHealth.setVisibility(View.GONE);
        resetMenuButton();

    }

    private void resetMenuButton() {
        healthButton.setTextColor(Color.BLACK);
        healthHistory.setTextColor(Color.BLACK);
        healthSummary.setTextColor(Color.BLACK);
    }

    private void healthViewVisible(View layoutView, RadioButton buttonView) {
        resetHealthViews();
        layoutView.setVisibility(View.VISIBLE);
        buttonView.setTextColor(Color.WHITE);

    }

    @OnClick(R.id.btn_health)
    public void todayHealthView() {
        healthViewVisible(todayHealth, healthButton);

    }


    @OnClick(R.id.btn_health_history)
    public void previousHealthView() {
        healthViewVisible(previousHealth, healthHistory);
        datePickerCalander.setYestardayDate();
        searchHealthRecord();

    }


    @OnClick(R.id.btn_health_summary)
    public void chartHealthView() {
        resetHealthViews();

    }

    @OnClick(R.id.search_health)
    public void searchHealthRecord() {
        getPreviousHealthRecord(datePickerCalander.getDate());
    }

    private void setDayHourPrevious(TextView textDay, String sp) {
        if (sp.compareToIgnoreCase("Hour") == 0 || sp.compareToIgnoreCase("ঘন্টা") == 0) {
            textDay.setText(getResources().getStringArray(R.array.day)[1]);
        } else textDay.setText(getResources().getStringArray(R.array.day)[0]);
    }

    private void resetHistory() {
        showTemperature.setText("");
        showTemperatureType.setText("");
        showCoughing.setText("");
        showPulse.setText("");
        showCoughingType.setText("");
        showLow.setText("");
        showHigh.setText("");
        showSneezing.setText("");
        showSneezingType.setText("");
        showHandWash.setText("");
        showHandWashType.setText("");
        showSleep.setText("");
        showActivities.setText("");
        showFood.setText("");
        showBreath.setProgress(0);
        previousMoringRow.removeAllViews();
        previousNoonRow.removeAllViews();
        previousNightRow.removeAllViews();
        previoueNightLists.clear();
        previoueNoonLists.clear();
        previoueMorningLists.clear();
    }


    private void getPreviousMedications(Map<String,Integer> map, ArrayList<PreviousMedicines> medicineLists,LinearLayout rows){
        for(String name:map.keySet()){
            PreviousMedicines previousMedicines = new PreviousMedicines(getContext());
            previousMedicines.name.setText(name);
            previousMedicines.quantity.setText(String.valueOf(map.get(name)));
            medicineLists.add(previousMedicines);
            rows.addView(previousMedicines.getView());
        }

    }
    public void getPreviousHealthRecord(Date pickedDate) {

        loading.startLoading();
        String today = dateFormatshow.format(pickedDate);
        // tvdate.setText("Date: " + dateFormatshow.format(todayDate) /*+ "\nTime: " + timeformat.format(todayDate)*/);
        resetHistory();
        Call<HealthRecordResponse> healthRecordResponseCall = mGeneralHelper.getApiService().getHealthRecord(mGeneralHelper.getToken(), dateFormat.format(pickedDate));
        healthRecordViewModel.getHealthRecord(healthRecordResponseCall);
        Log.d(TAG, "getHealthRecord: " + healthRecordResponseCall);
        healthRecordViewModel.getHealthRecordResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {
                Log.d(TAG, "onChanged: " + data);

                if (data == null) {
                    loading.stopLoading();
                    return;
                }
                //tvtime.setText("Time: "+dateFormat.format(date).toString());
                loading.stopLoading();
                for (Datum d : data) {
                    try {
                        Date ds = dateFormat.parse(d.getDate());
                        String ps = dateFormatshow.format(ds);
                        if (ps.equalsIgnoreCase(today)) {
                            String temp = d.getTemperature();
                            if (!temp.isEmpty()) {
                                String[] sp = temp.split("/");
                                showTemperature.setText(sp[0]);
                                if (sp[1].compareToIgnoreCase("Fahrenheit") == 0 || sp[1].compareToIgnoreCase("ফাঃ") == 0) {

                                    showTemperatureType.setText(getResources().getStringArray(R.array.temp)[1]);
                                } else
                                    showTemperatureType.setText(getResources().getStringArray(R.array.temp)[0]);
                            }
                            String cough = d.getCough();
                            if (!cough.isEmpty()) {
                                String[] sp = cough.split("/");
                                showCoughing.setText(sp[0]);
                                setDayHourPrevious(showCoughingType, sp[1]);
                            }
                            if (d.getDiastolic() != null)
                                showLow.setText(d.getDiastolic().toString());
                            if (d.getSystolic() != null)
                                showHigh.setText(d.getSystolic().toString());
                            if (d.getPulse() != null)
                                showPulse.setText(d.getPulse().toString());
                            if (d.getBreathing() != null) {
                                if (d.getBreathing().compareToIgnoreCase("normal") == 0)
                                    showBreath.setProgress(0);
                                else
                                    showBreath.setProgress(Integer.parseInt(d.getBreathing()));
                            }

                            String sneez = d.getSneezing();
                            if (!sneez.isEmpty()) {
                                String[] sp = sneez.split("/");
                                showSneezing.setText(sp[0]);
                                setDayHourPrevious(showSneezingType, sp[1]);
                            }
                            String hand = d.getHandWash();
                            if (!hand.isEmpty()) {
                                String[] sp = hand.split("/");
                                showHandWash.setText(sp[0]);
                                setDayHourPrevious(showHandWashType, sp[1]);

                            }
/*
                            List<String> painListData = d.getPain();
                            Log.d(TAG, "onChanged: " + painListData);
                            if (painListData != null) {
                                for (String i : painListData)
                                    if (!i.isEmpty() && !painList.isEmpty()) {
                                        painList.get(Integer.parseInt(i)).setChecked(true);
                                    }
                            }

 */
                            if (d.getSleepingHour() != null)
                                showSleep.setText(String.valueOf(d.getSleepingHour()));

                            if (d.getActivities() != null)
                                showActivities.setText(d.getActivities());

                            if (d.getFoodDiets() != null)
                                showFood.setText(d.getFoodDiets());
                            Medication medication = d.getMedication();
                            getPreviousMedications(medication.getMorning(),previoueMorningLists,previousMoringRow);
                            getPreviousMedications(medication.getNoon(),previoueNoonLists,previousNoonRow);
                            getPreviousMedications(medication.getEvening(),previoueNightLists,previousNightRow);

                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }


            }
        });


    }


    private void medicineButton(MedicineList medicineList,ArrayList<MedicineList> mlists, LinearLayout layout){
        medicineList.setAddNew(new MedicineList.AddNew() {
            @Override
            public void addNewLayout() {
                MedicineList newMedicine = new MedicineList(getContext());
                mlists.add(newMedicine);
                layout.addView(newMedicine.getView());
                medicineButton(newMedicine,mlists,layout);
            }
        });
        medicineList.setRemoveOld(new MedicineList.RemoveOld() {
            @Override
            public void removeOldLayout() {
                layout.removeView(medicineList.getView());
                mlists.remove(medicineList);
            }
        });
    }

}
