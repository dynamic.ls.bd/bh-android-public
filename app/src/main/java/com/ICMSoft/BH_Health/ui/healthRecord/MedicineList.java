package com.ICMSoft.BH_Health.ui.healthRecord;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ICMSoft.BH_Health.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicineList {
    private View view;
    private Context context;
    private AddNew addNew;
    private RemoveOld removeOld;

    @BindView(R.id.medicine_add_morning)
    Button add;

    @BindView(R.id.medicine_name)
    EditText name;

    @BindView(R.id.medicine_quantity)
    EditText quantity;
    public MedicineList(Context context) {
        this.context =context;
        view = LayoutInflater.from(context).inflate(R.layout.medicines_list,null);
        ButterKnife.bind(this,view);
        setAdd();
    }
    public View getView(){
        return view;
    }
    public interface AddNew{
        void addNewLayout();
    }

    public interface RemoveOld{
        void removeOldLayout();
    }

    public void setAddNew(AddNew addNew) {
        this.addNew = addNew;
    }

    public void setRemoveOld(RemoveOld removeOld) {
        this.removeOld = removeOld;
    }

    public void setAdd(){
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(add.getText().toString().compareToIgnoreCase("+")==0)
               {
                   add.setText("-");
                   add.setTextColor(Color.RED);
                   addNew.addNewLayout();
               }
               else
               {
                   removeOld.removeOldLayout();
               }
            }
        });
    }


}
