package com.ICMSoft.BH_Health.ui.safe_cirle.todayIMeet;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting.TodayIMeet;
import com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting.TodayIMeetResponse;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodayIMeetViewModel extends ViewModel {

    private static final String TAG = "TodayIMeetViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<List<TodayIMeet>> mGetData;
    private Context context;

   public TodayIMeetViewModel(){
       mSuccessLiveData = new MutableLiveData<>();
       mGetData = new MutableLiveData<>();
   }

    public void setContext(Context context) {
        this.context = context;
    }

    public LiveData<Boolean> getMeetingResponse() {
        return mSuccessLiveData;
    }
    public LiveData<List<TodayIMeet>> getDataResponse() {
        return mGetData;
    }
    public void memberMeetingResponse(Call<GeneralResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Gson gson = new Gson();
                   // AddSafeCircleError message=gson.fromJson(response.errorBody().charStream(), AddSafeCircleError.class);
                 /*   String s = "";
                    boolean flag = false;
                    if(message.getError().getCircle()!=null){
                        s+=message.getError().getCircle().get(0);
                        flag = true;
                    }
                    if(message.getError().getName()!=null){
                        if(flag)
                            s+="\n";
                        flag = true;
                        s+=message.getError().getName().get(0);
                    }
                    if(message.getError().getPhone()!=null){
                        if(flag)
                            s+="\n";
                        flag = true;
                        s+=message.getError().getPhone().get(0);
                    }
                    Toast.makeText(context,s,Toast.LENGTH_SHORT).show();

                  */
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });


    }
    public void memberDataResponse(Call<TodayIMeetResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<TodayIMeetResponse>() {
            @Override
            public void onResponse(Call<TodayIMeetResponse> call, Response<TodayIMeetResponse> response) {
                if (response.code() != 200) {
                    mGetData.setValue(null);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Gson gson = new Gson();
                   // AddSafeCircleError message=gson.fromJson(response.errorBody().charStream(), AddSafeCircleError.class);
                 /*   String s = "";
                    boolean flag = false;
                    if(message.getError().getCircle()!=null){
                        s+=message.getError().getCircle().get(0);
                        flag = true;
                    }
                    if(message.getError().getName()!=null){
                        if(flag)
                            s+="\n";
                        flag = true;
                        s+=message.getError().getName().get(0);
                    }
                    if(message.getError().getPhone()!=null){
                        if(flag)
                            s+="\n";
                        flag = true;
                        s+=message.getError().getPhone().get(0);
                    }
                    Toast.makeText(context,s,Toast.LENGTH_SHORT).show();

                  */
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mGetData.setValue(response.body().getData());

            }

            @Override
            public void onFailure(Call<TodayIMeetResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mGetData.setValue(null);
            }
        });


    }

}
