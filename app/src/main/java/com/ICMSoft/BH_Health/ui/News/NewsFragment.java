package com.ICMSoft.BH_Health.ui.News;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.NewsAdapter;
import com.ICMSoft.BH_Health.data.model.News.Datum;
import com.ICMSoft.BH_Health.data.model.News.NewsResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.airbnb.lottie.LottieAnimationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    private static final String TAG = "NewsFragment";
    private NewsViewModel newsViewModel;
    private GeneralHelper mGeneralHelper;
    private LinearLayoutManager mLinearLayoutManager;
    private NewsAdapter newsAdapter;
    ArrayAdapter<String> dataadapter;
    ViewGroup listView;

    public static int newsPosition;

    @BindView(R.id.rvLatestNews1)
    RecyclerView mRV1;

    @BindView(R.id.news_view)
    NestedScrollView newsView;

    Loading loading;
    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view,newsView);
        newsViewModel = new ViewModelProvider(this).get(NewsViewModel.class);
        newsPosition = 0;


        init();
        getNews();
        return view;
    }



    private void init() {
        mGeneralHelper = new GeneralHelper(getContext());
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRV1.setLayoutManager(mLinearLayoutManager);

    }

    private void getNews() {
        loading.startLoading();
        Log.d(TAG, "getNews: ");
        final Call<NewsResponse> responseCall = mGeneralHelper.getApiService().getNews(mGeneralHelper.getToken());
        newsViewModel.news(responseCall);


        ///  newsViewModel.getNewsListResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {

        newsViewModel.getNewsListResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {

                if (data != null) {
                    ArrayList<Datum> arrayList = new ArrayList<>();
                    for(Datum d: data)
                        arrayList.add(d);
                    newsAdapter = new NewsAdapter(arrayList);

                   // mRV1.setIO
                    mRV1.setAdapter(newsAdapter);
                    setLister();
                    Log.d(TAG, "onChanged: data size " + data.size());
                } else {
                    Toast.makeText(getContext(), "No data found", Toast.LENGTH_SHORT).show();

                }
                loading.stopLoading();

            }
        });


    }



    public void setLister(){
        newsAdapter.setNewsDetailsShowListener(new NewsAdapter.NewsDetailsShowListener() {
            @Override
            public void newsDetailsShowOnClicked(View view, int position) {
                newsPosition = position;
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.news_detals);
            }
        });
    }

}
