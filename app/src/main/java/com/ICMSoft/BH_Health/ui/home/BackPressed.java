package com.ICMSoft.BH_Health.ui.home;

public interface BackPressed {
    boolean onBackPressed();
}
