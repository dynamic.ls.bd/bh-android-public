package com.ICMSoft.BH_Health.ui.Announcement;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.AnnouncementAdapter;
import com.ICMSoft.BH_Health.adapter.NewsAdapter;
import com.ICMSoft.BH_Health.data.model.Announcement.AnnouncementResponse;

import com.ICMSoft.BH_Health.data.model.Announcement.Datum;
import com.ICMSoft.BH_Health.data.model.News.NewsResponse;
import com.ICMSoft.BH_Health.ui.News.NewsViewModel;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;


public class Announcement_Fragment extends Fragment {


    private static final String TAG = "NewsFragment";
    private AnnouncementViewModel newsViewModel;
    private GeneralHelper mGeneralHelper;
    private LinearLayoutManager mLinearLayoutManager;
    private AnnouncementAdapter newsAdapter;
    ArrayAdapter<String> dataadapter;
    ViewGroup listView;

    public static int newsPosition;

    @BindView(R.id.rvAnnouncement)
    RecyclerView mRV1;

    @BindView(R.id.news_view)
    NestedScrollView newsView;

    Loading loading;
    public Announcement_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
               View view = inflater.inflate(R.layout.fragment_announcemnet, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view,newsView);
        newsViewModel = new ViewModelProvider(this).get(AnnouncementViewModel.class);
        newsPosition = 0;

        init();
        getNews();
        return view;
    }

    private void init() {
        mGeneralHelper = new GeneralHelper(getContext());
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //  mFamilyMemberRV.setHasFixedSize(true);
        // use a linear layout manager
        // mLinearLayoutManager = new LinearLayoutManager(getContext());
        // mRV.setLayoutManager(mLinearLayoutManager);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRV1.setLayoutManager(mLinearLayoutManager);

    }

    private void getNews() {
        loading.startLoading();
        Log.d(TAG, "announcementNews: ");
        final Call<AnnouncementResponse> responseCall = mGeneralHelper.getApiService().getAnnouncement(mGeneralHelper.getToken());
        newsViewModel.announcement(responseCall);


        ///  newsViewModel.getNewsListResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {

        newsViewModel.getNewsListResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {

                if (data != null) {
                    ArrayList<Datum> arrayList = new ArrayList<>();
                    for(Datum d: data)
                        arrayList.add(d);
                    newsAdapter = new AnnouncementAdapter(arrayList);

                    // mRV1.setIO
                    mRV1.setAdapter(newsAdapter);
                    setLister();
                    Log.d(TAG, "onChanged: data size " + data.size());
                } else {
                    Toast.makeText(getContext(), "No data found", Toast.LENGTH_SHORT).show();

                }
                loading.stopLoading();

            }
        });


    }



    public void setLister()
    {
        newsAdapter.setAnnouncementDetailsListener(new AnnouncementAdapter.AnnouncementDetailsListener() {
            @Override
            public void AnnouncementDetailsOnClicked(View view, int position) {
                newsPosition = position;
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                navController.navigate(R.id.announcementDetails);

            }
        });
    }
}
