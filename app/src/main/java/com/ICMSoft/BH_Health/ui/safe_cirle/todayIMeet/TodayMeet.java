package com.ICMSoft.BH_Health.ui.safe_cirle.todayIMeet;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.TodayIMeetAdapter;
import com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting.TodayIMeet;
import com.ICMSoft.BH_Health.data.model.Circle.todayIMeeting.TodayIMeetResponse;
import com.ICMSoft.BH_Health.data.model.Circle.user.DatumUser;
import com.ICMSoft.BH_Health.data.model.Circle.user.UserResponse;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.ICMSoft.BH_Health.ui.safe_cirle.SafeCircleViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.safe_cirle.SafeCircleFragment.userList;
import static com.ICMSoft.BH_Health.ui.safe_cirle.SafeCircleFragment.usersAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class TodayMeet extends Fragment {
    DatePickerDialog picker;
    TimePickerDialog timepicker;
    private TodayIMeetViewModel toadyIMeetViewModel;
    private GeneralHelper mGeneralHelper;
    private SafeCircleViewModel safeCircleViewModel;
    private Calendar userForData;
    private TodayIMeetAdapter adapter;

    @BindView(R.id.rvMemberMeeting)
    RecyclerView memberMeeting;

    @BindView(R.id.meetingView)
    CoordinatorLayout  meetView;
    private Loading loading;
    private LinearLayoutManager mLinearLayoutManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_today_meet, container, false);
        ButterKnife.bind(this,view);
        loading = new Loading(view,meetView);
        toadyIMeetViewModel = new ViewModelProvider(this).get(TodayIMeetViewModel.class);
        safeCircleViewModel = new ViewModelProvider(this).get(SafeCircleViewModel.class);
        toadyIMeetViewModel.setContext(requireContext());
        safeCircleViewModel.setContext(requireContext());
        mGeneralHelper = new GeneralHelper(requireContext());
        userForData = Calendar.getInstance();
        getData();
        memberMeeting.addItemDecoration(new DividerItemDecoration(memberMeeting.getContext(), DividerItemDecoration.VERTICAL));
       return view;
    }
    
    private void getData(){
        loading.startLoading();
        Call<TodayIMeetResponse> responseCall = mGeneralHelper.getApiService().todayMeetingGetMember(mGeneralHelper.getToken());
        toadyIMeetViewModel.memberDataResponse(responseCall);
        toadyIMeetViewModel.getDataResponse().observe(getViewLifecycleOwner(), data -> {
            loading.stopLoading();
            if (data != null) {
                mLinearLayoutManager = new LinearLayoutManager(requireContext());
                memberMeeting.setLayoutManager(mLinearLayoutManager);

                memberMeeting.setVisibility(View.VISIBLE);
                memberMeeting.setHasFixedSize(true);
                adapter = new TodayIMeetAdapter(data);
                memberMeeting.setAdapter(adapter);
                editMeeting();

            }
            else
                memberMeeting.setVisibility(View.GONE);
        });
    }

   @OnClick(R.id.meeting_member_add)
    public void addMember() {

        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        final View customLayout = getLayoutInflater().inflate(R.layout.layout_add_member_meeting_dialog, null);
        TextView date_field = customLayout.findViewById(R.id.et_date);
       TextView time_field = customLayout.findViewById(R.id.et_time);
        TextView cancelTV = customLayout.findViewById(R.id.tv_cancel);
        TextView sendTV = customLayout.findViewById(R.id.tv_send);
        EditText name = customLayout.findViewById(R.id.et_name);
        AutoCompleteTextView phone = customLayout.findViewById(R.id.et_contact_auto);
        phone.setAdapter(usersAdapter);
        builder.setView(customLayout);
        AlertDialog dialog = builder.create();
        dialog.show();
       phone.setOnItemClickListener((parent, view, position, id) -> {
           DatumUser user =  userList.get(usersAdapter.getItem(position));
           assert user != null;
           name.setText(user.getName());

       });
        sendTV.setOnClickListener(v -> {
            String nameValue = name.getText().toString();
            String phoneValue = phone.getText().toString();
            String time = time_field.getText().toString();
            String dateValue = date_field.getText().toString();
            if(nameValue.isEmpty()){
                name.setError("Cannot Empty");
                return;
            }
            if(phoneValue.isEmpty()){
                phone.setError("Cannot Empty");
                return;
            }
            if(time.isEmpty()){
                time_field.setError("Cannot Empty");
                return;
            } if(dateValue.isEmpty()){
                date_field.setError("Cannot Empty");
                return;
            }
            Map<String, Object> params = new HashMap<>();
            DatumUser d = userList.get(phoneValue);
            if(d!= null)
            params.put("user_id",d.getId());
            params.put("name",nameValue);
            params.put("phone",phoneValue);
            params.put("type","1");

            Date meetTime =  userForData.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H:mm");
            params.put("date_time",dateFormat.format(meetTime));
            //Toast.makeText(requireContext(),dateFormat.format(meetTime),Toast.LENGTH_SHORT).show();

            Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().todayMeetingCreateMember(mGeneralHelper.getToken(), params);
            toadyIMeetViewModel.memberMeetingResponse(generalResponseCall);

            toadyIMeetViewModel.getMeetingResponse().observe(getViewLifecycleOwner(), aBoolean -> {

                if (aBoolean) {
                    getData();
                    dialog.dismiss();
                   // Toast.makeText(requireContext(), "Circle Member added successfully", Toast.LENGTH_SHORT).show();
                }else
                {
                    Toast.makeText(requireContext(), "Data Store Failed", Toast.LENGTH_SHORT).show();}
            });


        });
        cancelTV.setOnClickListener(view -> dialog.dismiss());

       //Date todayDate = new Date();
       SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy");

       userForData = Calendar.getInstance();
       /*
       int day = userForData.get(Calendar.DAY_OF_MONTH);
       int month = userForData.get(Calendar.MONTH);
       int year = userForData.get(Calendar.YEAR);

        */
       int hour = userForData.get(Calendar.HOUR_OF_DAY);
       int minutes = userForData.get(Calendar.MINUTE);
       date_field.setText(dateFormat.format(userForData.getTime()));
       Date time = userForData.getTime();
       SimpleDateFormat timeFormat12 = new SimpleDateFormat("hh:mm a");
       time_field.setText(timeFormat12.format(time));
/*
       date_field.setOnClickListener(view -> {


           // date picker dialog
           picker = new DatePickerDialog(requireContext(),
                   (view1, year1, monthOfYear, dayOfMonth) -> {

                       userForData.set(year1,monthOfYear,dayOfMonth);
                       date_field.setText(dateFormat.format(userForData.getTime()));
                   }, year, month, day);
           picker.show();
       });

 */



       time_field.setOnClickListener(v -> {
           timepicker = new TimePickerDialog(requireContext(),
                   (tp, sHour, sMinute) -> {
                       userForData.set(Calendar.HOUR_OF_DAY,sHour);
                       userForData.set(Calendar.MINUTE,sMinute);
                       Date time1 = userForData.getTime();
                       SimpleDateFormat timeFormat121 = new SimpleDateFormat("hh:mm a");
                       time_field.setText(timeFormat121.format(time1));
                   }, hour, minutes, false);
           timepicker.show();
       });



   }





    private void editMeeting(){
        adapter.setMemberUpdateClickListener(user -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            final View customLayout = getLayoutInflater().inflate(R.layout.layout_add_member_meeting_dialog, null);
            TextView date_field = customLayout.findViewById(R.id.et_date);
            TextView time_field = customLayout.findViewById(R.id.et_time);
            TextView cancelTV = customLayout.findViewById(R.id.tv_cancel);
            TextView sendTV = customLayout.findViewById(R.id.tv_send);
            sendTV.setText(getResources().getString(R.string.edit));
            EditText name = customLayout.findViewById(R.id.et_name);
            AutoCompleteTextView phone = customLayout.findViewById(R.id.et_contact_auto);

            name.setText(user.getPersonName());
            phone.setText(user.getPersonMobile());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy, hh:mm a");
            try {
                Date date = dateFormat.parse(user.getMeetingTime());
                SimpleDateFormat timeFormat1 = new SimpleDateFormat("hh:mm a");
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMMM, yyyy");
                time_field.setText(timeFormat1.format(date));
                date_field.setText(dateFormat1.format(date));
                userForData.setTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            phone.setAdapter(usersAdapter);
            builder.setView(customLayout);
            AlertDialog dialog = builder.create();
            dialog.show();
            phone.setOnItemClickListener((parent, view, position, id) -> {
                DatumUser user1 =  userList.get(usersAdapter.getItem(position));

                //Toast.makeText(requireContext(),String.valueOf(position),Toast.LENGTH_LONG).show();
                assert user1 != null;
                name.setText(user1.getName());

            });
            sendTV.setOnClickListener(v -> {
                String nameValue = name.getText().toString();
                String phoneValue = phone.getText().toString();
                String time = time_field.getText().toString();
                String dateValue = date_field.getText().toString();
                if(nameValue.isEmpty()){
                    name.setError("Cannot Empty");
                    return;
                }
                if(phoneValue.isEmpty()){
                    phone.setError("Cannot Empty");
                    return;
                }
                if(time.isEmpty()){
                    time_field.setError("Cannot Empty");
                    return;
                }
                if(dateValue.isEmpty()){
                    date_field.setError("Cannot Empty");
                    return;
                }

                Map<String, Object> params = new HashMap<>();


                params.put("meeting_id",String.valueOf(user.getId()));
                params.put("name",nameValue);
                params.put("phone",phoneValue);
                params.put("type","1");

                Date meetTime =  userForData.getTime();
                SimpleDateFormat dateFormat12 = new SimpleDateFormat("yyyy-MM-dd H:mm");
                params.put("date_time", dateFormat12.format(meetTime));
                //Toast.makeText(requireContext(),dateFormat.format(meetTime),Toast.LENGTH_SHORT).show();
                Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().todayMeetingUpdateMember(mGeneralHelper.getToken(), params);
                toadyIMeetViewModel.memberMeetingResponse(generalResponseCall);

                toadyIMeetViewModel.getMeetingResponse().observe(getViewLifecycleOwner(), aBoolean -> {

                    if (aBoolean) {
                        getData();
                        dialog.dismiss();

                    }
                });


            });
            cancelTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();

                }
            });


/*
            int day = userForData.get(Calendar.DAY_OF_MONTH);
            int month = userForData.get(Calendar.MONTH);
            int year = userForData.get(Calendar.YEAR);

*/
            int hour = userForData.get(Calendar.HOUR_OF_DAY);
            int minutes = userForData.get(Calendar.MINUTE);


/*
            date_field.setOnClickListener(view -> {


                // date picker dialog
                picker = new DatePickerDialog(requireContext(),
                        (view1, year1, monthOfYear, dayOfMonth) -> {

                            userForData.set(year1,monthOfYear,dayOfMonth);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMMM, yyyy");
                            date_field.setText(dateFormat1.format(userForData.getTime()));
                        }, year, month, day);
                picker.show();
            });

*/



            time_field.setOnClickListener(v -> {
                timepicker = new TimePickerDialog(requireContext(),
                        (tp, sHour, sMinute) -> {
                            userForData.set(Calendar.HOUR_OF_DAY,sHour);
                            userForData.set(Calendar.MINUTE,sMinute);
                            Date time = userForData.getTime();
                            SimpleDateFormat timeFormat12 = new SimpleDateFormat("hh:mm a");
                            time_field.setText(timeFormat12.format(time));
                        }, hour, minutes, false);
                timepicker.show();
            });



        });
        adapter.setMemberDeleteClickListener(user -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    requireContext());

            builder.setMessage("Are you sure to remove this member?");
            builder.setNegativeButton("NO",
                    (dialog, which) -> {

                    });
            builder.setPositiveButton("YES",
                    (dialog, which) -> {

                        Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().todayMeetingDeleteMember(mGeneralHelper.getToken(), user.getId());
                        toadyIMeetViewModel.memberMeetingResponse(generalResponseCall);

                        toadyIMeetViewModel.getMeetingResponse().observe(getViewLifecycleOwner(), aBoolean -> {


                            if (aBoolean) {
                                getData();
                                dialog.dismiss();
                            }
                        });
                    });
            builder.show();

        });
    }



}
