package com.ICMSoft.BH_Health.ui.selfQuarantine;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.SelfQuarantine.SelfQuarantineResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelfQuarantineViewModel extends ViewModel {
    private static final String TAG = "SelfQuarantineViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<SelfQuarantineResponse> mSelfQuarantineLiveData;

    private Context context;

    public SelfQuarantineViewModel() {
        mSuccessLiveData = new MutableLiveData<>();
        mSelfQuarantineLiveData = new MutableLiveData<>();

        context = null;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public LiveData<Boolean> getGeneralResponse() {
        Log.d(TAG, "getGeneralResponse: "+mSuccessLiveData.getValue());
        return mSuccessLiveData;
    }

    public LiveData<SelfQuarantineResponse> getSelfQuarantineResponse() {
        return mSelfQuarantineLiveData;
    }


    public void getSelfQuarantine(Call<SelfQuarantineResponse> SelfQuarantineResponseCall) {

        SelfQuarantineResponseCall.enqueue(new Callback<SelfQuarantineResponse>() {
            @Override
            public void onResponse(Call<SelfQuarantineResponse> call, Response<SelfQuarantineResponse> response) {
                if (response.code() != 200) {
                    Log.d(TAG, "onResponse: "+response.code());

                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                mSelfQuarantineLiveData.setValue(response.body());
                Log.d(TAG, "onResponse: "+response.code());
               // Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<SelfQuarantineResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });

    }


    public void updateSelfQuarantine(Call<GeneralResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });
    }
    public void createSelfQuarantine(Call<GeneralResponse> responseCall) {
        responseCall.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    Log.d(TAG, "onResponse21: "+response.code());
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse11: "+response.body().isSuccess());
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });
    }

}
