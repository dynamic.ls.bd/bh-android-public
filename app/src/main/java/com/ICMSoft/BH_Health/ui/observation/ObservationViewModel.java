package com.ICMSoft.BH_Health.ui.observation;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.observetion.ObservationTypeResponse;
import com.ICMSoft.BH_Health.data.model.observetion.observationList.Datum;
import com.ICMSoft.BH_Health.data.model.observetion.observationList.ObservationListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class
ObservationViewModel extends ViewModel {

    private static final String TAG = "ObservationViewModel";
    private MutableLiveData<Boolean> observationCreate;
    private MutableLiveData<List<Datum>> observationList;
    private MutableLiveData<List<String>> observationListTag;
    private MutableLiveData<List<String>> observationListTo;
    private MutableLiveData<List<String>> observationListType;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public ObservationViewModel(){
        observationCreate = new MutableLiveData<>();
        observationList = new MutableLiveData<>();
        observationListTag = new MutableLiveData<>();
        observationListTo = new MutableLiveData<>();
        observationListType = new MutableLiveData<>();
    }

    public MutableLiveData<List<String>> getObservationListTag() {
        return observationListTag;
    }

    public MutableLiveData<List<String>> getObservationListTo() {
        return observationListTo;
    }

    public MutableLiveData<List<String>> getObservationListType() {
        return observationListType;
    }

    public MutableLiveData<Boolean> getObservationCreate() {
        return observationCreate;
    }

    public MutableLiveData<List<Datum>> getObservationList() {
        return observationList;
    }

    public void createObservation(Call<GeneralResponse> responseCall){
        responseCall.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if(response.code()!=200)
                {
                    observationCreate.setValue(null);
                    return;
                }
                Toast.makeText(context, response.body().getMessage(),Toast.LENGTH_SHORT).show();
                observationCreate.setValue(true);

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                observationCreate.setValue(null);
            }
        });
    }
    public void setObservationList(Call<ObservationListResponse> responseCall){
        responseCall.enqueue(new Callback<ObservationListResponse>() {
            @Override
            public void onResponse(Call<ObservationListResponse> call, Response<ObservationListResponse> response) {
                if(response.code()!=200){
                    observationList.setValue(null);
                    return;
                }
                observationList.setValue(response.body().getData());
            }

            @Override
            public void onFailure(Call<ObservationListResponse> call, Throwable t) {
                observationList.setValue(null);
            }
        });
    }

    public void setObservationListTo(Call<ObservationTypeResponse> responseCall){
        responseCall.enqueue(new Callback<ObservationTypeResponse>() {
            @Override
            public void onResponse(Call<ObservationTypeResponse> call, Response<ObservationTypeResponse> response) {
                if(response.code()!=200){
                    observationListTo.setValue(null);
                    return;
                }
                observationListTo.setValue(response.body().getData());
            }

            @Override
            public void onFailure(Call<ObservationTypeResponse> call, Throwable t) {
                observationListTo.setValue(null);
            }
        });
    }

    public void setObservationListTag(Call<ObservationTypeResponse> responseCall){
        responseCall.enqueue(new Callback<ObservationTypeResponse>() {
            @Override
            public void onResponse(Call<ObservationTypeResponse> call, Response<ObservationTypeResponse> response) {
                if(response.code()!=200){
                    observationListTag.setValue(null);
                    return;
                }
                observationListTag.setValue(response.body().getData());
            }

            @Override
            public void onFailure(Call<ObservationTypeResponse> call, Throwable t) {
                observationListTag.setValue(null);
            }
        });
    }

    public void setObservationListType(Call<ObservationTypeResponse> responseCall){
        responseCall.enqueue(new Callback<ObservationTypeResponse>() {
            @Override
            public void onResponse(Call<ObservationTypeResponse> call, Response<ObservationTypeResponse> response) {
                if(response.code()!=200){
                    observationListType.setValue(null);
                    return;
                }
                observationListType.setValue(response.body().getData());
            }

            @Override
            public void onFailure(Call<ObservationTypeResponse> call, Throwable t) {
                observationListType.setValue(null);
            }
        });
    }
}
