package com.ICMSoft.BH_Health.ui.emergancy;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


/**
 * A simple {@link Fragment} subclass.
 */
public class Emergency_modify extends Fragment {
    TextView mblNo1, mblNo2, mblNo3, mblNo4, mblNo5, mblNo6, mblNo7, mblNo8, mblNo9, mblNo10, mblNo11, mblNo12;
    private static final String TAG = "Emergency_modify";
    @BindView(R.id.text_layout)
    LinearLayout textLayout;

    @BindView(R.id.voice_layout)
    LinearLayout voiceLayout;

    @BindView(R.id.call_layout)
    LinearLayout callLayout;

    @BindView(R.id.emergency_message)
    RadioButton emergencyMessageBtn;
    @BindView(R.id.emergency_voice)
    RadioButton emergencyVoiceBtn;
    @BindView(R.id.emergency_call)
    RadioButton emergencyCallBtn;

    @BindView(R.id.emergency_group)
    RadioGroup emergencyGroup;

    @BindView(R.id.recording_btn)
    Button recordingButton;
    @BindView(R.id.play_btn)
    Button playingButton;

    @BindView(R.id.send_voice)
    Button sendVoice;

    @BindView(R.id.timerValue)
    TextView timerValue;

    @BindView(R.id.activity_write_field)
    EditText txtMessage;

    @BindView(R.id.hospital_layout)
    LinearLayout hospital_layout;

    @BindView(R.id.ambulance_layout)
    LinearLayout ambulance_layout;

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    private Boolean recordToggle, playingToggle;
    private String startRecord = "Start Recording";
    private String restartRecord = "Restart Recording";
    private String stopRecord = "Stop Recording";
    private String startPlaying = "Start Playing";
    private String stopPlaying = "Stop Playing";

    private static final String LOG_TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static String fileName = null;
    private SimpleDateFormat sd;
    private GeneralHelper mGeneralHelper;
    private EmergancyViewModel emergancyViewModel;

    private MediaRecorder recorder = null;

    private MediaPlayer player = null;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private String[] permissions = {Manifest.permission.RECORD_AUDIO};



    public Emergency_modify() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_emergency_modify, container, false);
        ButterKnife.bind(this, view);
        playingButton.setVisibility(View.GONE);
        recordingButton.setText(startRecord);
        recordToggle = true;
        playingButton.setText(startPlaying);
        playingToggle = true;
        emergancyViewModel = new ViewModelProvider(this).get(EmergancyViewModel.class);
        mGeneralHelper = new GeneralHelper(requireContext());
        sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        fileName = requireActivity().getCacheDir().getAbsolutePath();
        fileName += "/audioMessage.amr";
        sendVoice.setVisibility(View.GONE);
        voiceLayout.setVisibility(View.GONE);
        callLayout.setVisibility(View.GONE);
        emergencyGroup.check(R.id.emergency_message);
        emergencyMessageBtn.setTextColor(Color.WHITE);
        return view;
    }

    @OnClick(R.id.emergency_message)
    public void getMessage() {
        voiceLayout.setVisibility(View.GONE);
        callLayout.setVisibility(View.GONE);
        textLayout.setVisibility(View.VISIBLE);
        emergencyMessageBtn.setTextColor(Color.WHITE);
        emergencyVoiceBtn.setTextColor(Color.BLACK);
        emergencyCallBtn.setTextColor(Color.BLACK);
    }

    @OnClick(R.id.emergency_voice)
    public void getVoice() {
        textLayout.setVisibility(View.GONE);
        callLayout.setVisibility(View.GONE);
        voiceLayout.setVisibility(View.VISIBLE);
        emergencyMessageBtn.setTextColor(Color.BLACK);
        emergencyVoiceBtn.setTextColor(Color.WHITE);
        emergencyCallBtn.setTextColor(Color.BLACK);
    }

    @OnClick(R.id.emergency_call)
    public void getCall() {
        voiceLayout.setVisibility(View.GONE);
        textLayout.setVisibility(View.GONE);
        callLayout.setVisibility(View.VISIBLE);
        emergencyMessageBtn.setTextColor(Color.BLACK);
        emergencyVoiceBtn.setTextColor(Color.BLACK);
        emergencyCallBtn.setTextColor(Color.WHITE);
    }

    @OnClick(R.id.btn_hospital)
    public void getHospital() {
        hospital_layout.setVisibility(View.VISIBLE);
        ambulance_layout.setVisibility(View.GONE);

    }

    @OnClick(R.id.btn_ambulance)
    public void getAmbulance() {
        hospital_layout.setVisibility(View.GONE);
        ambulance_layout.setVisibility(View.VISIBLE);
    }






    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            //voice
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
            //text
            case 0: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MyMessage();

                } else {
                    Toast.makeText(getContext(),
                            "SMS failed, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
        //     if (!permissionToRecordAccepted )
        //finish();

    }

    //voice
    public void reset() {
        startTime = 0L;
        timeInMilliseconds = 0L;
        timeSwapBuff = 0L;
        updatedTime = 0L;

        startTime = SystemClock.uptimeMillis();
        customHandler.postDelayed(updateTimerThread, 0);
    }

    public void stopWatch() {
        timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);
    }

    @OnClick(R.id.recording_btn)
    public void recording() {

        if (recordToggle) {
            if (ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
            { stopping();
            recordingButton.setText(stopRecord);
            reset();
            //fileName = getActivity().getExternalCacheDir().getAbsolutePath();
            Date date = new Date();

            // fileName += "/"+sd.format(date)+".amr";
            startRecording();
            recordToggle = false;
        }
            else
                ActivityCompat.requestPermissions(getActivity(),permissions,0);

        } else {
            recordToggle = true;
            recordingButton.setText(restartRecord);
            stopWatch();
            stopRecording();
            playingButton.setVisibility(View.VISIBLE);
            sendVoice.setVisibility(View.VISIBLE);
            Toast.makeText(getContext(), "Record Finished", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.play_btn)
    public void playing() {

        if (playingToggle) {
            stopping();
            playingButton.setText(stopPlaying);
            playingToggle = false;
            reset();
            startPlaying();
        } else {
            playingToggle = true;
            playingButton.setText(startPlaying);
            stopPlaying();
            stopWatch();

        }
    }


    private void startPlaying() {
        stopping();
        player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.prepare();
            player.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        player.release();
        player = null;
    }

    private void startRecording() {
        stopping();
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }

    private void stopRecording() {

        recorder.stop();
        recorder.release();
        recorder = null;
    }

    public void stopping() {
        if (recorder != null) {
            recorder.release();
            recorder = null;
            recordingButton.setText(restartRecord);
            recordToggle = true;
        }

        if (player != null) {
            player.release();
            player = null;
            playingToggle = true;
            playingButton.setText(startPlaying);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopping();
    }

    @OnClick(R.id.send_voice)
    public void sendVoice() {
        stopping();
        //Toast.makeText(getContext(),"Work will coming soon",Toast.LENGTH_SHORT).show();
        createVoice();
    }


    public void createVoice() {

        final Map<String, Object> params = new HashMap<>();
        params.put("type", "voice");
        File file = new File(fileName);
        Log.d(TAG, "create: " + Uri.fromFile(new File(fileName)));
        Log.d(TAG, "create: " + getActivity().getApplicationContext().getContentResolver().getType(Uri.fromFile(new File(fileName))));
        RequestBody type = RequestBody.create(MultipartBody.FORM, "voice");
        RequestBody filepart = RequestBody.create(MediaType.parse("audio/amr"), file);
        MultipartBody.Part record = MultipartBody.Part.createFormData("message", file.getName(), filepart);

        // Uri fileUri = Uri.fromFile(new File(fileName));
        String s = file.getName();
        Log.d(TAG, "create: " + file.getName());

        emergancyViewModel.setContext(getContext());
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().createEmergancyVoice(mGeneralHelper.getToken(), type, record);
        emergancyViewModel.createEmergancy(responseCall);

        emergancyViewModel.getGeneralResponse().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                //Log.d(TAG, "onChanged: "+aBoolean);
                if (!aBoolean)
                    Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
                //  else
                //Toast.makeText(getContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            timerValue.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);

            if (player != null && !player.isPlaying())
                playing();
        }
    };


    //text

    @OnClick(R.id.send_text)
    public void sendMessage() {

        // MyMessage();
        createMessage();
        txtMessage.setText("");

    }

    private void MyMessage() {

        // phoneNo = txtphoneNo.getText().toString().trim();
        String message = txtMessage.getText().toString().trim();
        if (!message.isEmpty()) {

            createMessage();
        } else {

            return;
        }
    }

    public void createMessage() {

        final Map<String, Object> params = new HashMap<>();
        params.put("type", "text");
        params.put("message", txtMessage.getText().toString());


        Log.d(TAG, "create: " + params);
        emergancyViewModel.setContext(getContext());
        Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().createEmergancy(mGeneralHelper.getToken(), params);
        emergancyViewModel.createEmergancy(responseCall);
        Log.d(TAG, "create: " + responseCall);

        emergancyViewModel.getGeneralResponse().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Log.d(TAG, "onChanged: " + aBoolean);
                if (aBoolean)
                    Toast.makeText(getContext(), "SMS sent", Toast.LENGTH_SHORT).show();
                //   else
                //  Toast.makeText(getContext(), "SMS failed, please try again", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
