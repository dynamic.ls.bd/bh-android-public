package com.ICMSoft.BH_Health.ui.notifications;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.notification.Datum;
import com.ICMSoft.BH_Health.data.model.notification.GetNotification;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<List<Datum>> mNotifications;

    public NotificationsViewModel() {
        mText = new MutableLiveData<>();
        mNotifications = new MutableLiveData<>();
        mText.setValue("This is notifications fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<List<Datum>> getNotificationResponse() {
        return mNotifications;
    }
    public void getNotification(Call<GetNotification> responseCall) {
        responseCall.enqueue(new Callback<GetNotification>() {
            @Override
            public void onResponse(Call<GetNotification> call, Response<GetNotification> response) {
                if (response.code() != 200) {
                    mNotifications.setValue(null);
                    return;
                }

                List<Datum> data = response.body().getData();
                mNotifications.setValue(data);

            }

            @Override
            public void onFailure(Call<GetNotification> call, Throwable t) {
                mNotifications.setValue(null);
            }
        });
    }
}