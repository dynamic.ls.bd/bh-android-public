package com.ICMSoft.BH_Health.ui.Utils;


import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ICMSoft.BH_Health.R;
import com.airbnb.lottie.LottieAnimationView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Loading {

    private View object;

    @BindView(R.id.no_network_view)
    RelativeLayout noNetworkView;
    @BindView(R.id.no_network)
    LottieAnimationView noNetwork;

    @BindView(R.id.loading_view)
    LinearLayout loadingAnimation;

    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    private boolean flag;

    private Timer timer;
    private TimerTask task;
    private Handler handler = new Handler();
    public Loading(View view, View object) {
        ButterKnife.bind(this, view);
        this.object = object;
        flag = true;

    }


    private void setTask() {
        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    object.setVisibility(View.GONE);
                    loadingAnimation.setVisibility(View.GONE);
                    animationView.cancelAnimation();
                    noNetworkView.setVisibility(View.VISIBLE);
                    noNetwork.playAnimation();

                });
            }
        };
    }

    private void startTimer() {
        timer = new Timer();
        setTask();
        timer.schedule(task, 20000);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void startLoading() {
        if (flag) {
            object.setVisibility(View.GONE);
            loadingAnimation.setVisibility(View.VISIBLE);
            animationView.playAnimation();
            startTimer();
            flag = false;
        }
    }

    public void stopLoading() {
        if (!flag) {
            object.setVisibility(View.VISIBLE);
            loadingAnimation.setVisibility(View.GONE);
            animationView.cancelAnimation();
            noNetworkView.setVisibility(View.GONE);
            noNetwork.cancelAnimation();
            stopTimer();
        }
    }

}
