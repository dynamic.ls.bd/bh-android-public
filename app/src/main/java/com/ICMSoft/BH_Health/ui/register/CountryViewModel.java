package com.ICMSoft.BH_Health.ui.register;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Register.Country.CountryData;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryListResponse;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryViewModel extends ViewModel {
    private static final String TAG = "CountryViewModel";
    private MutableLiveData<List<CountryData>> mCountryData;
    public CountryViewModel() {
        mCountryData = new MutableLiveData<>();
    }
    public LiveData<List<CountryData>> getCountryResponse() {
        return mCountryData;
    }

    public void getCountry(Call<CountryListResponse> responseCall) {
        Log.d(TAG, "getCountry: "+responseCall);
        responseCall.enqueue(new Callback<CountryListResponse>() {
            @Override
            public void onResponse(Call<CountryListResponse> call, Response<CountryListResponse> response) {
                Log.d(TAG, "onResponse: "+response.code());
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                List<CountryData> data = response.body().getData();
                Log.d(TAG, "onResponse: "+data);
                mCountryData.setValue(data);

            }

            @Override
            public void onFailure(Call<CountryListResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
}
