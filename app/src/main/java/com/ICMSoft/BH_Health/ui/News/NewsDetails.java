package com.ICMSoft.BH_Health.ui.News;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.News.Details.Data;
import com.ICMSoft.BH_Health.data.model.News.Details.NewsDetailsResponse;
import com.ICMSoft.BH_Health.data.model.beSafe.PicassoImageGetter;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.airbnb.lottie.LottieAnimationView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.News.NewsFragment.newsPosition;

public class NewsDetails extends Fragment {
    private static final String TAG = "NewsFragment";
    private NewsViewModel newsViewModel;
    private GeneralHelper mGeneralHelper;

    @BindView(R.id.details_news)
    TextView detailsNews;
    @BindView(R.id.title_details_news)
    TextView title;


    @BindView(R.id.news_details_view)
    ScrollView newsDetailsView;

    Loading loading;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.news_details, container, false);
        ButterKnife.bind(this,view);
        loading = new Loading(view,newsDetailsView);
        newsViewModel = new ViewModelProvider(this).get(NewsViewModel.class);
        mGeneralHelper = new GeneralHelper(getContext());


        getData();
        return view;
    }



    private void getData() {
        loading.startLoading();
        Call<NewsDetailsResponse> call = mGeneralHelper.getApiService().getNewsDetails( "news/" + (newsPosition+1));
        newsViewModel.newsDetails(call);

        newsViewModel.getNewsDetailsResponse().observe(getViewLifecycleOwner(), new Observer<Data>() {
            @Override
            public void onChanged(Data datum) {
                if (datum == null) {
                    // titleTV.setText("");
                    // bodyTV.setText("No Data Found");
                    loading.stopLoading();
                    return;
                }
                loading.stopLoading();
             //   PicassoImageGetter imageGetter = new PicassoImageGetter(getContext(),detailsNews,datum.getImage());
                Spannable html;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    html = (Spannable) Html.fromHtml(datum.getContent(), Html.FROM_HTML_MODE_LEGACY,null ,null);
                } else {
                    html = (Spannable) Html.fromHtml(datum.getContent(), null, null);
                }

                detailsNews.setText(html);
                title.setText(datum.getTitle());
                Log.d(TAG, "onChanged: "+datum.getTitle());


            }
        });
    }
}
