package com.ICMSoft.BH_Health.ui.Utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePickerCalander {
    private TextView date;
    private Context context;
    private Calendar cldr,changedCalander;
    private SimpleDateFormat dateFormat;


    public DatePickerCalander(TextView date, Context context) {
        this.date = date;
        this.context = context;
        changedCalander = cldr = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        setTodayDate();
        editOnClick();
    }

    public void setTodayDate(){
        date.setText(dateFormat.format(cldr.getTime()));
        changedCalander = Calendar.getInstance();
    }
    public void setYestardayDate(){

        changedCalander = Calendar.getInstance();
        changedCalander.add(Calendar.DAY_OF_MONTH,-1);
        date.setText(dateFormat.format(changedCalander.getTime()));
    }



    private void editOnClick(){
        date.setOnClickListener(v -> setDate());
    }




    private void setDate() {
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        DatePickerDialog picker = new DatePickerDialog(context,
                (view, year1, monthOfYear, dayOfMonth) -> {
                    date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year1);
                    changedCalander.set(year1, monthOfYear, dayOfMonth);
                }, year, month, day);

        picker.getDatePicker().setMaxDate(Calendar.getInstance().getTime().getTime());
        picker.show();
    }

    public Date getDate() {
        return changedCalander.getTime();
    }

}
