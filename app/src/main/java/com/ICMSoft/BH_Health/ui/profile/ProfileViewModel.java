package com.ICMSoft.BH_Health.ui.profile;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.profile.error.ProfileError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.profile.Data;
import com.ICMSoft.BH_Health.data.model.profile.ProfileResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileViewModel extends ViewModel {

    private static final String TAG = "ProfileViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<Data> mProfileLiveData;
    private Context context;

    public ProfileViewModel() {
        mSuccessLiveData = new MutableLiveData<>();
        mProfileLiveData = new MutableLiveData<>();
    }

    public LiveData<Boolean> getGeneralResponse() {
        return mSuccessLiveData;
    }

    public LiveData<Data> getProfileResponse() {
        return mProfileLiveData;
    }

    public void getProfile(Call<ProfileResponse> profileResponseCall) {

        profileResponseCall.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));


                    return;
                }
                mProfileLiveData.setValue(response.body().getData());

            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void updateProfile(Call<GeneralResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);

                    Gson gson = new Gson();
                    ProfileError message=gson.fromJson(response.errorBody().charStream(),ProfileError.class);
                    if(message.getError().getEmail()!= null && message.getError().getPhone() != null && message.getError().getName()!=null)
                    {
                        Toast.makeText(context,message.getError().getEmail().get(0)+"\n"+message.getError().getPhone().get(0)+"\n"+message.getError().getName(),Toast.LENGTH_SHORT).show();
                    }
                    else if(message.getError().getEmail()!= null)
                    {
                        Toast.makeText(context,message.getError().getEmail().get(0),Toast.LENGTH_SHORT).show();
                    }
                    else if(message.getError().getPhone() != null )
                    {
                        Toast.makeText(context,message.getError().getPhone().get(0),Toast.LENGTH_SHORT).show();
                    }
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessLiveData.setValue(true);
               // Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });
    }

}
