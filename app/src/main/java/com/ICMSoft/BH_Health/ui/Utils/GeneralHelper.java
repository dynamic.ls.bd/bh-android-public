package com.ICMSoft.BH_Health.ui.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.ICMSoft.BH_Health.Services.ApiService;
import com.ICMSoft.BH_Health.Services.RetrofitClient;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.AccessToken;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.UserPref;

public class GeneralHelper {

    private Context mContext;
    private SharedPreferences sharedPreferences;


    public GeneralHelper(Context context) {
        this.mContext = context;
        sharedPreferences = context.getSharedPreferences(UserPref, Context.MODE_PRIVATE);

    }

    public ApiService getApiService() {
        return  RetrofitClient.getRetrofitClient(mContext).create(ApiService.class);
    }

    public String getToken() {
        return "Bearer "+sharedPreferences.getString(AccessToken, null);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

}
