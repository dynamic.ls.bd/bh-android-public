package com.ICMSoft.BH_Health.ui.safe_cirle.todayIVisit;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.TodayIVisitAdapter;
import com.ICMSoft.BH_Health.data.model.Circle.todayIVisit.TodayIVisitResponse;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;


/**
 * A simple {@link Fragment} subclass.
 */
public class TodayVisit extends Fragment {

    private TimePickerDialog timepicker;

    @BindView(R.id.visiting)
    RecyclerView visiting;
    private LinearLayoutManager mLinearLayoutManager;
    private TodayIVisitViewModel todayIVisitViewModel;
    private GeneralHelper mGeneralHelper;
    private Calendar userForData;
    private TodayIVisitAdapter adapter;
    @BindView(R.id.visitingView)
    CoordinatorLayout visitingView;
    private Loading loading;
    public TodayVisit() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_today_visit, container, false);
        ButterKnife.bind(this,view);
        loading = new Loading(view,visitingView);
        todayIVisitViewModel = new ViewModelProvider(this).get(TodayIVisitViewModel.class);

        todayIVisitViewModel.setContext(requireContext());
        mGeneralHelper = new GeneralHelper(requireContext());
        userForData = Calendar.getInstance();
        getData();
        return view;
    }


    private void getData(){
        loading.startLoading();

        Call<TodayIVisitResponse> responseCall = mGeneralHelper.getApiService().todayVisitingGetMember(mGeneralHelper.getToken());
        todayIVisitViewModel.visitDataResponse(responseCall);
        todayIVisitViewModel.getDataResponse().observe(getViewLifecycleOwner(), data -> {
            loading.stopLoading();
            if (data != null) {
                mLinearLayoutManager = new LinearLayoutManager(getContext());
                visiting.setLayoutManager(mLinearLayoutManager);

                visiting.setVisibility(View.VISIBLE);
                visiting.setHasFixedSize(true);
                adapter = new TodayIVisitAdapter(data);
                visiting.setAdapter(adapter);

                editVisiting();
            }
            else
                visiting.setVisibility(View.GONE);

        });
    }


    @OnClick(R.id.meeting_member_add)
    public void addMember() {

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        final View customLayout = getLayoutInflater().inflate(R.layout.layout_visiting_dialog, null);
        TextView date_field = customLayout.findViewById(R.id.et_date);
        TextView time_field = customLayout.findViewById(R.id.et_time);


        TextView cancelTV = customLayout.findViewById(R.id.tv_cancel);
        TextView sendTV = customLayout.findViewById(R.id.tv_send);
        TextView place = customLayout.findViewById(R.id.et_place);
        //   AutoCompleteTextView text_mbl = customLayout.findViewById(R.id.et_contact_auto);
        //  ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1, mbl_no);
        //   text_mbl.setAdapter(adapter);


        //   setCategorySpinner(circleSpinner);
        builder.setView(customLayout);
        AlertDialog dialog = builder.create();
        dialog.show();

        sendTV.setOnClickListener(v -> {

            String placeValue = place.getText().toString();
            String time1 = time_field.getText().toString();
            String dateField = date_field.getText().toString();
            if(placeValue.isEmpty()){
                place.setError("Cannot Empty");
                return;
            }
            if(time1.isEmpty()){
                time_field.setError("Cannot Empty");
                return;
            }
            if(dateField.isEmpty()){
                date_field.setError("Cannot Empty");
                return;
            }
            Map<String, Object> params = new HashMap<>();

            params.put("address",placeValue);

            Date meetTime =  userForData.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H:mm");
            params.put("date_time",dateFormat.format(meetTime));
            //Toast.makeText(getContext(),dateFormat.format(meetTime),Toast.LENGTH_SHORT).show();
            Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().todayVisitingCreateMember(mGeneralHelper.getToken(), params);
            todayIVisitViewModel.visitMeetingResponse(generalResponseCall);

            todayIVisitViewModel.getVisitingResponse().observe(getViewLifecycleOwner(), aBoolean -> {

                if (aBoolean) {
                     getData();
                    dialog.dismiss();
                    // Toast.makeText(getContext(), "Circle Member added successfully", Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getContext(), "Data Store Failed", Toast.LENGTH_SHORT).show();
            });




        });

        cancelTV.setOnClickListener(view -> dialog.dismiss());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy");

        userForData = Calendar.getInstance();
        date_field.setText(dateFormat.format(userForData.getTime()));
        Date time12 = userForData.getTime();
        SimpleDateFormat timeFormat12 = new SimpleDateFormat("hh:mm a");
        time_field.setText(timeFormat12.format(time12));

/*
        date_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(getContext(),
                        (view1, year1, monthOfYear, dayOfMonth) -> {
                            userForData.set(year1,monthOfYear,dayOfMonth);
                            date_field.setText(dateFormat.format(userForData.getTime()));
                        }, year, month, day);
                picker.show();
            }
        });

 */

        time_field.setOnClickListener(v -> {
            final Calendar cldr = Calendar.getInstance();
            int hour = cldr.get(Calendar.HOUR_OF_DAY);
            int minutes = cldr.get(Calendar.MINUTE);
            // time picker dialog
            timepicker = new TimePickerDialog(getContext(),
                    (tp, sHour, sMinute) -> {
                        userForData.set(Calendar.HOUR_OF_DAY,sHour);
                        userForData.set(Calendar.MINUTE,sMinute);
                        Date time13 = userForData.getTime();
                        SimpleDateFormat timeFormat121 = new SimpleDateFormat("hh:mm a");
                        time_field.setText(timeFormat121.format(time13));
                    }, hour, minutes, false);
            timepicker.show();
        });



    }


    private void editVisiting(){
        adapter.setVisitUpdateClickListener(user -> {
// create an alert builder
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            // builder.setTitle("Add Member");
            // set the custom layout




            final View customLayout = getLayoutInflater().inflate(R.layout.layout_visiting_dialog, null);
            TextView date_field = customLayout.findViewById(R.id.et_date);
            TextView time_field = customLayout.findViewById(R.id.et_time);


            TextView cancelTV = customLayout.findViewById(R.id.tv_cancel);
            TextView sendTV = customLayout.findViewById(R.id.tv_send);
            TextView place = customLayout.findViewById(R.id.et_place);
            place.setText(user.getVisitAddress());

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy, hh:mm a");
            try {
                Date date1 = dateFormat.parse(user.getTime());
                SimpleDateFormat timeFormat1 = new SimpleDateFormat("hh:mm a");
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMMM, yyyy");
                time_field.setText(timeFormat1.format(date1));
                date_field.setText(dateFormat1.format(date1));
                userForData.setTime(date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            builder.setView(customLayout);
            AlertDialog dialog = builder.create();
            dialog.show();

            sendTV.setOnClickListener(v -> {

                String placeValue = place.getText().toString();
                String time = time_field.getText().toString();
                String dateField = date_field.getText().toString();
                if(placeValue.isEmpty()){
                    place.setError("Cannot Empty");
                    return;
                }
                if(time.isEmpty()){
                    time_field.setError("Cannot Empty");
                    return;
                }
                if(dateField.isEmpty()){
                    date_field.setError("Cannot Empty");
                    return;
                }
                Map<String, Object> params = new HashMap<>();
                params.put("visit_id",user.getId());

                params.put("address",placeValue);

                Date meetTime =  userForData.getTime();
                SimpleDateFormat dateFormat12 = new SimpleDateFormat("yyyy-MM-dd H:mm");
                params.put("date_time", dateFormat12.format(meetTime));
                Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().todayVisitingUpdateMember(mGeneralHelper.getToken(), params);
                todayIVisitViewModel.visitMeetingResponse(generalResponseCall);

                todayIVisitViewModel.getVisitingResponse().observe(getViewLifecycleOwner(), aBoolean -> {

                    if (aBoolean) {
                        getData();
                        dialog.dismiss();
                        // Toast.makeText(getContext(), "Circle Member added successfully", Toast.LENGTH_SHORT).show();
                    }else
                        Toast.makeText(getContext(), "Data Store Failed", Toast.LENGTH_SHORT).show();
                });




            });

            cancelTV.setOnClickListener(view -> dialog.dismiss());
            int hour = userForData.get(Calendar.HOUR_OF_DAY);
            int minutes = userForData.get(Calendar.MINUTE);

/*
            date_field.setOnClickListener(view -> {


                // date picker dialog
                picker = new DatePickerDialog(requireContext(),
                        (view1, year1, monthOfYear, dayOfMonth) -> {
                            userForData.set(year1,monthOfYear,dayOfMonth);
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd MMMM, yyyy");
                            date_field.setText(dateFormat1.format(userForData.getTime()));
                        }, year, month, day);
                picker.show();
            });

 */

            time_field.setOnClickListener(v -> {
                // time picker dialog
                timepicker = new TimePickerDialog(getContext(),
                        (tp, sHour, sMinute) -> {
                            userForData.set(Calendar.HOUR_OF_DAY,sHour);
                            userForData.set(Calendar.MINUTE,sMinute);
                            Date time = userForData.getTime();
                            SimpleDateFormat timeFormat12 = new SimpleDateFormat("hh:mm a");
                            time_field.setText(timeFormat12.format(time));
                        }, hour, minutes, false);
                timepicker.show();
            });


        });
        adapter.setVisitDeleteClickListener(user -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    requireContext());

            builder.setMessage("Are you sure to remove this member?");
            builder.setNegativeButton("NO",
                    (dialog, which) -> dialog.dismiss());
            builder.setPositiveButton("YES",
                    (dialog, which) -> {
                        loading.startLoading();
                        Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().todayVisitingDeleteMember(mGeneralHelper.getToken(), user.getId());
                        todayIVisitViewModel.visitMeetingResponse(generalResponseCall);

                        todayIVisitViewModel.getVisitingResponse().observe(getViewLifecycleOwner(), aBoolean -> {


                            if (aBoolean) {
                                getData();
                                dialog.dismiss();
                            }
                        });
                    });
            builder.show();

        });
    }
}
