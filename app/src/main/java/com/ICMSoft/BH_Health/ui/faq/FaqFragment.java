package com.ICMSoft.BH_Health.ui.faq;


import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.FaqAdapter;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.faq.Datum;
import com.ICMSoft.BH_Health.data.model.faq.FaqResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.ICMSoft.BH_Health.ui.Utils.RadioMenuSelection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment {

    private FaqViewModel faqViewModel;
    private GeneralHelper mGeneralHelper;
    private String selectedCategory;
    @BindView(R.id.faq_rv)
    RecyclerView mFaqRV;

    @BindView(R.id.categorySpinner)
    Spinner mCategorySpinner;

    @BindView(R.id.et_to)
    EditText mToET;

    @BindView(R.id.et_subject)
    EditText mSubjectET;


    @BindView(R.id.et_message)
    EditText mMessageET;

    @BindView(R.id.faq_view)
    NestedScrollView faqView;

    @BindView(R.id.btn_question)
    RadioButton questions;
    @BindView(R.id.btn_comments)
    RadioButton comments;
    @BindView(R.id.btn_suggestions)
    RadioButton suggestions;
    @BindView(R.id.btn_contribute)
    RadioButton contributions;

    private Loading loading;
    private boolean flag;
    private String selectedMenu;

    //private ArrayList<Datum> lists;
    private Map<String, ArrayList<Datum>> listMap;
    private Map<String, RadioButton> buttonMap;
    private RadioMenuSelection menuSelection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view, faqView);
        faqViewModel = new ViewModelProvider(this).get(FaqViewModel.class);
        flag = true;
        listMap = new HashMap<>();
        mGeneralHelper = new GeneralHelper(requireContext());
        setCategorySpinner(mCategorySpinner);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(requireContext());
        mFaqRV.setLayoutManager(mLinearLayoutManager);
        menuButtonSetup();
        menuSelection = new RadioMenuSelection(buttonMap);
        selectedMenu = "question";
        getFaq();

        return view;
    }

    private void menuButtonSetup() {
        buttonMap = new HashMap<>();
        buttonMap.put("question", questions);
        buttonMap.put("suggestion", suggestions);
        buttonMap.put("contribution", contributions);
        buttonMap.put("comment", comments);
    }

    private void getFaq() {
        loading.startLoading();
        final Map<String, Object> params = new HashMap<>();
        params.put("load", 1);

        final Call<FaqResponse> responseCall = mGeneralHelper.getApiService().getFaq(mGeneralHelper.getToken(), params);
        faqViewModel.getFaq(responseCall);

        faqViewModel.getFaqResponse().observe(getViewLifecycleOwner(), data -> {
            loading.stopLoading();
            listMap.clear();
            if (data == null) {

                return;
            }
            ArrayList<Datum> question = new ArrayList<>();
            ArrayList<Datum> suggestion = new ArrayList<>();
            ArrayList<Datum> contribution = new ArrayList<>();
            ArrayList<Datum> comment = new ArrayList<>();


            for (Datum d : data) {
                if (d.getCategory().compareToIgnoreCase("question") == 0)
                    question.add(d);
                else if (d.getCategory().compareToIgnoreCase("comment") == 0)
                    comment.add(d);
                else if (d.getCategory().equalsIgnoreCase("suggestion"))
                    suggestion.add(d);
                else if (d.getCategory().equalsIgnoreCase("contribution"))
                    contribution.add(d);
            }
            listMap.put("question", question);
            listMap.put("suggestion", suggestion);
            listMap.put("contribution", contribution);
            listMap.put("comment", comment);
            seletedView();

        });

    }

    private void seletedView() {
        FaqAdapter faqAdapter = new FaqAdapter(listMap.get(selectedMenu));
        menuSelection.seletedItem(selectedMenu, mFaqRV, faqAdapter);
    }

    @OnClick(R.id.btn_question)
    void questionClicked() {

        selectedMenu = "question";
        seletedView();
    }


    @OnClick(R.id.btn_comments)
    void commentsClicked() {
        selectedMenu = "comment";
        seletedView();
    }

    @OnClick(R.id.btn_contribute)
    void contributionClicked() {
        selectedMenu = "contribution";
        seletedView();
    }

    @OnClick(R.id.btn_suggestions)
    void suggestionsClicked() {
        selectedMenu = "suggestion";
        seletedView();
    }


    private void setCategorySpinner(Spinner spinner) {

        ArrayList<String> categories = new ArrayList<>();
        categories.add("Question");
        categories.add("Comment");
        categories.add("Suggestion");
        categories.add("Contribution");

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        ///On item click
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedCategory = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @OnClick(R.id.sendTV)
    void saveQuestion() {


        if (selectedCategory == null) {
            Toast.makeText(requireContext(), "No category Selected", Toast.LENGTH_SHORT).show();
            return;
        }

        String to = mToET.getText().toString();
        String subject = mSubjectET.getText().toString();
        String message = mMessageET.getText().toString();


        if (TextUtils.isEmpty(to)) {
            mToET.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(subject)) {
            mSubjectET.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(message)) {
            mMessageET.setError("Cannot be empty");
            return;
        }


        Map<String, String> params = new HashMap<>();
        params.put("category", selectedCategory.toLowerCase());
        params.put("to", to);
        params.put("subject", subject);
        params.put("message", message);

        flag = true;
        final Call<GeneralResponse> responseCall = mGeneralHelper.getApiService().storeQuestion(mGeneralHelper.getToken(), params);
        faqViewModel.saveQuestion(responseCall);

        faqViewModel.getStoreQuestionResponse().observe(getViewLifecycleOwner(), generalResponse -> {

            if (generalResponse.isSuccess()) {
                if (flag) {
                    getFaq();
                    Toast.makeText(requireContext(), " " + generalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    flag = false;
                }
            } else
                Toast.makeText(requireContext(), " " + generalResponse.getMessage(), Toast.LENGTH_SHORT).show();
        });

    }

}
