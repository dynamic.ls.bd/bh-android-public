package com.ICMSoft.BH_Health.ui.Announcement;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Announcement.AnnouncementResponse;
import com.ICMSoft.BH_Health.data.model.Announcement.Datum;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnnouncementViewModel extends ViewModel {

    private static final String TAG = "AnnouncementViewModel";


    private MutableLiveData<List<Datum>> newsList = new MutableLiveData<>();
    private MutableLiveData<Data> announcementDetails = new MutableLiveData<>();
    MutableLiveData<List<Datum>> getNewsListResponse() {
        return newsList;
    }
    MutableLiveData<Data> getAnnouncementDetailsResponse() {
        return announcementDetails;
    }

    public void announcement(Call<AnnouncementResponse> responseCall) {
        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<AnnouncementResponse>() {
            @Override
            public void onResponse(Call<AnnouncementResponse> call, Response<AnnouncementResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                List<Datum> data = response.body().getData();
                newsList.setValue(data);

            }

            @Override
            public void onFailure(Call<AnnouncementResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void announcementDetails(Call<AnnouncementDetailsResponse> responseCall) {
        Log.d(TAG, "news: ");
        responseCall.enqueue(new Callback<AnnouncementDetailsResponse>() {
            @Override
            public void onResponse(Call<AnnouncementDetailsResponse> call, Response<AnnouncementDetailsResponse> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                Data data = response.body().getData();
                announcementDetails.setValue(data);

            }

            @Override
            public void onFailure(Call<AnnouncementDetailsResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void getQuestionList(Call<GeneralResponse> responseCall) {


    }
}
