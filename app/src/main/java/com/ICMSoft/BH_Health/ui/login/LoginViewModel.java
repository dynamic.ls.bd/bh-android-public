package com.ICMSoft.BH_Health.ui.login;

import android.content.Context;
import android.util.Log;
import android.util.Patterns;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Auth.ForgetPassword.ForgetPassword;
import com.ICMSoft.BH_Health.data.model.Auth.ForgetPassword.ForgetPasswordError;
import com.ICMSoft.BH_Health.data.model.Auth.ResetError.ResetError;
import com.ICMSoft.BH_Health.data.model.Auth.ResetPassword.ResetPassword;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.LoginRepository;
import com.ICMSoft.BH_Health.data.model.Auth.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    private static final String TAG = "Login View model";

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private MutableLiveData<LoginResponse> loginResponse = new MutableLiveData<>();
    private MutableLiveData<Boolean> forgetPassword = new MutableLiveData<>();
    private MutableLiveData<Boolean> resetPassword = new MutableLiveData<>();
    private LoginRepository loginRepository;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<Boolean> getforgetPassword() {
        return forgetPassword;
    }
    LiveData<Boolean> getResetPassword() {
        return resetPassword;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    LiveData<LoginResponse> getLoginResponse() {
        return loginResponse;
    }

    public void login(Call<LoginResponse> loginCall) {

        loginCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() != 200) {
                    loginResult.setValue(new LoginResult(R.string.login_failed));
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Log.d(TAG, "onResponse: " + response.code());
                    return;
                }

                LoginResponse res = response.body();
                Log.d(TAG, "onResponse: " + res.getData());
                //Save state
                loginResult.setValue(new LoginResult(new LoggedInUserView(res.getData().getName())));
                loginResponse.setValue(response.body());


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });


        // can be launched in a separate asynchronous job
  /*      Result<LoggedInUser> result = loginRepository.login(username, password);
        if (result instanceof Result.Success) {
            LoggedInUser data = ((Result.Success<LoggedInUser>) result).getData();
            loginResult.setValue(new LoginResult(new LoggedInUserView(data.getDisplayName())));
        } else {
            loginResult.setValue(new LoginResult(R.string.login_failed));
        }*/
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }


    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 8;
    }

    public void forgetPassword(Call<ForgetPassword> loginCall) {

        loginCall.enqueue(new Callback<ForgetPassword>() {
            @Override
            public void onResponse(Call<ForgetPassword> call, Response<ForgetPassword> response) {
                if (response.code() != 200) {
                    forgetPassword.setValue(false);
                    Gson gson = new Gson();

                    ForgetPasswordError message = gson.fromJson(response.errorBody().charStream(), ForgetPasswordError.class);
                    Toast.makeText(context, message.getError(), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                forgetPassword.setValue(true);


            }

            @Override
            public void onFailure(Call<ForgetPassword> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                forgetPassword.setValue(false);
            }
        });

    }
    private boolean checkContext(){
        if(context == null)
            return false;
        return true;
    }
    public void resetPassword(Call<ResetPassword> loginCall) {

        loginCall.enqueue(new Callback<ResetPassword>() {
            @Override
            public void onResponse(Call<ResetPassword> call, Response<ResetPassword> response) {
                if (response.code() != 200) {
                    resetPassword.setValue(false);
                    Log.d(TAG, "onResponse: "+response.code());
                    if (response.code() == 401)
                        Toast.makeText(context, "Failed, Invalid Token.", Toast.LENGTH_SHORT).show();
                    else {
                        Gson gson = new Gson();

                        ResetError message = gson.fromJson(response.errorBody().charStream(), ResetError.class);
                        String s = "";
                        boolean flag = false;
                        if (message.getError().getToken() != null) {
                            s += message.getError().getToken().get(0);
                            flag = true;
                        }
                        if (message.getError().getEmail() != null) {
                            if (flag)
                                s += "\n";
                            s += message.getError().getEmail().get(0);
                            flag = true;
                        }
                        if (message.getError().getPassword() != null) {
                            if (flag)
                                s += "\n";
                            s += message.getError().getPassword().get(0);
                        }
                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

                    }
                    return;

                }
                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                resetPassword.setValue(true);


            }

            @Override
            public void onFailure(Call<ResetPassword> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                resetPassword.setValue(false);
            }
        });

    }
}
