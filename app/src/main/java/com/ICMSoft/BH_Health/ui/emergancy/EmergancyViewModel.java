package com.ICMSoft.BH_Health.ui.emergancy;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Emergancy.Error.Error;
import com.ICMSoft.BH_Health.data.model.Emergancy.Error.ErrorVoice;
import com.ICMSoft.BH_Health.data.model.Register.ErrorHandle.ErrorRegister;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.Emergancy.Data;
import com.ICMSoft.BH_Health.data.model.Emergancy.EmergancyResponse;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmergancyViewModel extends ViewModel {

    private static final String TAG = "EmergancyViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<Data> mEmergancyLiveData;
    private Context context;

    public EmergancyViewModel() {
        mSuccessLiveData = new MutableLiveData<>();
        mEmergancyLiveData = new MutableLiveData<>();
        context = null;
    }

    public LiveData<Boolean> getGeneralResponse() {
        return mSuccessLiveData;
    }

    public LiveData<Data> getEmergancyResponse() {
        return mEmergancyLiveData;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void getEmergancy(Call<EmergancyResponse> EmergancyResponseCall) {

        EmergancyResponseCall.enqueue(new Callback<EmergancyResponse>() {
            @Override
            public void onResponse(Call<EmergancyResponse> call, Response<EmergancyResponse> response) {
                if (response.code() != 200) {
                   // Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));


                    return;
                }
                mEmergancyLiveData.setValue(response.body().getData());

            }

            @Override
            public void onFailure(Call<EmergancyResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

    public void createEmergancy(Call<GeneralResponse> responseCall) {
        responseCall.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                   // Log.d(TAG, "onResponse: "+response.raw());
                  //  Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Gson gson = new Gson();
                    Log.d(TAG, "onResponse: "+response.code());
                    ErrorVoice message=gson.fromJson(response.errorBody().charStream(), ErrorVoice.class);
                    Log.d(TAG, "onResponse: "+message.getStatusCode());
                    Log.d(TAG, "onResponse: "+message.getError().getMessage());
                    Log.d(TAG, "onResponse: "+message.getError().getType());
                    if(message.getError().getType()!= null && message.getError().getMessage()!=null)
                        Toast.makeText(context,message.getError().getType().get(0)+"\n"+message.getError().getMessage().get(0),Toast.LENGTH_SHORT).show();
                    else  if(message.getError().getType()!= null )
                        Toast.makeText(context,message.getError().getType().get(0),Toast.LENGTH_SHORT).show();
                    else if(message.getError().getMessage()!=null)
                        Toast.makeText(context,message.getError().getMessage().get(0),Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });
    }

}
