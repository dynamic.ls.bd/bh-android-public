package com.ICMSoft.BH_Health.ui.safe_cirle;


import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.Services.ApiService;
import com.ICMSoft.BH_Health.adapter.CircleAdapter;
import com.ICMSoft.BH_Health.data.model.Circle.information.CircleUserData;
import com.ICMSoft.BH_Health.data.model.Circle.memberInformation.MemberInformation;
import com.ICMSoft.BH_Health.data.model.Circle.memberInformation.UserProfileInformation;
import com.ICMSoft.BH_Health.data.model.Circle.user.DatumUser;
import com.ICMSoft.BH_Health.data.model.Circle.user.UserResponse;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_PHONE;


/**
 * A simple {@link Fragment} subclass.
 */
public class SafeCircleFragment extends Fragment {
    private static final String TAG = "SafeCircleFragment";
    private SafeCircleViewModel safeCircleViewModel;
    private GeneralHelper mGeneralHelper;
    private CircleAdapter circleAdapter;
    private List<CircleUserData> mCircleList;


    @BindView(R.id.rvFamilyMember)
    RecyclerView mFamilyMemberRV;


    @BindView(R.id.btn_family)
    TextView btnFamily;
    @BindView(R.id.btn_colleague)
    TextView btnColleague;
    @BindView(R.id.btn_friend)
    TextView btnFriend;
    @BindView(R.id.btn_known)
    TextView btnKnown;

    @BindView(R.id.family_layout)
    RelativeLayout familyView;

    @BindView(R.id.colleagues_layout)
    RelativeLayout colleaguesView;

    @BindView(R.id.friend_layout)
    RelativeLayout friendView;

    @BindView(R.id.known_layout)
    RelativeLayout knownView;


    private String catagory = null;

    private Map<String, String> relationList;

    private ArrayList<String> relations;
    private String userNumber;

    @BindView(R.id.safe_circle_view)
    NestedScrollView safeCircleView;

    private Loading loading;

    public static Map<String, DatumUser> userList;
    private static ArrayList<String> USERS;
    public static ArrayAdapter<String> usersAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_safe_circle, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view, safeCircleView);

        safeCircleViewModel = new ViewModelProvider(this).get(SafeCircleViewModel.class);
        mGeneralHelper = new GeneralHelper(requireContext());
        safeCircleViewModel.setContext(getContext());
        familyView.setVisibility(View.VISIBLE);
        friendView.setVisibility(View.GONE);
        knownView.setVisibility(View.GONE);
        colleaguesView.setVisibility(View.GONE);
        USERS = new ArrayList<>();
        usersAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, USERS);
        userList = new HashMap<>();
        //--------------------------For Active Button----------------
        btnFamily.setTextColor(getResources().getColor(R.color.colorWhite));
        btnFamily.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        relationList = new HashMap<>();
        relationList.put("family", "Family");
        relationList.put("friend", "Friend/Relative");
        relationList.put("known", "Known");
        relationList.put("colleague", "Colleague");
        relations = new ArrayList<>();
        relations.addAll(relationList.values());
        userNumber = mGeneralHelper.getSharedPreferences().getString(USER_PHONE,"");
        catagory = "family";
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        mFamilyMemberRV.setLayoutManager(mLinearLayoutManager);
        getUserList();
        getCircleData();

        mFamilyMemberRV.addItemDecoration(new DividerItemDecoration(mFamilyMemberRV.getContext(), DividerItemDecoration.VERTICAL));

        return view;
    }


    private void getCircleData() {
        loading.startLoading();

        ApiService apiService = mGeneralHelper.getApiService();
        safeCircleViewModel.myCircles(apiService.myCircles(mGeneralHelper.getToken(), catagory));
        if (mCircleList != null)
            mCircleList.clear();
        safeCircleViewModel.getMyCirleListResponse().observe(getViewLifecycleOwner(), data -> {

            if (data == null) {
                loading.stopLoading();
                return;
            }


            loading.stopLoading();
            selectedCircleId = data.getId().toString();
            mCircleList = data.getCircleUsers();
            for(int i = 0; i < mCircleList.size();i++)
            {
                CircleUserData c = mCircleList.get(i);
                if(c.getPhone().equalsIgnoreCase(userNumber))
                    {
                        mCircleList.remove(i);
                        break;
                    }

            }
            circleAdapter = new CircleAdapter(mCircleList,getContext());
            mFamilyMemberRV.setAdapter(circleAdapter);
            setMemberUpdateListener();
            mFamilyMemberRV.setVisibility(View.VISIBLE);
        });
    }


    private String selectedCircleId = null;


    @OnClick(R.id.fav_button)
    public void addMember() {

        // create an alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        final View customLayout = getLayoutInflater().inflate(R.layout.layout_add_member_dialog, null);
        Spinner circleSpinner = customLayout.findViewById(R.id.circleSpinner);
        EditText nameET = customLayout.findViewById(R.id.et_name);
        EditText ageET = customLayout.findViewById(R.id.et_age);
        EditText areaET = customLayout.findViewById(R.id.et_area);
        EditText occupationET = customLayout.findViewById(R.id.et_occupation);
        AutoCompleteTextView contactET = customLayout.findViewById(R.id.et_contact);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_list_item_1, USERS);
        contactET.setAdapter(adapter);

        contactET.setOnItemClickListener((parent, view, position, id) -> nameET.setText(Objects.requireNonNull(userList.get(contactET.getText().toString())).getName()));


        TextView cancelTV = customLayout.findViewById(R.id.tv_cancel);
        TextView sendTV = customLayout.findViewById(R.id.tv_send);

        setCategorySpinner(circleSpinner);


        builder.setView(customLayout);
        AlertDialog dialog = builder.create();
        dialog.show();


        sendTV.setOnClickListener(view -> {


            String name = nameET.getText().toString();
            String age = ageET.getText().toString();
            String address = areaET.getText().toString();
            String occupation = occupationET.getText().toString();
            String phone = contactET.getText().toString();

            if (TextUtils.isEmpty(name)) {
                nameET.setError("Cannot be empty");
                return;
            }

            if (TextUtils.isEmpty(age)) {
                ageET.setError("Cannot be empty");
                return;
            }

            if (TextUtils.isEmpty(address)) {
                areaET.setError("Cannot be empty");
                return;
            }


            Map<String, String> params = new HashMap<>();

            params.put("name", name);
            params.put("relation", circleSpinner.getSelectedItem().toString());
            params.put("phone", phone);
            params.put("age", age);
            params.put("address", address);
            params.put("occupation", occupation);
            for (String s : relationList.keySet()) {
                if (Objects.requireNonNull(relationList.get(s)).compareToIgnoreCase(circleSpinner.getSelectedItem().toString()) == 0) {
                    params.put("circle", s);
                    break;
                }
            }
            Log.d(TAG, "onClick: " + params);


            Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().createMember(mGeneralHelper.getToken(), params);
            safeCircleViewModel.generalResponse(generalResponseCall);

            safeCircleViewModel.getGeneralResponse().observe(getViewLifecycleOwner(), aBoolean -> {

                if (aBoolean) {
                    getCircleData();
                    dialog.dismiss();
                    Toast.makeText(getContext(), "Circle Member added successfully", Toast.LENGTH_SHORT).show();
                }
            });

        });


        cancelTV.setOnClickListener(view -> dialog.dismiss());


    }

    private void getUserList() {
        final Call<UserResponse> responseCall = mGeneralHelper.getApiService().getUserList(mGeneralHelper.getToken(), "");
        safeCircleViewModel.getUserList(responseCall);
        safeCircleViewModel.getUserResponse().observe(getViewLifecycleOwner(), data -> {
            userList.clear();
            USERS.clear();
            if (data != null) {
                for (DatumUser d : data) {
                    userList.put(d.getPhone(), d);
                    USERS.add(d.getPhone());
                }

            }
        });
    }

    private void setCategorySpinner(Spinner spinner) {

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, relations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        String ss = "";
        for (String s : relationList.keySet())
            if (s.compareToIgnoreCase(catagory) == 0) {
                ss += relationList.get(s);
                break;
            }

        spinner.setAdapter(adapter);
        for (int i = 0; i < relations.size(); i++)
            if (relations.get(i).compareToIgnoreCase(ss) == 0) {
                spinner.setSelection(i);
                break;
            }

    }


    private void setMemberUpdateListener() {

        circleAdapter.setCircleMemberUpdateClickListener((view, user) -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
            final View customLayout = getLayoutInflater().inflate(R.layout.layout_add_member_dialog, null);

            Spinner circleSpinner = customLayout.findViewById(R.id.circleSpinner);
            setCategorySpinner(circleSpinner);
            TextView titleTV = customLayout.findViewById(R.id.titleTV);
            titleTV.setText(getResources().getString(R.string.edit));

            EditText nameET = customLayout.findViewById(R.id.et_name);
            EditText ageET = customLayout.findViewById(R.id.et_age);
            EditText areaET = customLayout.findViewById(R.id.et_area);
            EditText occupationET = customLayout.findViewById(R.id.et_occupation);
            EditText contactET = customLayout.findViewById(R.id.et_contact);
            TextView cancelTV = customLayout.findViewById(R.id.tv_cancel);
            TextView sendTV = customLayout.findViewById(R.id.tv_send);

            nameET.setText(user.getName());
            contactET.setText(user.getPhone());
            builder.setView(customLayout);
            AlertDialog dialog = builder.create();
            dialog.show();
            Map<String, String> params = new HashMap<>();

            params.put("circle_id", selectedCircleId);
            params.put("user_id", String.valueOf(user.getId()));

            Call<MemberInformation> generalResponseCall = mGeneralHelper.getApiService().memberInformation(mGeneralHelper.getToken(), params);
            safeCircleViewModel.memberInformaion(generalResponseCall);

            safeCircleViewModel.getmemberResponse().observe(getViewLifecycleOwner(), profileData -> {

                if (profileData != null) {
                    for (UserProfileInformation profile : profileData) {
                        ageET.setText(profile.getProfile().getAge());
                        occupationET.setText(profile.getProfile().getOccupation());
                        areaET.setText(profile.getProfile().getAddress());
                    }
                }
            });


            sendTV.setOnClickListener(view1 -> {
                String name = nameET.getText().toString();
                String age = ageET.getText().toString();
                String address = areaET.getText().toString();
                String occupation = occupationET.getText().toString();
                String phone = contactET.getText().toString();

                if (TextUtils.isEmpty(name)) {
                    nameET.setError("Cannot be empty");
                    return;
                }
                Map<String, String> params1 = new HashMap<>();

                params1.put("circle_id", selectedCircleId);
                params1.put("user_id", String.valueOf(user.getId()));
                params1.put("name", name);
                params1.put("phone", phone);

                params1.put("age", age);
                params1.put("address", address);
                params1.put("occupation", occupation);
                /*
                for (String s : relationList.keySet()) {
                    if (Objects.requireNonNull(relationList.get(s)).compareToIgnoreCase(circleSpinner.getSelectedItem().toString()) == 0) {
                        params.put("circle", s);
                        break;
                    }
                }

                 */
                loading.startLoading();
                Call<GeneralResponse> generalResponseCall1 = mGeneralHelper.getApiService().updateCircleMember(mGeneralHelper.getToken(), params1);
                safeCircleViewModel.generalResponse(generalResponseCall1);

                safeCircleViewModel.getGeneralResponse().observe(getViewLifecycleOwner(), aBoolean -> {

                    if (aBoolean) {
                        getCircleData();
                        dialog.dismiss();

                    } else {
                        loading.stopLoading();
                    }
                });

            });


            cancelTV.setOnClickListener(view12 -> dialog.dismiss());
        });
        circleAdapter.setCircleMemberDeleteClickListener((view, circleUser) -> {
            // Log.d(TAG, "onClick: " + user.getId());
            Map<String, Object> params = new HashMap<>();


            params.put("circle", catagory);
            params.put("user_id", circleUser.getId());

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    requireContext());

            builder.setMessage("Are you sure to remove this member?");
            builder.setNegativeButton("NO",
                    (dialog, which) -> {

                    });
            builder.setPositiveButton("YES",
                    (dialog, which) -> {
                loading.startLoading();
                        Call<GeneralResponse> generalResponseCall = mGeneralHelper.getApiService().deleteMember(mGeneralHelper.getToken(), params);
                        safeCircleViewModel.deleteResponse(generalResponseCall);

                        safeCircleViewModel.getDeleteResponse().observe(getViewLifecycleOwner(), aBoolean -> {
                            if (aBoolean)
                                getCircleData();
                            else
                                loading.stopLoading();
                        });
                    });
            builder.show();


        });


    }

    @OnClick(R.id.familyView)
    public void familyShow() {
        familyView.setVisibility(View.VISIBLE);
        friendView.setVisibility(View.GONE);
        knownView.setVisibility(View.GONE);
        colleaguesView.setVisibility(View.GONE);
        btnFamily.setTextColor(getResources().getColor(R.color.colorWhite));
        btnFamily.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btnColleague.setTextColor(getResources().getColor(R.color.colorBlack));
        btnColleague.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnFriend.setTextColor(getResources().getColor(R.color.colorBlack));
        btnFriend.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnKnown.setTextColor(getResources().getColor(R.color.colorBlack));
        btnKnown.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        catagory = "family";
        getCircleData();

    }

    @OnClick(R.id.colleaguesView)
    public void colleaguesShow() {
        familyView.setVisibility(View.GONE);
        friendView.setVisibility(View.GONE);
        knownView.setVisibility(View.GONE);
        colleaguesView.setVisibility(View.VISIBLE);
        btnFamily.setTextColor(getResources().getColor(R.color.colorBlack));
        btnFamily.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnColleague.setTextColor(getResources().getColor(R.color.colorWhite));
        btnColleague.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btnFriend.setTextColor(getResources().getColor(R.color.colorBlack));
        btnFriend.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnKnown.setTextColor(getResources().getColor(R.color.colorBlack));
        btnKnown.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        catagory = "colleague";
        getCircleData();
    }

    @OnClick(R.id.friendView)
    public void friendShow() {
        familyView.setVisibility(View.GONE);
        friendView.setVisibility(View.VISIBLE);
        knownView.setVisibility(View.GONE);
        colleaguesView.setVisibility(View.GONE);
        btnFamily.setTextColor(getResources().getColor(R.color.colorBlack));
        btnFamily.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnColleague.setTextColor(getResources().getColor(R.color.colorBlack));
        btnColleague.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnFriend.setTextColor(getResources().getColor(R.color.colorWhite));
        btnFriend.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btnKnown.setTextColor(getResources().getColor(R.color.colorBlack));
        btnKnown.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        catagory = "friend";
        getCircleData();
    }

    @OnClick(R.id.knownView)
    public void knownShow() {
        familyView.setVisibility(View.GONE);
        friendView.setVisibility(View.GONE);
        knownView.setVisibility(View.VISIBLE);
        colleaguesView.setVisibility(View.GONE);
        btnFamily.setTextColor(getResources().getColor(R.color.colorBlack));
        btnFamily.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnColleague.setTextColor(getResources().getColor(R.color.colorBlack));
        btnColleague.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnFriend.setTextColor(getResources().getColor(R.color.colorBlack));
        btnFriend.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        btnKnown.setTextColor(getResources().getColor(R.color.colorWhite));
        btnKnown.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        catagory = "known";
        getCircleData();
    }

    @OnClick(R.id.today_meet)
    public void meetingToday() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.meeting_today);
    }

    @OnClick(R.id.today_visit)
    public void visitingToday() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.visiting_today);
    }


}