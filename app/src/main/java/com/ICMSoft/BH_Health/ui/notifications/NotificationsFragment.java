package com.ICMSoft.BH_Health.ui.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.NotificationAdapter;
import com.ICMSoft.BH_Health.data.model.notification.Datum;
import com.ICMSoft.BH_Health.data.model.notification.GetNotification;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private GeneralHelper mGeneralHelper;
    private Loading loading;
    private NotificationAdapter notificationAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    @BindView(R.id.notification_scroller)
    NestedScrollView notificationScroller;

    @BindView(R.id.notification_list)
    RecyclerView notificationList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel = new ViewModelProvider(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        //final TextView textView = root.findViewById(R.id.text_notifications);

        ButterKnife.bind(this,root);
        mGeneralHelper = new GeneralHelper(getContext());
        loading = new Loading(root,notificationScroller);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        notificationList.setLayoutManager(mLinearLayoutManager);
        getNotificaions();
        return root;
    }


    private void getNotificaions() {
        loading.startLoading();
        //Log.d(TAG, "getNews: ");
        final Call<GetNotification> responseCall = mGeneralHelper.getApiService().getNotifications(mGeneralHelper.getToken());
        notificationsViewModel.getNotification(responseCall);


        ///  newsViewModel.getNewsListResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {

        notificationsViewModel.getNotificationResponse().observe(getViewLifecycleOwner(), new Observer<List<Datum>>() {
            @Override
            public void onChanged(List<Datum> data) {

                if (data!=null){
                    notificationAdapter = new NotificationAdapter(data);
                    notificationList.setAdapter(notificationAdapter);

                }
                loading.stopLoading();

            }
        });


    }
}