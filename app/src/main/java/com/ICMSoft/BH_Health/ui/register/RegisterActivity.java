package com.ICMSoft.BH_Health.ui.register;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.ICMSoft.BH_Health.MainActivity;
import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryData;
import com.ICMSoft.BH_Health.data.model.Register.Country.CountryListResponse;
import com.ICMSoft.BH_Health.data.model.Register.RegisterResponse;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.Datum;
import com.ICMSoft.BH_Health.data.model.Register.Upazila.UpazilaListResponse;
import com.ICMSoft.BH_Health.ui.Utils.DatePickerCalander;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.login.LoginActivity;
import com.airbnb.lottie.LottieAnimationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.login.LoginActivity.AccessToken;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.ROLE;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_EMAIL;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_NAME;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.USER_PHONE;
import static com.ICMSoft.BH_Health.ui.login.LoginActivity.UserPref;

public class RegisterActivity extends AppCompatActivity {
    public static final String USER_FIREBASE_TOKEN = "fi4cQlK9yjV6uvylDLqaM4:APA91bE0T-TZ0Y90X0AklZJ6i95BxMozQ6kS4JI48PHHi21_aPQxZIMVGCN0EaF-d9UtFacQobYQ0j-zruKcdlNYAXiZpxvkYNkZV36751f8V01k6BF3fx9snyC3SfTjSDlev42pSDrV";


    private RegisterViewModel registerViewModel;
    private UpazilaViewModel upazilaViewModel;
    private CountryViewModel countryViewModel;


    @BindView(R.id.et_name)
    EditText nameET;

    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.et_email)
    EditText emailET;

    @BindView(R.id.et_password)
    EditText passwordET;

    @BindView(R.id.et_cofirmPassword)
    EditText confirmPasswordET;

    @BindView(R.id.et_phone)
    EditText phoneET;

    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.age)
    EditText age;
    @BindView(R.id.occupation)
    EditText occupation;
    @BindView(R.id.return_from_aboard)
    Spinner return_from_aboard;

    @BindView(R.id.return_date)
    TextView return_date;

    @BindView(R.id.country_name)
    AutoCompleteTextView country_name;
    @BindView(R.id.up_name)
    AutoCompleteTextView upazila_name;


    @BindView(R.id.btn_register)
    Button register;

    @BindView(R.id.chk_term)
    CheckBox checkBoxTearm;

    @BindView(R.id.firstPart)
    LinearLayout firstPart;
    @BindView(R.id.secondPart)
    LinearLayout secondPart;

    DatePickerDialog picker;
    //EditText return_date;
    ArrayAdapter<String> upazila_adapter;
    ArrayAdapter<String> country_adapter;
    private GeneralHelper mGeneralHelper;


    Map<String, String> upazilaValue;
    Map<String, String> countryValue;
    Map<String, Integer> upazilaMap;
    Map<String, Integer> countryMap;
    private SharedPreferences sharedpreferences;
    @BindView(R.id.register_loading_view)
    LinearLayout registerLoading;
    @BindView(R.id.register_view)
    ScrollView registerView;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    private boolean flag;


    @BindView(R.id.no_network_news_view)
    RelativeLayout noNetworkHealthView;
    @BindView(R.id.no_network_view)
    LottieAnimationView noNetwork;


    private String role="";

    Timer timer;
    TimerTask task;
    Handler handler = new Handler();

    private DatePickerCalander datePickerCalander;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        flag = false;
        datePickerCalander = new DatePickerCalander(return_date, this);
        ArrayList<String> UPAZILA = new ArrayList<>();
        ArrayList<String> COUNTRIES = new ArrayList<>();
        upazila_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, UPAZILA);
        upazila_name.setAdapter(upazila_adapter);
        country_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, COUNTRIES);
        country_name.setAdapter(country_adapter);
        return_date.setInputType(InputType.TYPE_NULL);
        registerViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        registerViewModel.setContext(getApplicationContext());
        upazilaViewModel = new ViewModelProvider(this).get(UpazilaViewModel.class);
        countryViewModel = new ViewModelProvider(this).get(CountryViewModel.class);
        mGeneralHelper = new GeneralHelper(getApplicationContext());
        return_date.setVisibility(View.GONE);
        country_name.setVisibility(View.GONE);
        upazilaValue = new HashMap<>();
        countryValue = new HashMap<>();
        upazilaMap = new HashMap<>();
        countryMap = new HashMap<>();

        firstPart.setVisibility(View.VISIBLE);
        secondPart.setVisibility(View.GONE);

        sharedpreferences = getSharedPreferences(UserPref, Context.MODE_PRIVATE);
        register.setEnabled(false);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if (false) {

            noNetwork.cancelAnimation();
            noNetworkHealthView.setVisibility(View.GONE);
            stopLoading();
            return true;
        }
        backView();
        return true;
    }

    private void backView(){
        if(firstPart.getVisibility() == View.VISIBLE) {
            Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(myIntent);
            finish();
        }
        else {
            resetView();
            firstPart.setVisibility(View.VISIBLE);
        }
    }

    private void startLoading() {
        registerView.setVisibility(View.GONE);
        registerLoading.setVisibility(View.VISIBLE);
        animationView.playAnimation();
        flag = true;
        startTimer();
    }

    private void stopLoading() {
        registerView.setVisibility(View.VISIBLE);
        registerLoading.setVisibility(View.GONE);
        animationView.cancelAnimation();
        flag = false;
        stopTimer();
    }

    private void setTask() {
        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(() -> {
                    registerView.setVisibility(View.GONE);
                    registerLoading.setVisibility(View.GONE);
                    animationView.cancelAnimation();
                    noNetworkHealthView.setVisibility(View.VISIBLE);
                    noNetwork.playAnimation();
                });
            }
        };
    }

    private void startTimer() {
        timer = new Timer();
        setTask();
        timer.schedule(task, 20000);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
            noNetwork.cancelAnimation();
        }
    }

    private void resetView(){
        firstPart.setVisibility(View.GONE);
        secondPart.setVisibility(View.GONE);
    }


@OnClick(R.id.user_role)
void userRole(){
    resetView();
    secondPart.setVisibility(View.VISIBLE);
    role = "user";
}


    @OnClick(R.id.doctor_role)
    void doctorRole(){
        resetView();
        secondPart.setVisibility(View.VISIBLE);
        role = "doctor";
    }
    @OnClick(R.id.nurse_role)
    void nurserRole(){
        resetView();
        secondPart.setVisibility(View.VISIBLE);
        role = "nurse";
    }
    @OnTextChanged(R.id.up_name)
    public void getUpazila() {
        upazilaMap.clear();
        upazilaValue.clear();
        upazilaValue.put("term", upazila_name.getText().toString());
        getUpazilaList();
    }

    @OnTextChanged(R.id.country_name)
    public void getCountry() {
        countryMap.clear();
        countryValue.clear();
        countryValue.put("term", country_name.getText().toString());
        getCountryList();
    }

    private void getUpazilaList() {
        final Call<UpazilaListResponse> responseCall = mGeneralHelper.getApiService().getUpazilaList(upazilaValue);
        upazilaViewModel.getUpazila(responseCall);
        upazilaViewModel.getUpazilaResponse().observe(this, data -> {
            upazila_adapter.clear();
            if (data != null) {
                for (Datum d : data) {
                    upazilaMap.put(d.getName(), d.getId());
                    upazila_adapter.add(d.getName());
                }
            }
        });
    }

    private void getCountryList() {
        final Call<CountryListResponse> responseCall = mGeneralHelper.getApiService().getCountryList(countryValue);
        countryViewModel.getCountry(responseCall);
        countryViewModel.getCountryResponse().observe(this, data -> {
            country_adapter.clear();
            if (data != null) {
                for (CountryData d : data) {
                    countryMap.put(d.getName(), d.getId());
                    country_adapter.add(d.getName());
                }
            }
        });
    }

    @OnItemSelected(R.id.return_from_aboard)
    public void returnFromAbroard(int position) {
        switch (position) {
            case 0:
                return_date.setVisibility(View.GONE);
                country_name.setVisibility(View.GONE);
                break;
            case 1:
                return_date.setVisibility(View.VISIBLE);
                country_name.setVisibility(View.VISIBLE);
                break;
        }
    }

    @OnClick(R.id.chk_term)
    public void checkTearm() {
        if (checkBoxTearm.isChecked())
            register.setEnabled(true);
        else
            register.setEnabled(false);
    }


    @OnClick(R.id.btn_register)
    public void submit() {


        String name = nameET.getText().toString();
        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();
        String confirmPassword = confirmPasswordET.getText().toString();
        String phone = phoneET.getText().toString();
        String address = this.address.getText().toString();
        String age = this.age.getText().toString();
        String occupation = this.occupation.getText().toString();
        String return_from_aboard = this.return_from_aboard.getSelectedItem().toString();
        String upazila_name = this.upazila_name.getText().toString();
        String country_name = this.country_name.getText().toString();
        String return_date = this.return_date.getText().toString();

        if (TextUtils.isEmpty(name)) {
            nameET.setError("Cannot be empty");
            return;
        }
        if (TextUtils.isEmpty(upazila_name)) {
            this.upazila_name.setError("Cannot be empty");
            return;
        }
        if (upazilaMap.get(upazila_name) == null) {
            this.upazila_name.setError("Upazila cannot found. Please select upazila from dropdown");
            return;
        }
        boolean aboard = return_from_aboard.compareToIgnoreCase("yes") == 0 || return_from_aboard.compareToIgnoreCase("হ্যাঁ") == 0;
        if (TextUtils.isEmpty(country_name) && aboard) {
            this.country_name.setError("Cannot be empty");
            return;
        }

        if (countryMap.get(country_name) == null && aboard) {
            this.country_name.setError("Country cannot found. Please select country from dropdown");
            return;
        }
        if (TextUtils.isEmpty(return_date) && aboard) {
            this.return_date.setError("Cannot be empty");
            return;
        }
        if (TextUtils.isEmpty(address)) {
            this.address.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(age)) {
            this.age.setError("Cannot be empty");
            return;
        }
        if (TextUtils.isEmpty(name)) {
            nameET.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(occupation)) {
            this.occupation.setError("Cannot be empty");
            return;
        }


        if (TextUtils.isEmpty(password)) {
            passwordET.setError("Cannot be empty");
            return;
        }

        if (password.length() < 8) {
            passwordET.setError("Password must be at least 8 character");
            return;
        }

        if (TextUtils.isEmpty(phone)) {
            phoneET.setError("Cannot be empty");
            return;
        }

        if (phone.length() != 11) {
            phoneET.setError("The phone must be 11 digits");
            return;
        }


        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailET.setError("Please Enter valid email");
            emailET.requestFocus();
            return;
        }


        if (TextUtils.isEmpty(confirmPassword)) {
            confirmPasswordET.setError("Cannot be empty");
            return;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(getApplicationContext(), "Password and Confirm password does not match", Toast.LENGTH_LONG).show();
            passwordET.setError("Password does not match");
            confirmPasswordET.setError("Password does not match");
            return;
        }


        final Map<String, Object> params = new HashMap<>();
        params.put("firebase_token", USER_FIREBASE_TOKEN);
        params.put("name", name);
        params.put("email", email);
        params.put("phone", phone);
        params.put("password", password);
        params.put("password_confirmation", confirmPassword);
        params.put("address", address);
        params.put("age", age);
        params.put("occupation", occupation);
        params.put("role",role);

        params.put("upazila_id", upazilaMap.get(upazila_name));
        if (aboard) {
            params.put("returned_from_abroad", "1");
            if (TextUtils.isEmpty(return_date)) {
                this.return_date.setError("Cannot be empty");
                return;
            }
            if (TextUtils.isEmpty(country_name)) {
                this.country_name.setError("Cannot be empty");
                return;
            }

            params.put("rb_country_id", countryMap.get(country_name));
            try {
                Date d = new SimpleDateFormat("dd/MM/yyyy").parse(return_date);
                params.put("rba_date", new SimpleDateFormat("yyyy-MM-dd").format(d));


            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else
            params.put("returned_from_abroad", "0");
        startLoading();
        Call<RegisterResponse> call = mGeneralHelper.getApiService().register(params);

        registerViewModel.registerUser(call);


        registerViewModel.getRegister().observe(this, registerResponse -> {
            if (registerResponse != null) {
                stopLoading();
                Toast.makeText(getApplicationContext(), "Registration Successfully", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(AccessToken, registerResponse.getToken());
                editor.putString(USER_NAME, registerResponse.getData().getName());
                editor.putString(USER_EMAIL, registerResponse.getData().getEmail());
                editor.putString(USER_PHONE, registerResponse.getData().getPhone());
                editor.putString(ROLE, registerResponse.getData().getRole());
                editor.apply();
                Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                stopLoading();
            }

        });


    }

    @OnClick(R.id.terms)
    public void termShow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View customLayout = getLayoutInflater().inflate(R.layout.termandconditiondialog, null);

        TextView cancelTV = customLayout.findViewById(R.id.cancel);

        builder.setView(customLayout);
        AlertDialog dialog = builder.create();
        dialog.show();


        cancelTV.setOnClickListener(view -> dialog.dismiss());
    }


    @Override
    public void onBackPressed() {
        backView();
    }
}
