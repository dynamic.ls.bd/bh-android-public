package com.ICMSoft.BH_Health.ui.beSafe;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ICMSoft.BH_Health.R;

import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.github.barteksc.pdfviewer.PDFView;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;


import static com.ICMSoft.BH_Health.ui.beSafe.Protocol.protocolDocument;

public class ProtocolDetails extends Fragment {

    public static final String TAG="ProtocolDetails";
    @BindView(R.id.pdfView)
    PDFView pdfView;

    private Loading loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_protocol_details, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view,pdfView);
        getData();
        return view;
    }

    private void getData() {
        loading.startLoading();
        FileLoader.with(getContext())
                .load(protocolDocument,false) //2nd parameter is optioal, pass true to force load from network
                .fromDirectory("protocolFiles", FileLoader.DIR_INTERNAL)
                .asFile(new FileRequestListener<File>() {
                    @Override
                    public void onLoad(FileLoadRequest request, FileResponse<File> response) {
                        File loadedFile = response.getBody();

                        pdfView.fromFile(loadedFile).load();
                        loading.stopLoading();
                    }

                    @Override
                    public void onError(FileLoadRequest request, Throwable t) {
                    }
                });


    }
}
