package com.ICMSoft.BH_Health.ui.Utils;

import android.graphics.Color;
import android.view.View;
import android.widget.RadioButton;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

public class RadioMenuSelection {

    private Map<String,RadioButton> buttonMap;

    public RadioMenuSelection(Map<String, RadioButton> buttonMap) {
        this.buttonMap = buttonMap;
    }

    public RadioMenuSelection(Map<String, Integer> buttonMap, View view) {
        for(String s: buttonMap.keySet()){
            this.buttonMap.put(s, view.findViewById(buttonMap.get(s)));
        }
    }



    private void unselected(RadioButton button) {
        button.setSelected(false);
        button.setTextColor(Color.BLACK);
    }

    private void selected(RadioButton button) {
        button.setChecked(true);
        button.setTextColor(Color.WHITE);
    }

    private void resetMenu() {
        for(String s:buttonMap.keySet())
            unselected(buttonMap.get(s));
    }

    public void seletedItem(String seletedString, RecyclerView recyclerView, RecyclerView.Adapter<?> adapter) {
        resetMenu();
        selected(buttonMap.get(seletedString));
        recyclerView.setAdapter(adapter);
    }

}
