package com.ICMSoft.BH_Health.ui.dashboard;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Emergancy.EmergancyResponse;
import com.ICMSoft.BH_Health.data.model.Statistics.GetStatistics;
import com.ICMSoft.BH_Health.data.model.Statistics.GetStatisticsData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<GetStatisticsData> statisticalData;

    public DashboardViewModel() {
        mText = new MutableLiveData<>();
        statisticalData = new MutableLiveData<>();
        mText.setValue("This is dashboard fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<GetStatisticsData> getStatisticsData() {
        return statisticalData;
    }
    public void getStatistics(Call<GetStatistics> EmergancyResponseCall) {

        EmergancyResponseCall.enqueue(new Callback<GetStatistics>() {
            @Override
            public void onResponse(Call<GetStatistics> call, Response<GetStatistics> response) {
                if (response.code() != 200) {
                    // Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    statisticalData.setValue(null);

                    return;
                }
                statisticalData.setValue(response.body().getData());

            }

            @Override
            public void onFailure(Call<GetStatistics> call, Throwable t) {

                statisticalData.setValue(null);
            }
        });

    }
}