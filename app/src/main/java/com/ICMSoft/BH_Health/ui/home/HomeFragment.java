package com.ICMSoft.BH_Health.ui.home;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ICMSoft.BH_Health.MainActivity;
import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.ImageSliderAdapter;
import com.google.firebase.messaging.FirebaseMessaging;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";
    @BindView(R.id.imageSlider)
    SliderView sliderView;
    @BindView(R.id.be_safe_home_title)
    TextView besafeTitle;
    @BindView(R.id.latestNews)
    TextView latestNews;

    @BindView(R.id.faqTitle)
    TextView faqTitle;



    public static boolean menusBeSafe = true;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, root);
        createNotificationChannel();
        FirebaseMessaging.getInstance().subscribeToTopic("all")
                .addOnCompleteListener(task -> {
                });



        List<Integer> sliderImage = new ArrayList<>();
        sliderImage.add(R.drawable.slider_one);
        sliderImage.add(R.drawable.be_safe_png);
        sliderImage.add(R.drawable.safe_circle);
        sliderImage.add(R.drawable.health_record);
        sliderImage.add(R.drawable.self_quarentine);
        ImageSliderAdapter adapter = new ImageSliderAdapter(getContext(), sliderImage);
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.DROP);
        sliderView.startAutoCycle();
        if (MainActivity.checkRole("user")){
            besafeTitle.setText(R.string.label_besafe);
            latestNews.setText(R.string.latest_news);
            faqTitle.setText(R.string.faq);
        }

        else{
            besafeTitle.setText(R.string.label_protocol);
            latestNews.setText(R.string.label_announcement);
            faqTitle.setText(R.string.label_observations);
        }



        return root;
    }




    @OnClick(R.id.rlSafeCircle)
    public void safeCircle() {

        if (getActivity() != null) {
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

            navController.navigate(R.id.safeCircleFragment);
        }

    }

    @OnClick(R.id.faq)
    public void faq() {

        if (getActivity() != null) {
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            if (MainActivity.checkRole("user"))
            navController.navigate(R.id.questionFragment);
            else
                navController.navigate(R.id.observationPage);

        }

    }

    @OnClick(R.id.news)
    public void news() {
        if (getActivity() != null) {
            Log.d(TAG, "news");
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            if (MainActivity.checkRole("user"))
                navController.navigate(R.id.newsFragment);
            else
                navController.navigate(R.id.announcement);

        } else {

            Log.d(TAG, "news: null ");
        }

    }


    @OnClick(R.id.self_quarantine)
    public void selfQuarantine() {
        if (getActivity() != null) {
            Log.d(TAG, "news");
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.selfQuarantineFragment);
        }
    }


    @OnClick(R.id.health_record)
    public void healthRecord() {
        if (getActivity() != null) {
            Log.d(TAG, "news");
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.healthRecordFragment);
        }
    }

    @OnClick(R.id.rlBeSafe)
    public void beSafe() {
        if (getActivity() != null) {
            menusBeSafe = true;
            Log.d(TAG, "beSafeFragment");
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            if(MainActivity.checkRole("user"))
            navController.navigate(R.id.beSafeFragment);
            else
                navController.navigate(R.id.protocol);
        }
    }

    @OnClick(R.id.emergency_id)
    public void emergency() {
        if (getActivity() != null) {
            Log.d(TAG, "news");
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.emergency_fragment);
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "My Notification";
            String description = "My Notification Description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("BH", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = requireActivity().getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }
    }

}