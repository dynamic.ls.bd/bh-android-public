package com.ICMSoft.BH_Health.ui.observation;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import android.widget.RadioButton;

import android.widget.MultiAutoCompleteTextView;

import android.widget.Spinner;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.ObservationAdapter;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.observetion.ObservationTypeResponse;
import com.ICMSoft.BH_Health.data.model.observetion.observationList.Datum;
import com.ICMSoft.BH_Health.data.model.observetion.observationList.ObservationListResponse;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.ICMSoft.BH_Health.ui.Utils.RadioMenuSelection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;


/**
 * A simple {@link Fragment} subclass.
 */
public class Observations extends Fragment {


    private ObservationViewModel observationViewModel;
    private GeneralHelper mGeneralHelper;
    @BindView(R.id.observatoin_rv)
    RecyclerView observationShowList;

    @BindView(R.id.typeSpinner)
    Spinner typeSpinner;

    @BindView(R.id.toSpinner)
    Spinner toSpinner;

    @BindView(R.id.et_subject)
    MultiAutoCompleteTextView mSubjectET;




    @BindView(R.id.et_message)
    EditText mMessageET;

    @BindView(R.id.btn_observation)
    RadioButton observation;

    @BindView(R.id.btn_comments)
    RadioButton comments;


    @BindView(R.id.btn_learnings)
    RadioButton learning;

    private ArrayList<String> listTo;
    private ArrayList<String> listTag;




    private ArrayAdapter<String> typeAdapter,toAdapter;
    private ArrayAdapter<String> tagNames;
    @BindView(R.id.observation_view)
    NestedScrollView observationView;
    private Loading loading;
    private RadioMenuSelection menuSelection;
    private String selected;
    private Map<String,ArrayList<Datum>> listMap;
    public Observations() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_observations, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view,observationView);
        observationViewModel = new ViewModelProvider(this).get(ObservationViewModel.class);
        observationViewModel.setContext(requireContext());
        mGeneralHelper = new GeneralHelper(requireContext());
        listTo = new ArrayList<>();
        listTag = new ArrayList<>();
        ArrayList<String> listType = new ArrayList<>();
        typeAdapter = new ArrayAdapter<>(requireContext(),android.R.layout.simple_spinner_dropdown_item, listType);
        toAdapter = new ArrayAdapter<>(requireContext(),android.R.layout.simple_spinner_dropdown_item,listTo);
        typeSpinner.setAdapter(typeAdapter);
        toSpinner.setAdapter(toAdapter);
        loading.startLoading();
        menuSelection = new RadioMenuSelection(buttonInit());
        listMap = new HashMap<>();
        selected = "observation";
        getConfig();
        init();

       // String[] tageSuggestion = {"Medicine", "Wash", "Breathing Management", "Ventilator", "Improvement"};
        tagNames = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, listTag);
        mSubjectET.setThreshold(1);
        mSubjectET.setAdapter(tagNames);
        mSubjectET.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());




        return view;
    }
    private Map<String, RadioButton> buttonInit(){
        Map<String, RadioButton> buttonMap = new HashMap<>();
        buttonMap.put("observation",observation);
        buttonMap.put("comments",comments);
        buttonMap.put("learning",learning);
        return buttonMap;
    }

    private void init() {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        observationShowList.setLayoutManager(mLinearLayoutManager);
        getObservationList();

    }
    private void getObservationList() {
        loading.startLoading();

        final Call<ObservationListResponse> responseCall = mGeneralHelper.getApiService().getObservationList(mGeneralHelper.getToken());
        observationViewModel.setObservationList(responseCall);

        observationViewModel.getObservationList().observe(getViewLifecycleOwner(), data -> {
            loading.stopLoading();
            if (data == null) {
                return;
            }


            ArrayList<Datum> observationList = new ArrayList<>();
            ArrayList<Datum> learings = new ArrayList<>();
            ArrayList<Datum> commentList = new ArrayList<>();


            for (Datum d : data) {
                if (d.getType().compareToIgnoreCase("observation") == 0)
                    observationList.add(d);
                else if (d.getType().compareToIgnoreCase("comments") == 0)
                    commentList.add(d);
                else if (d.getType().equalsIgnoreCase("learnings"))
                    learings.add(d);

            }
            listMap.put("observation", observationList);
            listMap.put("comments", commentList);
            listMap.put("learning", learings);
            seletedObservation();

        });

    }


    private void getConfig() {

        Call<ObservationTypeResponse> responseCall = mGeneralHelper.getApiService().getObservationTo();
        observationViewModel.setObservationListTo(responseCall);

        observationViewModel.getObservationListTo().observe(getViewLifecycleOwner(), data -> {

            toAdapter.clear();
            if (data == null) {
                return;
            }
            toAdapter.addAll(data);

        });

        Call<ObservationTypeResponse> responseCall1 = mGeneralHelper.getApiService().getObservationTag();
        observationViewModel.setObservationListTag(responseCall1);

        observationViewModel.getObservationListTag().observe(getViewLifecycleOwner(), data -> {

            tagNames.clear();
            listTag.clear();
            if (data == null) {
                return;
            }
            listTag.addAll(data);
            tagNames.notifyDataSetChanged();
            //tagNames.addAll(data);

        });

        Call<ObservationTypeResponse> responseCall2 = mGeneralHelper.getApiService().getObservationType();
        observationViewModel.setObservationListType(responseCall2);

        observationViewModel.getObservationListType().observe(getViewLifecycleOwner(), data -> {

            typeAdapter.clear();
            if (data == null) {
                return;
            }
            typeAdapter.addAll(data);


        });


    }


    @OnClick(R.id.sendTV)
    void saveQuestion() {

        String to = toSpinner.getSelectedItem().toString();
        String type = typeSpinner.getSelectedItem().toString();
        String subject = mSubjectET.getText().toString();
        String message = mMessageET.getText().toString();



        if (TextUtils.isEmpty(subject)) {
            mSubjectET.setError("Cannot be empty");
            return;
        }

        if (TextUtils.isEmpty(message)) {
            mMessageET.setError("Cannot be empty");
            return;
        }


        Map<String, Object> params = new HashMap<>();

        String tags[] = subject.split(",");
        ArrayList<String> tag = new ArrayList<>();
        for(int j = 0; j< tags.length;j++){
            int x = 0;
            tags[j] = tags[j].trim();

            if(!tags[j].isEmpty()) {

                for (int i = 0; i < listTag.size(); i++)
                    if (listTag.get(i).equalsIgnoreCase(tags[j])) {
                        x++;
                        tag.add(tags[j]);
                        break;
                    }

                if (x == 0) {
                    mSubjectET.setError(tags[j] + " has not tag");
                    return;
                }
            }
        }

        params.put("to", to);
        params.put("type", type);
        params.put("tags", tag);
        params.put("message", message);

        Call<GeneralResponse> responseCall1 = mGeneralHelper.getApiService().createObservation(mGeneralHelper.getToken(), params);
        observationViewModel.createObservation(responseCall1);


        observationViewModel.getObservationCreate().observe(getViewLifecycleOwner(), result-> {
                if(result){
                    mSubjectET.setText("");
                    mMessageET.setText("");
                    toSpinner.setSelection(0);
                    typeSpinner.setSelection(0);
                    getObservationList();
                }



        });

    }

    private void seletedObservation(){
        ObservationAdapter observationAdapter = new ObservationAdapter(listMap.get(selected));
        menuSelection.seletedItem(selected,observationShowList, observationAdapter);

    }

    @OnClick(R.id.btn_observation)
    void observationClick(){
        selected = "observation";
        seletedObservation();
    }

    @OnClick(R.id.btn_comments)
    void commentsClick(){
        selected = "comments";
        seletedObservation();
    }

    @OnClick(R.id.btn_learnings)
    void learningClick(){
        selected = "learning";
        seletedObservation();
    }


}
