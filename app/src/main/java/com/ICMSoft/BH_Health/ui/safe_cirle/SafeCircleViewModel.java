package com.ICMSoft.BH_Health.ui.safe_cirle;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ICMSoft.BH_Health.data.model.Circle.DeleteAdmin;
import com.ICMSoft.BH_Health.data.model.Circle.addCircle.AddSafeCircleError;
import com.ICMSoft.BH_Health.data.model.Circle.editCircle.EditUserCircle;
import com.ICMSoft.BH_Health.data.model.Circle.editCircle.UserProfile;
import com.ICMSoft.BH_Health.data.model.Circle.information.CircleInformation;
import com.ICMSoft.BH_Health.data.model.Circle.information.DataUser;
import com.ICMSoft.BH_Health.data.model.Circle.memberInformation.MemberInformation;
import com.ICMSoft.BH_Health.data.model.Circle.memberInformation.UserProfileInformation;
import com.ICMSoft.BH_Health.data.model.GeneralResponse;
import com.ICMSoft.BH_Health.data.model.Circle.user.DatumUser;
import com.ICMSoft.BH_Health.data.model.Circle.user.UserResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SafeCircleViewModel extends ViewModel {

    private static final String TAG = "SafeCircleViewModel";
    private MutableLiveData<Boolean> mSuccessLiveData;
    private MutableLiveData<Boolean> mSuccessDeleteLiveData;
    private MutableLiveData<DataUser> myCircleList;
    private MutableLiveData<List<UserProfileInformation>> memberInformation;
    private MutableLiveData<List<UserProfile>> circleData;
    private Context context;
    private MutableLiveData<List<DatumUser>> mUserListData;
    public void setContext(Context context) {
        this.context = context;
    }

    public SafeCircleViewModel() {
        mSuccessLiveData = new MutableLiveData<>();
        myCircleList  = new MutableLiveData<>();
        mSuccessDeleteLiveData  = new MutableLiveData<>();
        circleData  = new MutableLiveData<>();
        mUserListData = new MutableLiveData<>();
        memberInformation = new MutableLiveData<>();
    }

    public LiveData<Boolean> getGeneralResponse() {
        return mSuccessLiveData;
    }
    public LiveData<List<UserProfile>> getCircleDataResponse() {
        return circleData;
    }
    public LiveData<Boolean> getDeleteResponse() {
        return mSuccessDeleteLiveData;
    }
    public LiveData<List<DatumUser>> getUserResponse() {
        return mUserListData;
    }
    MutableLiveData<DataUser> getMyCirleListResponse() {
        return myCircleList;
    }
    MutableLiveData<List<UserProfileInformation>> getmemberResponse() {
        return memberInformation;
    }

    public void myCircles(Call<CircleInformation> responseCall) {

        responseCall.enqueue(new Callback<CircleInformation>() {
            @Override
            public void onResponse(Call<CircleInformation> call, Response<CircleInformation> response) {
                if (response.code() != 200) {
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }

                DataUser data = response.body().getData();
                myCircleList.setValue(data);

            }

            @Override
            public void onFailure(Call<CircleInformation> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void memberInformaion(Call<MemberInformation> responseCall) {

        responseCall.enqueue(new Callback<MemberInformation>() {
            @Override
            public void onResponse(Call<MemberInformation> call, Response<MemberInformation> response) {
                if (response.code() != 200) {
                    memberInformation.setValue(null);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }


                memberInformation.setValue(response.body().getData().getUserProfile());

            }

            @Override
            public void onFailure(Call<MemberInformation> call, Throwable t) {
                memberInformation.setValue(null);
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
    }
    public void getUserList(Call<UserResponse> SelfQuarantineResponseCall) {

        SelfQuarantineResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.code() != 200) {
                    Log.d(TAG, "onResponse: "+response.code());
                    mUserListData.setValue(null);

                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                mUserListData.setValue(response.body().getData());
                Log.d(TAG, "onResponse: "+response.code());
                // Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mUserListData.setValue(null);
            }
        });

    }


    public void generalResponse(Call<GeneralResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessLiveData.setValue(false);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Gson gson = new Gson();
                    String s = "";
                    if(response.code()==302) {
                        AddSafeCircleError message = gson.fromJson(response.errorBody().charStream(), AddSafeCircleError.class);

                        boolean flag = false;
                        if (message.getError().getCircle() != null) {
                            s += message.getError().getCircle().get(0);
                            flag = true;
                        }
                        if (message.getError().getName() != null) {
                            if (flag)
                                s += "\n";
                            flag = true;
                            s += message.getError().getName().get(0);
                        }
                        if (message.getError().getPhone() != null) {
                            if (flag)
                                s += "\n";
                            s += message.getError().getPhone().get(0);
                        }
                    }
                    else if(response.code()==400){
                        GeneralResponse message = gson.fromJson(response.errorBody().charStream(), GeneralResponse.class);
                        s = message.getMessage();
                    }
                    Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessLiveData.setValue(false);
            }
        });


    }
    public void deleteResponse(Call<GeneralResponse> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                if (response.code() != 200) {
                    mSuccessDeleteLiveData.setValue(false);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    Gson gson = new Gson();
                    DeleteAdmin message=gson.fromJson(response.errorBody().charStream(), DeleteAdmin.class);

                    Toast.makeText(context,message.getMessage(),Toast.LENGTH_SHORT).show();


                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                mSuccessDeleteLiveData.setValue(true);
                Toast.makeText(context,response.body().getMessage(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mSuccessDeleteLiveData.setValue(false);
            }
        });


    }


    public void editUser(Call<EditUserCircle> call) {

        Log.d(TAG, "registerUser: ");
        call.enqueue(new Callback<EditUserCircle>() {
            @Override
            public void onResponse(Call<EditUserCircle> call, Response<EditUserCircle> response) {
                if (response.code() != 200) {
                    circleData.setValue(null);
                    Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    return;
                }
                Log.d(TAG, "onResponse: "+response.code());
                circleData.setValue(response.body().getData().getUserProfile());
            }

            @Override
            public void onFailure(Call<EditUserCircle> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                circleData.setValue(null);
            }
        });


    }


}
