package com.ICMSoft.BH_Health.ui.beSafe;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ICMSoft.BH_Health.R;
import com.ICMSoft.BH_Health.adapter.BeSafeAdapter;
import com.ICMSoft.BH_Health.data.model.beSafe.BeSafeResponse;
import com.ICMSoft.BH_Health.data.model.beSafe.Catagory.BeSafeCatagory;
import com.ICMSoft.BH_Health.data.model.beSafe.Catagory.Catagory;
import com.ICMSoft.BH_Health.data.model.beSafe.Datum;
import com.ICMSoft.BH_Health.data.model.beSafe.PicassoImageGetter;
import com.ICMSoft.BH_Health.ui.Utils.GeneralHelper;
import com.ICMSoft.BH_Health.ui.Utils.Loading;
import com.airbnb.lottie.LottieAnimationView;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.jar.Attributes;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;

import static com.ICMSoft.BH_Health.ui.home.HomeFragment.menusBeSafe;

public class BeSafeFragment extends Fragment  {
    String TAG = "BeSafeFragment";

    private BeSafeViewModel mViewModel;
    private GeneralHelper mGeneralHelper;



    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;


    @BindView(R.id.besafe_list)
    RecyclerView besafeListView;

    private BeSafeAdapter beSafeAdapter;
    private ArrayList<Datum> data;
    private LinearLayoutManager mLinearLayoutManager;


    @BindView(R.id.be_safe_view)
    NestedScrollView beSafeView;
    private boolean flag;

    ArrayList<RadioButton> menus;
    private boolean activate;

    Loading loading;
    public static BeSafeFragment newInstance() {
        return new BeSafeFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.be_safe_fragment, container, false);
        ButterKnife.bind(this, view);
        loading = new Loading(view,beSafeView);
        mViewModel = new ViewModelProvider(this).get(BeSafeViewModel.class);
        mGeneralHelper = new GeneralHelper(getContext());
        mViewModel.setActivity(getActivity());
        data  = new ArrayList<>();
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        besafeListView.setLayoutManager(mLinearLayoutManager);
        flag = true;
        menus = new ArrayList<>();
        activate = true;
        loading.startLoading();
        intialMenu();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
               for(int i = 0; i < menus.size();i++){
                   if(menus.get(i).isChecked()){
                       selected(i);

                       break;

                   }
               }
            }
        });


        return view;
    }

    public void menuButtonSetup(RadioButton button){
        button.setBackgroundResource(R.drawable.radio_flat_selector);
        button.setPadding(10,30,10,30);
        button.setButtonDrawable(android.R.color.transparent);
        button.setGravity(Gravity.CENTER_HORIZONTAL);
        button.setTextColor(Color.BLACK);

        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        p.weight = 1;
        p.setMargins(0,0,30,0);
        button.setLayoutParams(p);
    }


    private  void intialMenu(){

           Call<BeSafeCatagory> call = mGeneralHelper.getApiService().getBeSafeCatagory();
           mViewModel.getBesafeCatagory(call);


           mViewModel.getBeSafeCatagoryData().observe(getViewLifecycleOwner(), new Observer<BeSafeCatagory>() {
               @Override
               public void onChanged(BeSafeCatagory datum) {
                   if (datum == null) {
                       return;
                   }
                   if(activate) {
                       RadioButton button;
                       for (Catagory data : datum.getData()) {
                           button = new RadioButton(getContext());
                           button.setText(data.getTitle());
                           button.setTag(data.getTitle());
                           menuButtonSetup(button);

                           radioGroup.addView(button);

                           menus.add(button);


                       }


                       menusReset();
                       selected(0);
                       activate = false;
                   }

               }
           });



    }


    private void getData(int id) {
        loading.startLoading();
        data.clear();

        Log.d(TAG, "getData: "+id);



        Call<BeSafeResponse> call = mGeneralHelper.getApiService().getBeSafe( "category/" + id);
        mViewModel.getBesafe(call);

        mViewModel.getBeSafeData().observe(getViewLifecycleOwner(), datum -> {
            if (datum == null) {
               // titleTV.setText("");
               // bodyTV.setText("No Data Found");
                loading.stopLoading();
                return;
            }

            loading.stopLoading();
            data.clear();
            //Log.d(TAG, "onChanged11: "+beSafeAdapter.getCount());
            for(Datum d:datum)
            {
                data.add(d);
                Log.d(TAG, "onChanged: "+d.getTitle());
            }





            beSafeAdapter = new BeSafeAdapter(getContext(),data);
            besafeListView.setAdapter(beSafeAdapter);
            imageTouch();


        });
    }

private void menusReset(){
        for(RadioButton b:menus)
        {
            b.setChecked(false);
            b.setTextColor(Color.BLACK);
        }
}

private void selected(int i){
        menusReset();
    menus.get(i).setTextColor(Color.WHITE);
    menus.get(i).setChecked(true);
    getData(i+1);
}



    private void imageTouch(){
        beSafeAdapter.setImageShow(new BeSafeAdapter.ImageShow() {
            @Override
            public void imagePressed(View view, String url) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                View customLayout = getLayoutInflater().inflate(R.layout.be_safe_image_show, null);

                PhotoView photoView = (PhotoView) customLayout.findViewById(R.id.photo_view);
                Picasso.get()
                        .load(url)
                      //  .fit()
                        .into(photoView);
                builder.setView(customLayout);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(BeSafeViewModel.class);

        // TODO: Use the ViewModel
    }

}
