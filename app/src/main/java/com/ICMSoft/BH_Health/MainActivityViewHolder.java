package com.ICMSoft.BH_Health;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.GsonBuilder;
import com.ICMSoft.BH_Health.data.model.Auth.LogoutResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivityViewHolder extends ViewModel {
    private static final String TAG = "MainActivityViewHolder";

    private MutableLiveData<Boolean> mSuccessLiveData;

    public MainActivityViewHolder() {
        mSuccessLiveData = new MutableLiveData<>();
    }

    public LiveData<Boolean> logoutResponse() {
        return mSuccessLiveData;
    }

    public void logoutUser(Call<LogoutResponse> call) {

       Log.d(TAG, "logoutUser: ");

       call.enqueue(new Callback<LogoutResponse>() {
           @Override
           public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {

               if (response.code() != 200) {
                   mSuccessLiveData.setValue(false);
                   Log.w(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                   Log.d(TAG, "onResponse: "+response.code());
                   return;
               }

               mSuccessLiveData.setValue(true);
           }

           @Override
           public void onFailure(Call<LogoutResponse> call, Throwable t) {

           }
       });

    }

}
